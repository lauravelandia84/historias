package historiamedica

/**
 * Patient
 * A domain class describes the data object and it's mapping to the database
 */
class Patient {
  
  String name
  String firtsName
  String represent
  String cedula 
  Date dateBirth
  String gender
  String statusMarital
  Date dateHistory
  String groupSanguineo
  String placeBirth
  String country
  String phone
  String address
  String email
  String allergy
  String referedTo
  String insurance
  String numberInsurance
  byte[] image
  static  hasMany   = [seguros:Insurance]  // tells GORM to associate other domain objects for a 1-n or n-m mapping

    static  mapping = {
    }
    
  static  constraints = {
    name blank: false, nullable: false
    firtsName blank: false, nullable: false
    gender (inList:['Femenino','Masculino']) 
    statusMarital (inList:['Casado(a)','Soltero(a)','Viudo(a)'])
    groupSanguineo (inList:['O-','O+','A-','A+','B-','B+','AB-','AB-'])
    image(nullable:true, maxSize:1073741824) /* 64K */ 
    }
  String toString(){
    return name+' '+firtsName
  }     
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
