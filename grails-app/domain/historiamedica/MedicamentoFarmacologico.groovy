package historiamedica

/**
 * MedicamentoFarmacologico
 * A domain class describes the data object and it's mapping to the database
 */
class MedicamentoFarmacologico implements Serializable {
    Medicamento medicamento
    Farmacologico farmacologico

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static mappedBy		= []	// specifies which property should be used in a mapping 
	
    static mapping = {
        id composite: ['medicamento', 'farmacologico']
        version false
    }
    
	static constraints = {
    }

    static MedicamentoFarmacologico create(Medicamento medicamento, Farmacologico farmacologico, boolean flush = false) {
        new MedicamentoFarmacologico(medicamento: medicamento, farmacologico: farmacologico).save(flush: flush, insert: true)
    }

    static boolean remove(Medicamento medicamento, Farmacologico farmacologico, boolean flush = false) {
        MedicamentoFarmacologico instance = MedicamentoFarmacologico.findByMedicamentoAndFarmacologico(medicamento, farmacologico)
        if (!instance) {
            return false
        }

        instance.delete(flush: flush)
        true
    }
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
