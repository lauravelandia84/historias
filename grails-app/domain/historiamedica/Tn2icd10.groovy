package historiamedica

/**
 * Tn2icd10
 * A domain class describes the data object and it's mapping to the database
 */
class Tn2icd10 {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static mappedBy		= []	// specifies which property should be used in a mapping 
	static hasMany	= [tn3icd10:Tn3icd10]
	static  belongsTo   = [tn1icd10:Tn1icd10] 
	String descripcion
	String categoria
	String subcategoria
    static mapping = {
    }
    
	static constraints = {
		descripcion blank: false, nullable: false
		categoria blank: false, nullable: false
		subcategoria blank: false, nullable: false
    }
		String toString(){
    	return descripcion
    	return categoria
    	return subcategoria
    	
  }    
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
