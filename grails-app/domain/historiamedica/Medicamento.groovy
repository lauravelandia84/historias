package historiamedica

/**
 * Medicamento
 * A domain class describes the data object and it's mapping to the database
 */
class Medicamento {

static  belongsTo = Laboratorios
static hasMany   = [principio:Principio,farmacologico:Farmacologico]
String medicamento
String nombre
String descripcion
String composicion
String propiedades
String farmacología
String indicaciones
String dosificacion
String contraindicaciones
String advertencias
String reaccionesAdversas
String precauciones
String interacciones
String conservacion
String sobredosificacion
String presentaciones
String nota
String total
    Laboratorios laboratorios
	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
//	static belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static mappedBy		= []	// specifies which property should be used in a mapping 
	
    static mapping = {
        principios joinTable:[name:"principio_medicamento", key:"medicamento_id"]
        farmacologicos joinTable: [name: "farmacologico_medicamento", key: "medicamento_id"]
        descripcion type: "text"
        composicion type: "text"
        propiedades type: "text"
        farmacología type: "text"
        indicaciones type: "text"
        dosificacion type: "text"
        contraindicaciones type: "text"
        advertencias type: "text"
        reaccionesAdversas type: "text"
        precauciones type: "text"
        interacciones type: "text"
        conservacion type: "text"
        sobredosificacion type: "text"
        presentaciones type: "text"
        nota type: "text"
        total type: "text"
    }
    
	static constraints = {
		medicamento blank: false, nullable: false
    }
	String toString(){
        return medicamento
    }
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}

}
