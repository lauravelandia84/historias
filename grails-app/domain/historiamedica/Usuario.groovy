package historiamedica

class Usuario {

	transient springSecurityService

	String username
	String password
    String email
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired
    static hasMany = [rol:Rol]

	static constraints = {
		username blank: false, unique: true
		password blank: false
        email    blank: false, email: true
	}

	static mapping = {
        rols joinTable:[name:"usario_rol", key:"usuario_id"]
		password column: '`password`'
	}

	Set<Rol> getAuthorities() {
		UsuarioRol.findAllByUsuario(this).collect { it.rol } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
}
