package historiamedica
//import historiamedica.Patient

/**
 * Insurance
 * A domain class describes the data object and it's mapping to the database
 */
class Insurance {

	/* Default (injected) attributes of GORM */
//	Long	id
//	Long	version
	
	/* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated
	
  //Patient patient	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping 
  String name
  String rif
  String nif
  String address
  String email
  String phone
  String numberInsurance
  //static belongsTo = [patient:Patient]
  
    static	mapping = {
    }
    
	static	constraints = {
		name blank: false, nullable: false
    }
  String toString(){
  return name
   } 
}
