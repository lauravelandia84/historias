package historiamedica

class Rol {

	String authority
    static belongsTo = Usuario
    static hasMany = [usuario: Usuario]

	static mapping = {
        usuarios joinTable:[name:"usuario_rol", key:"rol_id"]
		cache true
	}

	static constraints = {
		authority blank: false, unique: true
	}
    String toString(){
        return authority
    }
}
