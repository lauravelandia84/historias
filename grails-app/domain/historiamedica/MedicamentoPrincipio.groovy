package historiamedica

/**
 * MedicamentoPrincipio
 * A domain class describes the data object and it's mapping to the database
 */
class MedicamentoPrincipio implements Serializable {
    Medicamento medicamento
    Principio principio

    /* Default (injected) attributes of GORM */
//	Long	id
//	Long	version

    /* Automatic timestamping of GORM */
//	Date	dateCreated
//	Date	lastUpdated

//	static belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static mappedBy		= []	// specifies which property should be used in a mapping 

    static mapping = {
        id composite: ['medicamento', 'principio']
        version false
    }

    static constraints = {
    }

    static MedicamentoPrincipio create(Medicamento medicamento, Principio principio, boolean flush = false) {
        new MedicamentoPrincipio(medicamento: medicamento, principio: principio).save(flush: flush, insert: true)
    }

    static boolean remove(Medicamento medicamento, Principio principio, boolean flush = false) {
        MedicamentoPrincipio instance = MedicamentoPrincipio.findByMedicamentoAndPrincipio(medicamento, principio)
        if (!instance) {
            return false
        }

        instance.delete(flush: flush)
        true
    }
    /*
     * Methods of the Domain Class
     */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
