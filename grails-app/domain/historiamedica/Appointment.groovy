package historiamedica

/**
 * Appointment
 * A domain class describes the data object and it's mapping to the database
 */
class Appointment {
	//static	hasMany	= [consulta:Consultation]
	//static belongsTo  = [medico:Medico,patient:Patient] 
    static mapping = {
    }
    
	static constraints = {
    }
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
