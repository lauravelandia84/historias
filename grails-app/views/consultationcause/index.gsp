
<%@ page import="historiamedica.Consultationcause" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'consultationcause.label', default: 'Consultationcause')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-consultationcause" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'consultationcause.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="name" title="${message(code: 'consultationcause.name.label', default: 'Name')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${consultationcauseInstanceList}" status="i" var="consultationcauseInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${consultationcauseInstance.id}">${fieldValue(bean: consultationcauseInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: consultationcauseInstance, field: "name")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${consultationcauseInstanceCount}" />
	</div>
</section>

</body>

</html>
