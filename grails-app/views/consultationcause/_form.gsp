<%@ page import="historiamedica.Consultationcause" %>



			<div class="control-group fieldcontain ${hasErrors(bean: consultationcauseInstance, field: 'code', 'error')} required">
				<label for="code" class="control-label"><g:message code="consultationcause.code.label" default="Code" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="code" required="" value="${consultationcauseInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: consultationcauseInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: consultationcauseInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="consultationcause.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${consultationcauseInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: consultationcauseInstance, field: 'name', 'error')}</span>
				</div>
			</div>

