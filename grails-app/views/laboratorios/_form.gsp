<%@ page import="historiamedica.Laboratorios" %>



			<div class="control-group fieldcontain ${hasErrors(bean: laboratoriosInstance, field: 'laboratorio', 'error')} required">
				<label for="laboratorio" class="control-label"><g:message code="laboratorios.laboratorio.label" default="Laboratorio" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="laboratorio" required="" value="${laboratoriosInstance?.laboratorio}"/>
					<span class="help-inline">${hasErrors(bean: laboratoriosInstance, field: 'laboratorio', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: laboratoriosInstance, field: 'ciudad', 'error')} ">
				<label for="ciudad" class="control-label"><g:message code="laboratorios.ciudad.label" default="Ciudad" /></label>
				<div class="controls">
					<g:textField name="ciudad" value="${laboratoriosInstance?.ciudad}"/>
					<span class="help-inline">${hasErrors(bean: laboratoriosInstance, field: 'ciudad', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: laboratoriosInstance, field: 'dirección', 'error')} ">
				<label for="dirección" class="control-label"><g:message code="laboratorios.dirección.label" default="Dirección" /></label>
				<div class="controls">
					<g:textField name="dirección" value="${laboratoriosInstance?.dirección}"/>
					<span class="help-inline">${hasErrors(bean: laboratoriosInstance, field: 'dirección', 'error')}</span>
				</div>
			</div>



			<div class="control-group fieldcontain ${hasErrors(bean: laboratoriosInstance, field: 'telefono', 'error')} ">
				<label for="telefono" class="control-label"><g:message code="laboratorios.telefono.label" default="Telefono" /></label>
				<div class="controls">
					<g:textField name="telefono" value="${laboratoriosInstance?.telefono}"/>
					<span class="help-inline">${hasErrors(bean: laboratoriosInstance, field: 'telefono', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: laboratoriosInstance, field: 'medicamento', 'error')} ">
				<label for="medicamento" class="control-label"><g:message code="laboratorios.medicamento.label" default="Medicamento" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${laboratoriosInstance?.medicamento?}" var="m">
    <li><g:link controller="medicamento" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="medicamento" action="create" params="['laboratorios.id': laboratoriosInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'medicamento.label', default: 'Medicamento')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: laboratoriosInstance, field: 'medicamento', 'error')}</span>
				</div>
			</div>