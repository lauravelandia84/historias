
<%@ page import="historiamedica.Laboratorios" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'laboratorios.label', default: 'Laboratorios')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>		
<section id="list-laboratorios" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="laboratorio" title="${message(code: 'laboratorios.laboratorio.label', default: 'Laboratorio')}" />
			
				<g:sortableColumn property="ciudad" title="${message(code: 'laboratorios.ciudad.label', default: 'Ciudad')}" />
			
				<g:sortableColumn property="dirección" title="${message(code: 'laboratorios.dirección.label', default: 'Dirección')}" />
			
				<g:sortableColumn property="telefono" title="${message(code: 'laboratorios.telefono.label', default: 'Telefono')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${laboratoriosInstanceList}" status="i" var="laboratoriosInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${laboratoriosInstance.id}">${fieldValue(bean: laboratoriosInstance, field: "laboratorio")}</g:link></td>
			
				<td>${fieldValue(bean: laboratoriosInstance, field: "ciudad")}</td>
			
				<td>${fieldValue(bean: laboratoriosInstance, field: "dirección")}</td>
			
				<td>${fieldValue(bean: laboratoriosInstance, field: "telefono")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${laboratoriosInstanceTotal}" />
	</div>
</section>

</body>

</html>
