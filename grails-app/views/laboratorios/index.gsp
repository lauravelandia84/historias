
<%@ page import="historiamedica.Laboratorios" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'laboratorios.label', default: 'Laboratorios')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-laboratorios" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="laboratorio" title="${message(code: 'laboratorios.laboratorio.label', default: 'Laboratorio')}" />
			
				<g:sortableColumn property="ciudad" title="${message(code: 'laboratorios.ciudad.label', default: 'Ciudad')}" />
			
				<g:sortableColumn property="dirección" title="${message(code: 'laboratorios.dirección.label', default: 'Dirección')}" />
			
				<g:sortableColumn property="telefono" title="${message(code: 'laboratorios.telefono.label', default: 'Telefono')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${laboratoriosInstanceList}" status="i" var="laboratoriosInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${laboratoriosInstance.id}">${fieldValue(bean: laboratoriosInstance, field: "laboratorio")}</g:link></td>
			
				<td>${fieldValue(bean: laboratoriosInstance, field: "ciudad")}</td>
			
				<td>${fieldValue(bean: laboratoriosInstance, field: "dirección")}</td>
			
				<td>${fieldValue(bean: laboratoriosInstance, field: "telefono")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${laboratoriosInstanceCount}" />
	</div>
</section>

</body>

</html>
