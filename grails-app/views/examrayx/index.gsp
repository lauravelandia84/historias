
<%@ page import="historiamedica.Examrayx" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'examrayx.label', default: 'Examrayx')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-examrayx" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'examrayx.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="name" title="${message(code: 'examrayx.name.label', default: 'Name')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${examrayxInstanceList}" status="i" var="examrayxInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${examrayxInstance.id}">${fieldValue(bean: examrayxInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: examrayxInstance, field: "name")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${examrayxInstanceCount}" />
	</div>
</section>

</body>

</html>
