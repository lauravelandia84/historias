<%@ page import="historiamedica.Examrayx" %>



			<div class="control-group fieldcontain ${hasErrors(bean: examrayxInstance, field: 'code', 'error')} required">
				<label for="code" class="control-label"><g:message code="examrayx.code.label" default="Code" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="code" required="" value="${examrayxInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: examrayxInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: examrayxInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="examrayx.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${examrayxInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: examrayxInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: examrayxInstance, field: 'subexamrayx', 'error')} ">
				<label for="subexamrayx" class="control-label"><g:message code="examrayx.subexamrayx.label" default="Subexamrayx" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${examrayxInstance?.subexamrayx?}" var="s">
    <li><g:link controller="subexamrayx" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="subexamrayx" action="create" params="['examrayx.id': examrayxInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: examrayxInstance, field: 'subexamrayx', 'error')}</span>
				</div>
			</div>

