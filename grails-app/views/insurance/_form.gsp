<%@ page import="historiamedica.Insurance" %>



			<div class="control-group fieldcontain ${hasErrors(bean: insuranceInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="insurance.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${insuranceInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: insuranceInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: insuranceInstance, field: 'address', 'error')} ">
				<label for="address" class="control-label"><g:message code="insurance.address.label" default="Address" /></label>
				<div class="controls">
					<g:textField name="address" value="${insuranceInstance?.address}"/>
					<span class="help-inline">${hasErrors(bean: insuranceInstance, field: 'address', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: insuranceInstance, field: 'email', 'error')} ">
				<label for="email" class="control-label"><g:message code="insurance.email.label" default="Email" /></label>
				<div class="controls">
					<g:textField name="email" value="${insuranceInstance?.email}"/>
					<span class="help-inline">${hasErrors(bean: insuranceInstance, field: 'email', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: insuranceInstance, field: 'nif', 'error')} ">
				<label for="nif" class="control-label"><g:message code="insurance.nif.label" default="Nif" /></label>
				<div class="controls">
					<g:textField name="nif" value="${insuranceInstance?.nif}"/>
					<span class="help-inline">${hasErrors(bean: insuranceInstance, field: 'nif', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: insuranceInstance, field: 'numberInsurance', 'error')} ">
				<label for="numberInsurance" class="control-label"><g:message code="insurance.numberInsurance.label" default="Number Insurance" /></label>
				<div class="controls">
					<g:textField name="numberInsurance" value="${insuranceInstance?.numberInsurance}"/>
					<span class="help-inline">${hasErrors(bean: insuranceInstance, field: 'numberInsurance', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: insuranceInstance, field: 'phone', 'error')} ">
				<label for="phone" class="control-label"><g:message code="insurance.phone.label" default="Phone" /></label>
				<div class="controls">
					<g:textField name="phone" value="${insuranceInstance?.phone}"/>
					<span class="help-inline">${hasErrors(bean: insuranceInstance, field: 'phone', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: insuranceInstance, field: 'rif', 'error')} ">
				<label for="rif" class="control-label"><g:message code="insurance.rif.label" default="Rif" /></label>
				<div class="controls">
					<g:textField name="rif" value="${insuranceInstance?.rif}"/>
					<span class="help-inline">${hasErrors(bean: insuranceInstance, field: 'rif', 'error')}</span>
				</div>
			</div>

