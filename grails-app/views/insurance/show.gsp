
<%@ page import="historiamedica.Insurance" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'insurance.label', default: 'Insurance')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>	
<section id="show-insurance" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="insurance.name.label" default="Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: insuranceInstance, field: "name")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="insurance.address.label" default="Address" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: insuranceInstance, field: "address")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="insurance.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: insuranceInstance, field: "email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="insurance.nif.label" default="Nif" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: insuranceInstance, field: "nif")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="insurance.numberInsurance.label" default="Number Insurance" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: insuranceInstance, field: "numberInsurance")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="insurance.phone.label" default="Phone" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: insuranceInstance, field: "phone")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="insurance.rif.label" default="Rif" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: insuranceInstance, field: "rif")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
