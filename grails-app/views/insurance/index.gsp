
<%@ page import="historiamedica.Insurance" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'insurance.label', default: 'Insurance')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-insurance" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'insurance.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'insurance.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="email" title="${message(code: 'insurance.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="nif" title="${message(code: 'insurance.nif.label', default: 'Nif')}" />
			
				<g:sortableColumn property="numberInsurance" title="${message(code: 'insurance.numberInsurance.label', default: 'Number Insurance')}" />
			
				<g:sortableColumn property="phone" title="${message(code: 'insurance.phone.label', default: 'Phone')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${insuranceInstanceList}" status="i" var="insuranceInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${insuranceInstance.id}">${fieldValue(bean: insuranceInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "email")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "nif")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "numberInsurance")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "phone")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${insuranceInstanceCount}" />
	</div>
</section>

</body>

</html>
