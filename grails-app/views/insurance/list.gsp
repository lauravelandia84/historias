
<%@ page import="historiamedica.Insurance" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'insurance.label', default: 'Insurance')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>	
<section id="list-insurance" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'insurance.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'insurance.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="email" title="${message(code: 'insurance.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="nif" title="${message(code: 'insurance.nif.label', default: 'Nif')}" />
			
				<g:sortableColumn property="numberInsurance" title="${message(code: 'insurance.numberInsurance.label', default: 'Number Insurance')}" />
			
				<g:sortableColumn property="phone" title="${message(code: 'insurance.phone.label', default: 'Phone')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${insuranceInstanceList}" status="i" var="insuranceInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${insuranceInstance.id}">${fieldValue(bean: insuranceInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "email")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "nif")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "numberInsurance")}</td>
			
				<td>${fieldValue(bean: insuranceInstance, field: "phone")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${insuranceInstanceTotal}" />
	</div>
</section>

</body>

</html>
