<%@ page import="historiamedica.Tn3icd10" %>


			<div class="control-group fieldcontain ${hasErrors(bean: tn3icd10Instance, field: 'descripcion', 'error')} required">
				<label for="descripcion" class="control-label"><g:message code="tn3icd10.descripcion.label" default="Descripcion" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="descripcion" required="" value="${tn3icd10Instance?.descripcion}"/>
					<span class="help-inline">${hasErrors(bean: tn3icd10Instance, field: 'descripcion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn3icd10Instance, field: 'categoria', 'error')} required">
				<label for="categoria" class="control-label"><g:message code="tn3icd10.categoria.label" default="Categoria" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="categoria" required="" value="${tn3icd10Instance?.categoria}"/>
					<span class="help-inline">${hasErrors(bean: tn3icd10Instance, field: 'categoria', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn3icd10Instance, field: 'tn2icd10', 'error')} required">
				<label for="tn2icd10" class="control-label"><g:message code="tn3icd10.tn2icd10.label" default="Tn2icd10" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="tn2icd10" name="tn2icd10.id" from="${historiamedica.Tn2icd10.list()}" optionKey="id" required="" value="${tn3icd10Instance?.tn2icd10?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: tn3icd10Instance, field: 'tn2icd10', 'error')}</span>
				</div>
			</div>

