<label for="tn3icd10" class="control-label"><g:message code="consultation.tn3icd10.label" default="Tn3icd10" /></label><br><br>
<div class="controls"  style="margin-left: 15px;">
    <g:select id="tn3icd10" name="tn3icd10"  from="${tn3icd10List}" optionKey="id" size="10"
              onchange=" agregarDiagnostico(this.value, this.options[this.selectedIndex].text)"/>
</div>z