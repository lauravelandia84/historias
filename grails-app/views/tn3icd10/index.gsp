
<%@ page import="historiamedica.Tn3icd10" %>
<!DOCTYPE html>
<html>


<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tn3icd10.label', default: 'Tn3icd10')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-tn3icd10" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="descripcion" title="${message(code: 'tn3icd10.descripcion.label', default: 'Descripcion')}" />
			
				<g:sortableColumn property="categoria" title="${message(code: 'tn3icd10.categoria.label', default: 'Categoria')}" />
			
				<th><g:message code="tn3icd10.tn2icd10.label" default="Tn2icd10" /></th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${tn3icd10InstanceList}" status="i" var="tn3icd10Instance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${tn3icd10Instance.id}">${fieldValue(bean: tn3icd10Instance, field: "descripcion")}</g:link></td>
			
				<td>${fieldValue(bean: tn3icd10Instance, field: "categoria")}</td>
			
				<td>${fieldValue(bean: tn3icd10Instance, field: "tn2icd10")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${tn3icd10InstanceCount}" />
	</div>
</section>

</body>

</html>
