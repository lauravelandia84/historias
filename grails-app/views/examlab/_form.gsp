<%@ page import="historiamedica.Examlab" %>



			<div class="control-group fieldcontain ${hasErrors(bean: examlabInstance, field: 'code', 'error')} required">
				<label for="code" class="control-label"><g:message code="examlab.code.label" default="Code" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="code" required="" value="${examlabInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: examlabInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: examlabInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="examlab.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${examlabInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: examlabInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: examlabInstance, field: 'subexamlab', 'error')} ">
				<label for="subexamlab" class="control-label"><g:message code="examlab.subexamlab.label" default="Subexamlab" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${examlabInstance?.subexamlab?}" var="s">
    <li><g:link controller="subexamlab" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="subexamlab" action="create" params="['examlab.id': examlabInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'subexamlab.label', default: 'Subexamlab')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: examlabInstance, field: 'subexamlab', 'error')}</span>
				</div>
			</div>

