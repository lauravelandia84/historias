
<%@ page import="historiamedica.Examlab" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'examlab.label', default: 'Examlab')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-examlab" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'examlab.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="name" title="${message(code: 'examlab.name.label', default: 'Name')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${examlabInstanceList}" status="i" var="examlabInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${examlabInstance.id}">${fieldValue(bean: examlabInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: examlabInstance, field: "name")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${examlabInstanceCount}" />
	</div>
</section>

</body>

</html>
