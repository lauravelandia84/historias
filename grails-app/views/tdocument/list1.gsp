<%@ page import="historiamedica.Patient" %>
<%@ page import="historiamedica.Consultation" %>
<%@ page import="historiamedica.Tdocument" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'patient.label', default: 'Pacientes')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<ul id="Menu" class="nav nav-pills">
		<li>
							<g:link class="list1" action="list1" args="[entityName]">
								<i class="icon-list"></i>
								<g:message code="Buscar Documentos Médicos por fecha de Consulta"/>
							</g:link>
						</li>
		</li>
		
	</ul>

<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list1" method="GET">
        <div class="fieldcontain">
            <label for="query"><b>Buscar fecha:</label> <b>Fecha del Documento
            <bs:datePicker name="dateInit" precision="day"  value="${params.query}"  />
          
            <div class="form-actions">
			<g:submitButton name="search" class="btn btn-primary" value="Buscar" />
            
		
		 
        </div>
        </div>
    </g:form>
</fieldset>
<table>	
<section id="list-tdocument" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
				<g:sortableColumn property="datedocument" title="${message(code: 'tdocument.datedocument.label', default: 'Datedocument')}" />
			
				<g:sortableColumn property="description" title="${message(code: 'tdocument.description.label', default: 'Description')}" />
			
				<th><g:message code="tdocument.medico.label" default="Medico" /></th>
			
				<th><g:message code="tdocument.patient.label" default="Patient" /></th>
			
				<th><g:message code="tdocument.typedocument.label" default="Typedocument" /></th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${tdocumentInstanceList}" status="i" var="tdocumentInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>${tdocumentInstance[0]}</td>
			
				<td>${tdocumentInstance[1]}</td>
			
				<td>${tdocumentInstance[2]}</td>

				<td>${tdocumentInstance[3]}</td>

				<td>${tdocumentInstance[4]}</td>
				<td><g:link action="downList1" id="${tdocumentInstance[5]}" params="[export: 'pdf']">Descargar</g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		
	</div>
</section>

</body>

</html>
