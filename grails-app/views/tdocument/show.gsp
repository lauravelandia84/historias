
<%@ page import="historiamedica.Tdocument" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tdocument.label', default: 'Tdocument')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>	
<section id="show-tdocument" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="tdocument.datedocument.label" default="Datedocument" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${tdocumentInstance?.datedocument}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="tdocument.description.label" default="Description" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: tdocumentInstance, field: "description")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="tdocument.medico.label" default="Medico" /></td>
				
				<td valign="top" class="value"><g:link controller="medico" action="show" id="${tdocumentInstance?.medico?.id}">${tdocumentInstance?.medico?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="tdocument.patient.label" default="Patient" /></td>
				
				<td valign="top" class="value"><g:link controller="patient" action="show" id="${tdocumentInstance?.patient?.id}">${tdocumentInstance?.patient?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="tdocument.typedocument.label" default="Typedocument" /></td>
				
				<td valign="top" class="value"><g:link controller="typedocument" action="show" id="${tdocumentInstance?.typedocument?.id}">${tdocumentInstance?.typedocument?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
