
<%@ page import="historiamedica.Tdocument" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tdocument.label', default: 'Tdocument')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-tdocument" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="datedocument" title="${message(code: 'tdocument.datedocument.label', default: 'Datedocument')}" />
			
				<g:sortableColumn property="description" title="${message(code: 'tdocument.description.label', default: 'Description')}" />
			
				<th><g:message code="tdocument.medico.label" default="Medico" /></th>
			
				<th><g:message code="tdocument.patient.label" default="Patient" /></th>
			
				<th><g:message code="tdocument.typedocument.label" default="Typedocument" /></th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${tdocumentInstanceList}" status="i" var="tdocumentInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${tdocumentInstance.id}">${fieldValue(bean: tdocumentInstance, field: "datedocument")}</g:link></td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "description")}</td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "medico")}</td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "patient")}</td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "typedocument")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${tdocumentInstanceCount}" />
	</div>
</section>

</body>

</html>
