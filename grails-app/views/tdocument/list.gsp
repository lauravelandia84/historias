
<%@ page import="historiamedica.Tdocument" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tdocument.label', default: 'Tdocument')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>		
<section id="list-tdocument" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="datedocument" title="${message(code: 'tdocument.datedocument.label', default: 'Datedocument')}" />
			
				<g:sortableColumn property="description" title="${message(code: 'tdocument.description.label', default: 'Description')}" />
			
				<th><g:message code="tdocument.medico.label" default="Medico" /></th>
			
				<th><g:message code="tdocument.patient.label" default="Patient" /></th>
			
				<th><g:message code="tdocument.typedocument.label" default="Typedocument" /></th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${tdocumentInstanceList}" status="i" var="tdocumentInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${tdocumentInstance.id}">${fieldValue(bean: tdocumentInstance, field: "datedocument")}</g:link></td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "description")}</td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "medico")}</td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "patient")}</td>
			
				<td>${fieldValue(bean: tdocumentInstance, field: "typedocument")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${tdocumentInstanceTotal}" />
	</div>
</section>

</body>

</html>
