<%@ page import="historiamedica.Tdocument" %>



			<div class="control-group fieldcontain ${hasErrors(bean: tdocumentInstance, field: 'datedocument', 'error')} required">
				<label for="datedocument" class="control-label"><g:message code="tdocument.datedocument.label" default="Datedocument" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="datedocument" precision="day"  value="${tdocumentInstance?.datedocument}"  />
					<span class="help-inline">${hasErrors(bean: tdocumentInstance, field: 'datedocument', 'error')}</span>
				</div>
			</div>

			

			<div class="control-group fieldcontain ${hasErrors(bean: tdocumentInstance, field: 'medico', 'error')} required">
				<label for="medico" class="control-label"><g:message code="tdocument.medico.label" default="Medico" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="medico" name="medico.id" from="${historiamedica.Medico.list()}" optionKey="id" required="" value="${tdocumentInstance?.medico?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: tdocumentInstance, field: 'medico', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tdocumentInstance, field: 'patient', 'error')} required">
				<label for="patient" class="control-label"><g:message code="tdocument.patient.label" default="Patient" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="patient" name="patient.id" from="${historiamedica.Patient.list()}" optionKey="id" required="" value="${tdocumentInstance?.patient?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: tdocumentInstance, field: 'patient', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tdocumentInstance, field: 'typedocument', 'error')} required">
				<label for="typedocument" class="control-label"><g:message code="tdocument.typedocument.label" default="Typedocument" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="typedocument" name="typedocument.id" from="${historiamedica.Typedocument.list()}" optionKey="id" required="" value="${tdocumentInstance?.typedocument?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: tdocumentInstance, field: 'typedocument', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: tdocumentInstance, field: 'description', 'error')} ">
				<label for="description" class="control-label"><g:message code="tdocument.description.label" default="Description" /></label>
				<div class="controls">
					<g:textArea name="description" style="width: 60%;" value="${tdocumentInstance?.description}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: tdocumentInstance, field: 'description', 'error')}</span>
				</div>
			</div>
