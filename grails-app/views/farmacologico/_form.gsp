<%@ page import="historiamedica.Farmacologico" %>



			<div class="control-group fieldcontain ${hasErrors(bean: farmacologicoInstance, field: 'farmacologico', 'error')} required">
				<label for="farmacologico" class="control-label"><g:message code="farmacologico.farmacologico.label" default="Farmacologico" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="farmacologico" required="" value="${farmacologicoInstance?.farmacologico}"/>
					<span class="help-inline">${hasErrors(bean: farmacologicoInstance, field: 'farmacologico', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: farmacologicoInstance, field: 'medicamento', 'error')} ">
				<label for="medicamento" class="control-label"><g:message code="farmacologico.medicamento.label" default="Medicamento" /></label>
				<div class="controls">
					
					<span class="help-inline">${hasErrors(bean: farmacologicoInstance, field: 'medicamento', 'error')}</span>
				</div>
			</div>

