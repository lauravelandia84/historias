
<%@ page import="historiamedica.Farmacologico" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'farmacologico.label', default: 'Farmacologico')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-farmacologico" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="farmacologico" title="${message(code: 'farmacologico.farmacologico.label', default: 'Farmacologico')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${farmacologicoInstanceList}" status="i" var="farmacologicoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${farmacologicoInstance.id}">${fieldValue(bean: farmacologicoInstance, field: "farmacologico")}</g:link></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${farmacologicoInstanceCount}" />
	</div>
</section>

</body>

</html>
