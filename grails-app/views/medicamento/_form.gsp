<%@ page import="historiamedica.Medicamento" %>



			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'medicamento', 'error')} required">
				<label for="medicamento" class="control-label"><g:message code="medicamento.medicamento.label" default="Medicamento" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="medicamento" required="" value="${medicamentoInstance?.medicamento}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'medicamento', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'advertencias', 'error')} ">
				<label for="advertencias" class="control-label"><g:message code="medicamento.advertencias.label" default="Advertencias" /></label>
				<div class="controls">
					<g:textField name="advertencias" value="${medicamentoInstance?.advertencias}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'advertencias', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'composicion', 'error')} ">
				<label for="composicion" class="control-label"><g:message code="medicamento.composicion.label" default="Composicion" /></label>
				<div class="controls">
					<g:textField name="composicion" value="${medicamentoInstance?.composicion}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'composicion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'conservacion', 'error')} ">
				<label for="conservacion" class="control-label"><g:message code="medicamento.conservacion.label" default="Conservacion" /></label>
				<div class="controls">
					<g:textField name="conservacion" value="${medicamentoInstance?.conservacion}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'conservacion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'contraindicaciones', 'error')} ">
				<label for="contraindicaciones" class="control-label"><g:message code="medicamento.contraindicaciones.label" default="Contraindicaciones" /></label>
				<div class="controls">
					<g:textField name="contraindicaciones" value="${medicamentoInstance?.contraindicaciones}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'contraindicaciones', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'descripcion', 'error')} ">
				<label for="descripcion" class="control-label"><g:message code="medicamento.descripcion.label" default="Descripcion" /></label>
				<div class="controls">
					<g:textField name="descripcion" value="${medicamentoInstance?.descripcion}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'descripcion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'dosificacion', 'error')} ">
				<label for="dosificacion" class="control-label"><g:message code="medicamento.dosificacion.label" default="Dosificacion" /></label>
				<div class="controls">
					<g:textField name="dosificacion" value="${medicamentoInstance?.dosificacion}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'dosificacion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'farmacología', 'error')} ">
				<label for="farmacología" class="control-label"><g:message code="medicamento.farmacología.label" default="Farmacología" /></label>
				<div class="controls">
					<g:textField name="farmacología" value="${medicamentoInstance?.farmacología}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'farmacología', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'farmacologico', 'error')} ">
				<label for="farmacologico" class="control-label"><g:message code="medicamento.farmacologico.label" default="Farmacologico" /></label>
				<div class="controls">
					<g:select name="farmacologico" from="${historiamedica.Farmacologico.list()}" multiple="multiple" optionKey="id" size="5" value="${medicamentoInstance?.farmacologico*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'farmacologico', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'indicaciones', 'error')} ">
				<label for="indicaciones" class="control-label"><g:message code="medicamento.indicaciones.label" default="Indicaciones" /></label>
				<div class="controls">
					<g:textField name="indicaciones" value="${medicamentoInstance?.indicaciones}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'indicaciones', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'interacciones', 'error')} ">
				<label for="interacciones" class="control-label"><g:message code="medicamento.interacciones.label" default="Interacciones" /></label>
				<div class="controls">
					<g:textField name="interacciones" value="${medicamentoInstance?.interacciones}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'interacciones', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'laboratorios', 'error')} required">
				<label for="laboratorios" class="control-label"><g:message code="medicamento.laboratorios.label" default="Laboratorios" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="laboratorios" name="laboratorios.id" from="${historiamedica.Laboratorios.list()}" optionKey="id" required="" value="${medicamentoInstance?.laboratorios?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'laboratorios', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'nombre', 'error')} ">
				<label for="nombre" class="control-label"><g:message code="medicamento.nombre.label" default="Nombre" /></label>
				<div class="controls">
					<g:textField name="nombre" value="${medicamentoInstance?.nombre}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'nombre', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'nota', 'error')} ">
				<label for="nota" class="control-label"><g:message code="medicamento.nota.label" default="Nota" /></label>
				<div class="controls">
					<g:textField name="nota" value="${medicamentoInstance?.nota}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'nota', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'precauciones', 'error')} ">
				<label for="precauciones" class="control-label"><g:message code="medicamento.precauciones.label" default="Precauciones" /></label>
				<div class="controls">
					<g:textField name="precauciones" value="${medicamentoInstance?.precauciones}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'precauciones', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'presentaciones', 'error')} ">
				<label for="presentaciones" class="control-label"><g:message code="medicamento.presentaciones.label" default="Presentaciones" /></label>
				<div class="controls">
					<g:textField name="presentaciones" value="${medicamentoInstance?.presentaciones}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'presentaciones', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'principio', 'error')} ">
				<label for="principio" class="control-label"><g:message code="medicamento.principio.label" default="Principio" /></label>
				<div class="controls">
					<g:select name="principio" from="${historiamedica.Principio.list()}" multiple="multiple" optionKey="id" size="5" value="${medicamentoInstance?.principio*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'principio', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'propiedades', 'error')} ">
				<label for="propiedades" class="control-label"><g:message code="medicamento.propiedades.label" default="Propiedades" /></label>
				<div class="controls">
					<g:textField name="propiedades" value="${medicamentoInstance?.propiedades}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'propiedades', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'reaccionesAdversas', 'error')} ">
				<label for="reaccionesAdversas" class="control-label"><g:message code="medicamento.reaccionesAdversas.label" default="Reacciones Adversas" /></label>
				<div class="controls">
					<g:textField name="reaccionesAdversas" value="${medicamentoInstance?.reaccionesAdversas}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'reaccionesAdversas', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'sobredosificacion', 'error')} ">
				<label for="sobredosificacion" class="control-label"><g:message code="medicamento.sobredosificacion.label" default="Sobredosificacion" /></label>
				<div class="controls">
					<g:textField name="sobredosificacion" value="${medicamentoInstance?.sobredosificacion}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'sobredosificacion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicamentoInstance, field: 'total', 'error')} ">
				<label for="total" class="control-label"><g:message code="medicamento.total.label" default="Total" /></label>
				<div class="controls">
					<g:textField name="total" value="${medicamentoInstance?.total}"/>
					<span class="help-inline">${hasErrors(bean: medicamentoInstance, field: 'total', 'error')}</span>
				</div>
			</div>

