
<%@ page import="historiamedica.Medicamento" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medicamento.label', default: 'Medicamento')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

    <g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

    <li class="${ params.action == "list" ? 'active' : '' }">
        <g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
    </li>
    <li class="${ params.action == "create" ? 'active' : '' }">
        <g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
    </li>

    <g:if test="${ params.action == 'show' || params.action == 'edit' }">
        <!-- the item is an object (not a list) -->
        <li class="${ params.action == "edit" ? 'active' : '' }">
            <g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
        </li>
        <li class="">
            <g:render template="/_common/modals/deleteTextLink"/>
        </li>
    </g:if>

</ul>
<section id="list-medicamento" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="medicamento" title="${message(code: 'medicamento.medicamento.label', default: 'Medicamento')}" />
			
				<g:sortableColumn property="advertencias" title="${message(code: 'medicamento.advertencias.label', default: 'Advertencias')}" />
			
				<g:sortableColumn property="composicion" title="${message(code: 'medicamento.composicion.label', default: 'Composicion')}" />
			
				<g:sortableColumn property="conservacion" title="${message(code: 'medicamento.conservacion.label', default: 'Conservacion')}" />
			
				<g:sortableColumn property="contraindicaciones" title="${message(code: 'medicamento.contraindicaciones.label', default: 'Contraindicaciones')}" />
			
				<g:sortableColumn property="descripcion" title="${message(code: 'medicamento.descripcion.label', default: 'Descripcion')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${medicamentoInstanceList}" status="i" var="medicamentoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${medicamentoInstance.id}">${fieldValue(bean: medicamentoInstance, field: "medicamento")}</g:link></td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "advertencias")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "composicion")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "conservacion")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "contraindicaciones")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "descripcion")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${medicamentoInstanceTotal}" />
	</div>
</section>

</body>

</html>
