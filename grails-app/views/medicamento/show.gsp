
<%@ page import="historiamedica.Medicamento" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medicamento.label', default: 'Medicamento')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

    <g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

    <li class="${ params.action == "list" ? 'active' : '' }">
        <g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
    </li>
    <li class="${ params.action == "create" ? 'active' : '' }">
        <g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
    </li>

    <g:if test="${ params.action == 'show' || params.action == 'edit' }">
        <!-- the item is an object (not a list) -->
        <li class="${ params.action == "edit" ? 'active' : '' }">
            <g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
        </li>
        <li class="">
            <g:render template="/_common/modals/deleteTextLink"/>
        </li>
    </g:if>

</ul>
<section id="show-medicamento" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.medicamento.label" default="Medicamento" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "medicamento")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.advertencias.label" default="Advertencias" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "advertencias")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.composicion.label" default="Composicion" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "composicion")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.conservacion.label" default="Conservacion" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "conservacion")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.contraindicaciones.label" default="Contraindicaciones" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "contraindicaciones")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.descripcion.label" default="Descripcion" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "descripcion")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.dosificacion.label" default="Dosificacion" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "dosificacion")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.farmacología.label" default="Farmacología" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "farmacología")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.farmacologico.label" default="Farmacologico" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${medicamentoInstance.farmacologico}" var="f">
						<li><g:link controller="farmacologico" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.indicaciones.label" default="Indicaciones" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "indicaciones")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.interacciones.label" default="Interacciones" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "interacciones")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.laboratorios.label" default="Laboratorios" /></td>
				
				<td valign="top" class="value"><g:link controller="laboratorios" action="show" id="${medicamentoInstance?.laboratorios?.id}">${medicamentoInstance?.laboratorios?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.nombre.label" default="Nombre" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "nombre")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.nota.label" default="Nota" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "nota")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.precauciones.label" default="Precauciones" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "precauciones")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.presentaciones.label" default="Presentaciones" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "presentaciones")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.principio.label" default="Principio" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${medicamentoInstance.principio}" var="p">
						<li><g:link controller="principio" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.propiedades.label" default="Propiedades" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "propiedades")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.reaccionesAdversas.label" default="Reacciones Adversas" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "reaccionesAdversas")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.sobredosificacion.label" default="Sobredosificacion" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "sobredosificacion")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medicamento.total.label" default="Total" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicamentoInstance, field: "total")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
