
<%@ page import="historiamedica.Medicamento" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medicamento.label', default: 'Medicamento')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-medicamento" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="medicamento" title="${message(code: 'medicamento.medicamento.label', default: 'Medicamento')}" />
			
				<g:sortableColumn property="advertencias" title="${message(code: 'medicamento.advertencias.label', default: 'Advertencias')}" />
			
				<g:sortableColumn property="composicion" title="${message(code: 'medicamento.composicion.label', default: 'Composicion')}" />
			
				<g:sortableColumn property="conservacion" title="${message(code: 'medicamento.conservacion.label', default: 'Conservacion')}" />
			
				<g:sortableColumn property="contraindicaciones" title="${message(code: 'medicamento.contraindicaciones.label', default: 'Contraindicaciones')}" />
			
				<g:sortableColumn property="descripcion" title="${message(code: 'medicamento.descripcion.label', default: 'Descripcion')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${medicamentoInstanceList}" status="i" var="medicamentoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${medicamentoInstance.id}">${fieldValue(bean: medicamentoInstance, field: "medicamento")}</g:link></td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "advertencias")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "composicion")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "conservacion")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "contraindicaciones")}</td>
			
				<td>${fieldValue(bean: medicamentoInstance, field: "descripcion")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${medicamentoInstanceCount}" />
	</div>
</section>

</body>

</html>
