<%@ page import="historiamedica.Medicamento" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medicamento.label', default: 'Medicamento')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

    <g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

    <li class="${ params.action == "list" ? 'active' : '' }">
        <g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
    </li>
    <li class="${ params.action == "create" ? 'active' : '' }">
        <g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
    </li>

    <g:if test="${ params.action == 'show' || params.action == 'edit' }">
        <!-- the item is an object (not a list) -->
        <li class="${ params.action == "edit" ? 'active' : '' }">
            <g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
        </li>
        <li class="">
            <g:render template="/_common/modals/deleteTextLink"/>
        </li>
    </g:if>

</ul>
<section id="edit-medicamento" class="first">

	<g:hasErrors bean="${medicamentoInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${medicamentoInstance}" as="list" />
	</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" >
		<g:hiddenField name="id" value="${medicamentoInstance?.id}" />
		<g:hiddenField name="version" value="${medicamentoInstance?.version}" />
		<fieldset class="form">
			<g:render template="form"/>
		</fieldset>
		<div class="form-actions">
			<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
			<g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
            <button class="btn" type="reset">Cancel</button>
		</div>
	</g:form>

</section>
			
</body>

</html>
