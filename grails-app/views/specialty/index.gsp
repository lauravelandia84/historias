
<%@ page import="historiamedica.Specialty" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'specialty.label', default: 'Specialty')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-specialty" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'specialty.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="description" title="${message(code: 'specialty.description.label', default: 'Description')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${specialtyInstanceList}" status="i" var="specialtyInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${specialtyInstance.id}">${fieldValue(bean: specialtyInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: specialtyInstance, field: "description")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${specialtyInstanceCount}" />
	</div>
</section>

</body>

</html>
