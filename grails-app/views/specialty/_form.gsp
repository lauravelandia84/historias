<%@ page import="historiamedica.Specialty" %>



			<div class="control-group fieldcontain ${hasErrors(bean: specialtyInstance, field: 'code', 'error')} required">
				<label for="code" class="control-label"><g:message code="specialty.code.label" default="Code" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="code" required="" value="${specialtyInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: specialtyInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: specialtyInstance, field: 'description', 'error')} required">
				<label for="description" class="control-label"><g:message code="specialty.description.label" default="Description" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="description" required="" value="${specialtyInstance?.description}"/>
					<span class="help-inline">${hasErrors(bean: specialtyInstance, field: 'description', 'error')}</span>
				</div>
			</div>

