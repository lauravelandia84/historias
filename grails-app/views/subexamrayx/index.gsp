
<%@ page import="historiamedica.Subexamrayx" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'subexamrayx.label', default: 'Subexamrayx')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-subexamrayx" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'subexamrayx.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="description" title="${message(code: 'subexamrayx.description.label', default: 'Description')}" />
			
				<th><g:message code="subexamrayx.examrayx.label" default="Examrayx" /></th>
			
				<g:sortableColumn property="idline" title="${message(code: 'subexamrayx.idline.label', default: 'Idline')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${subexamrayxInstanceList}" status="i" var="subexamrayxInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${subexamrayxInstance.id}">${fieldValue(bean: subexamrayxInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: subexamrayxInstance, field: "description")}</td>
			
				<td>${fieldValue(bean: subexamrayxInstance, field: "examrayx")}</td>
			
				<td>${fieldValue(bean: subexamrayxInstance, field: "idline")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${subexamrayxInstanceCount}" />
	</div>
</section>

</body>

</html>
