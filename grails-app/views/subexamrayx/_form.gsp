<%@ page import="historiamedica.Subexamrayx" %>



			<div class="control-group fieldcontain ${hasErrors(bean: subexamrayxInstance, field: 'code', 'error')} required">
				<label for="code" class="control-label"><g:message code="subexamrayx.code.label" default="Code" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="code" required="" value="${subexamrayxInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: subexamrayxInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: subexamrayxInstance, field: 'description', 'error')} ">
				<label for="description" class="control-label"><g:message code="subexamrayx.description.label" default="Description" /></label>
				<div class="controls">
					<g:textField name="description" value="${subexamrayxInstance?.description}"/>
					<span class="help-inline">${hasErrors(bean: subexamrayxInstance, field: 'description', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: subexamrayxInstance, field: 'idline', 'error')} ">
				<label for="idline" class="control-label"><g:message code="subexamrayx.idline.label" default="Idline" /></label>
				<div class="controls">
					<g:textField name="idline" value="${subexamrayxInstance?.idline}"/>
					<span class="help-inline">${hasErrors(bean: subexamrayxInstance, field: 'idline', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: subexamrayxInstance, field: 'examrayx', 'error')} required">
				<label for="examrayx" class="control-label"><g:message code="subexamrayx.examrayx.label" default="Examrayx" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="examrayx" name="examrayx.id" from="${historiamedica.Examrayx.list()}" optionKey="id" required="" value="${subexamrayxInstance?.examrayx?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: subexamrayxInstance, field: 'examrayx', 'error')}</span>
				</div>
			</div>
