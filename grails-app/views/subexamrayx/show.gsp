
<%@ page import="historiamedica.Subexamrayx" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'subexamrayx.label', default: 'Subexamrayx')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>
<section id="show-subexamrayx" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="subexamrayx.code.label" default="Code" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: subexamrayxInstance, field: "code")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="subexamrayx.description.label" default="Description" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: subexamrayxInstance, field: "description")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="subexamrayx.examrayx.label" default="Examrayx" /></td>
				
				<td valign="top" class="value"><g:link controller="examrayx" action="show" id="${subexamrayxInstance?.examrayx?.id}">${subexamrayxInstance?.examrayx?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="subexamrayx.idline.label" default="Idline" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: subexamrayxInstance, field: "idline")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
