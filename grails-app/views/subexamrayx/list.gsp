
<%@ page import="historiamedica.Subexamrayx" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'subexamrayx.label', default: 'Subexamrayx')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>	
<section id="list-subexamrayx" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'subexamrayx.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="description" title="${message(code: 'subexamrayx.description.label', default: 'Description')}" />
			
				<th><g:message code="subexamrayx.examrayx.label" default="Examrayx" /></th>
			
				<g:sortableColumn property="idline" title="${message(code: 'subexamrayx.idline.label', default: 'Idline')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${subexamrayxInstanceList}" status="i" var="subexamrayxInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${subexamrayxInstance.id}">${fieldValue(bean: subexamrayxInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: subexamrayxInstance, field: "description")}</td>
			
				<td>${fieldValue(bean: subexamrayxInstance, field: "examrayx")}</td>
			
				<td>${fieldValue(bean: subexamrayxInstance, field: "idline")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${subexamrayxInstanceTotal}" />
	</div>
</section>

</body>

</html>
