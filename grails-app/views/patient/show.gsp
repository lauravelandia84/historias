
<%@ page import="historiamedica.Patient" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'patient.label', default: 'Patient')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>
<section id="show-patient" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.name.label" default="Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "name")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.firtsName.label" default="Firts Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "firtsName")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.gender.label" default="Gender" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "gender")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.statusMarital.label" default="Status Marital" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "statusMarital")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.groupSanguineo.label" default="Group Sanguineo" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "groupSanguineo")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.image.label" default="Image" /></td>
				<td valign="top" class="value"><img src="/historiamedica/patient/showImage/${params.id}" width="128" height="128"></td>
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.address.label" default="Address" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "address")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.allergy.label" default="Allergy" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "allergy")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.cedula.label" default="Cedula" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "cedula")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.country.label" default="Country" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "country")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.dateBirth.label" default="Date Birth" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${patientInstance?.dateBirth}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.dateHistory.label" default="Date History" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${patientInstance?.dateHistory}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.insurance.label" default="Insurance" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "insurance")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.numberInsurance.label" default="Number Insurance" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "numberInsurance")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.phone.label" default="Phone" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "phone")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.placeBirth.label" default="Place Birth" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "placeBirth")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.referedTo.label" default="Refered To" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "referedTo")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.represent.label" default="Represent" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: patientInstance, field: "represent")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="patient.seguros.label" default="Seguros" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${patientInstance.seguros}" var="s">
						<li><g:link controller="insurance" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
