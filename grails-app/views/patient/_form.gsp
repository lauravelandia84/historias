<%@ page import="historiamedica.Patient" %>



			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="patient.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${patientInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'firtsName', 'error')} required">
				<label for="firtsName" class="control-label"><g:message code="patient.firtsName.label" default="Firts Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="firtsName" required="" value="${patientInstance?.firtsName}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'firtsName', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'gender', 'error')} ">
				<label for="gender" class="control-label"><g:message code="patient.gender.label" default="Gender" /></label>
				<div class="controls">
					<g:select name="gender" from="${patientInstance.constraints.gender.inList}" value="${patientInstance?.gender}" valueMessagePrefix="patient.gender" noSelection="['': '']"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'gender', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'statusMarital', 'error')} ">
				<label for="statusMarital" class="control-label"><g:message code="patient.statusMarital.label" default="Status Marital" /></label>
				<div class="controls">
					<g:select name="statusMarital" from="${patientInstance.constraints.statusMarital.inList}" value="${patientInstance?.statusMarital}" valueMessagePrefix="patient.statusMarital" noSelection="['': '']"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'statusMarital', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'groupSanguineo', 'error')} ">
				<label for="groupSanguineo" class="control-label"><g:message code="patient.groupSanguineo.label" default="Group Sanguineo" /></label>
				<div class="controls">
					<g:select name="groupSanguineo" from="${patientInstance.constraints.groupSanguineo.inList}" value="${patientInstance?.groupSanguineo}" valueMessagePrefix="patient.groupSanguineo" noSelection="['': '']"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'groupSanguineo', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'image', 'error')} ">
				<label for="image" class="control-label"><g:message code="patient.image.label" default="Image" /></label>
				<div class="controls">
					<input type="file" id="image" name="image" />
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'image', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'address', 'error')} ">
				<label for="address" class="control-label"><g:message code="patient.address.label" default="Address" /></label>
				<div class="controls">
					<g:textField name="address" value="${patientInstance?.address}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'address', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'allergy', 'error')} ">
				<label for="allergy" class="control-label"><g:message code="patient.allergy.label" default="Allergy" /></label>
				<div class="controls">
					<g:textField name="allergy" value="${patientInstance?.allergy}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'allergy', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'cedula', 'error')} ">
				<label for="cedula" class="control-label"><g:message code="patient.cedula.label" default="Cedula" /></label>
				<div class="controls">
					<g:textField name="cedula" value="${patientInstance?.cedula}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'cedula', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'country', 'error')} ">
				<label for="country" class="control-label"><g:message code="patient.country.label" default="Country" /></label>
				<div class="controls">
					<g:textField name="country" value="${patientInstance?.country}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'country', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'dateBirth', 'error')} required">
				<label for="dateBirth" class="control-label"><g:message code="patient.dateBirth.label" default="Date Birth" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="dateBirth" precision="day"  value="${patientInstance?.dateBirth}"  />
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'dateBirth', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'dateHistory', 'error')} required">
				<label for="dateHistory" class="control-label"><g:message code="patient.dateHistory.label" default="Date History" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="dateHistory" precision="day"  value="${patientInstance?.dateHistory}"  />
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'dateHistory', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'email', 'error')} ">
				<label for="email" class="control-label"><g:message code="patient.email.label" default="Email" /></label>
				<div class="controls">
					<g:textField name="email" value="${patientInstance?.email}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'email', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'insurance', 'error')} ">
				<label for="insurance" class="control-label"><g:message code="patient.insurance.label" default="Insurance" /></label>
				<div class="controls">
					<g:textField name="insurance" value="${patientInstance?.insurance}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'insurance', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'numberInsurance', 'error')} ">
				<label for="numberInsurance" class="control-label"><g:message code="patient.numberInsurance.label" default="Number Insurance" /></label>
				<div class="controls">
					<g:textField name="numberInsurance" value="${patientInstance?.numberInsurance}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'numberInsurance', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'phone', 'error')} ">
				<label for="phone" class="control-label"><g:message code="patient.phone.label" default="Phone" /></label>
				<div class="controls">
					<g:textField name="phone" value="${patientInstance?.phone}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'phone', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'placeBirth', 'error')} ">
				<label for="placeBirth" class="control-label"><g:message code="patient.placeBirth.label" default="Place Birth" /></label>
				<div class="controls">
					<g:textField name="placeBirth" value="${patientInstance?.placeBirth}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'placeBirth', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'referedTo', 'error')} ">
				<label for="referedTo" class="control-label"><g:message code="patient.referedTo.label" default="Refered To" /></label>
				<div class="controls">
					<g:textField name="referedTo" value="${patientInstance?.referedTo}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'referedTo', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'represent', 'error')} ">
				<label for="represent" class="control-label"><g:message code="patient.represent.label" default="Represent" /></label>
				<div class="controls">
					<g:textField name="represent" value="${patientInstance?.represent}"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'represent', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: patientInstance, field: 'seguros', 'error')} ">
				<label for="seguros" class="control-label"><g:message code="patient.seguros.label" default="Seguros" /></label>
				<div class="controls">
					<g:select name="seguros" from="${historiamedica.Insurance.list()}" multiple="multiple" optionKey="id" size="5" value="${patientInstance?.seguros*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: patientInstance, field: 'seguros', 'error')}</span>
				</div>
			</div>

