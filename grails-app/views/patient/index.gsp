
<%@ page import="historiamedica.Patient" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'patient.label', default: 'Patient')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-patient" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'patient.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="firtsName" title="${message(code: 'patient.firtsName.label', default: 'Firts Name')}" />
			
				<g:sortableColumn property="gender" title="${message(code: 'patient.gender.label', default: 'Gender')}" />
			
				<g:sortableColumn property="statusMarital" title="${message(code: 'patient.statusMarital.label', default: 'Status Marital')}" />
			
				<g:sortableColumn property="groupSanguineo" title="${message(code: 'patient.groupSanguineo.label', default: 'Group Sanguineo')}" />
			
				<g:sortableColumn property="image" title="${message(code: 'patient.image.label', default: 'Image')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${patientInstanceList}" status="i" var="patientInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${patientInstance.id}">${fieldValue(bean: patientInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: patientInstance, field: "firtsName")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "gender")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "statusMarital")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "groupSanguineo")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "image")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${patientInstanceCount}" />
	</div>
</section>

</body>

</html>
