<%@ page import="historiamedica.Consultation" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'patient.label', default: 'Pacientes')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<ul id="Menu" class="nav nav-pills">
		<li>
							<g:link class="list1" action="list1" args="[entityName]">
								<i class="icon-list"></i>
								<g:message code="Buscar Pacientes por fecha de Consulta"/>
							</g:link>
						</li>
		</li>
		<li>
							<g:link class="list2" action="list2">
								<i class="icon-list"></i>
								<g:message code="Buscar Pacientes por medico" args="[entityName]" />
							</g:link>
						</li>
		</li>
		<li>
							<g:link class="list3" action="list3">
								<i class="icon-list"></i>
								<g:message code="Buscar Pacientes por especialidad" args="[entityName]" />
							</g:link>
						</li>
		</li>
	</ul>

<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list3" method="GET">
        <div class="fieldcontain">
            <label for="query"><b>Buscar por Pacientes por especialidad:</label> 
            <g:textField  name="description" value="${params.query}" />
            <div class="form-actions">
			<g:submitButton name="search" class="btn btn-primary" value="Buscar" />
			<g:link action="downList3" params="[export: 'pdf', description: params.description, search: params.search]">Descargar</g:link>
		</div>
        </div>
    </g:form>
</fieldset>
<table>	
<section id="list-patient" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'patient.name.label', default: 'Nombres')}" />
			
				<g:sortableColumn property="firtsName" title="${message(code: 'patient.firtsName.label', default: 'Apellidos')}" />
			
				<g:sortableColumn property="gender" title="${message(code: 'patient.gender.label', default: 'Sexo')}" />
			
				<g:sortableColumn property="statusMarital" title="${message(code: 'patient.statusMarital.label', default: 'Estado Civil')}" />
			
				<g:sortableColumn property="groupSanguineo" title="${message(code: 'patient.groupSanguineo.label', default: 'Grupo Sanguineo')}" />
			
				<g:sortableColumn property="allergy" title="${message(code: 'patient.allergy.label', default: 'Alergias')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${consultationInstanceList}" status="i" var="consultationInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td>${consultationInstance[0]}</td>
			
				<td>${consultationInstance[1]}</td>
			
				<td>${consultationInstance[2]}</td>

				<td>${consultationInstance[3]}</td>

				<td>${consultationInstance[4]}</td>

				<td>${consultationInstance[5]}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	
</section>

</body>

</html>
