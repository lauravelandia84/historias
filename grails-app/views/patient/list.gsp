
<%@ page import="historiamedica.Patient" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'patient.label', default: 'Patient')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>
<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list" method="GET">
        <div class="fieldcontain">
            <label for="query"><b>Buscar por Nombre del Paciente:</label>
            <g:textField name="query" value="${params.query}"/>
            
        </div>
    </g:form>
</fieldset>
<table>

<section id="list-patient" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'patient.name.label', default: 'Nombres')}" />
			
				<g:sortableColumn property="firtsName" title="${message(code: 'patient.firtsName.label', default: 'Apellidos')}" />
			
				<g:sortableColumn property="gender" title="${message(code: 'patient.gender.label', default: 'Sexo')}" />
			
				<g:sortableColumn property="statusMarital" title="${message(code: 'patient.statusMarital.label', default: 'Estado Civil')}" />
			
				<g:sortableColumn property="groupSanguineo" title="${message(code: 'patient.groupSanguineo.label', default: 'Grupo Sanguineo')}" />
			
				<g:sortableColumn property="allergy" title="${message(code: 'patient.allergy.label', default: 'Alergias')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${patientInstanceList}" status="i" var="patientInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${patientInstance.id}">${fieldValue(bean: patientInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: patientInstance, field: "firtsName")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "gender")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "statusMarital")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "groupSanguineo")}</td>
			
				<td>${fieldValue(bean: patientInstance, field: "allergy")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	
</section>

</body>

</html>
