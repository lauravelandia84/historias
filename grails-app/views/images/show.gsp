
<%@ page import="historiamedica.Images" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'images.label', default: 'Images')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-images" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="images.filename.label" default="Filename" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: imagesInstance, field: "filename")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="images.image.label" default="Image" /></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
