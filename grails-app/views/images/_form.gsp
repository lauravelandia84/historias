<%@ page import="historiamedica.Images" %>



			<div class="control-group fieldcontain ${hasErrors(bean: imagesInstance, field: 'filename', 'error')} ">
				<label for="filename" class="control-label"><g:message code="images.filename.label" default="Filename" /></label>
				<div class="controls">
					<g:textField name="filename" value="${imagesInstance?.filename}"/>
					<span class="help-inline">${hasErrors(bean: imagesInstance, field: 'filename', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: imagesInstance, field: 'image', 'error')} required">
				<label for="image" class="control-label"><g:message code="images.image.label" default="Image" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<input type="file" id="image" name="image" />
					<span class="help-inline">${hasErrors(bean: imagesInstance, field: 'image', 'error')}</span>
				</div>
			</div>

