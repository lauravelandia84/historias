
<%@ page import="historiamedica.Images" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'images.label', default: 'Images')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-images" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="filename" title="${message(code: 'images.filename.label', default: 'Filename')}" />
			
				<g:sortableColumn property="image" title="${message(code: 'images.image.label', default: 'Image')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${imagesInstanceList}" status="i" var="imagesInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${imagesInstance.id}">${fieldValue(bean: imagesInstance, field: "filename")}</g:link></td>
			
				<td>${fieldValue(bean: imagesInstance, field: "image")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${imagesInstanceTotal}" />
	</div>
</section>

</body>

</html>
