
<%@ page import="historiamedica.Images" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'images.label', default: 'Images')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-images" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="filename" title="${message(code: 'images.filename.label', default: 'Filename')}" />
			
				<g:sortableColumn property="image" title="${message(code: 'images.image.label', default: 'Image')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${imagesInstanceList}" status="i" var="imagesInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${imagesInstance.id}">${fieldValue(bean: imagesInstance, field: "filename")}</g:link></td>
			
				<td>${fieldValue(bean: imagesInstance, field: "image")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${imagesInstanceCount}" />
	</div>
</section>

</body>

</html>
