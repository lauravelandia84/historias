
<%@ page import="historiamedica.Typedocument" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'typedocument.label', default: 'Typedocument')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-typedocument" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'typedocument.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="name" title="${message(code: 'typedocument.name.label', default: 'Name')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${typedocumentInstanceList}" status="i" var="typedocumentInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${typedocumentInstance.id}">${fieldValue(bean: typedocumentInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: typedocumentInstance, field: "name")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${typedocumentInstanceCount}" />
	</div>
</section>

</body>

</html>
