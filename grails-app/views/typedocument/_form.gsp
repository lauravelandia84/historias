<%@ page import="historiamedica.Typedocument" %>



			<div class="control-group fieldcontain ${hasErrors(bean: typedocumentInstance, field: 'code', 'error')} required">
				<label for="code" class="control-label"><g:message code="typedocument.code.label" default="Code" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="code" required="" value="${typedocumentInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: typedocumentInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: typedocumentInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="typedocument.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${typedocumentInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: typedocumentInstance, field: 'name', 'error')}</span>
				</div>
			</div>

