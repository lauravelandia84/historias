<ul class="nav pull-right">
	<li class="dropdown">
		<a class="dropdown-toggle" href="#"><g:message code="default.info.label"/> <b class="caret"></b></a>
		<ul class="dropdown-menu">
		
			<%-- Note: Links to pages without controller are redirected in conf/UrlMappings.groovy --%>
			<li class=""><a href="${createLink(uri: '/patient/list1')}"><b>Listado de Pacientes</b></a></li>
			<li class=""><a href="${createLink(uri: '/medico/list1')}"><b>Listado de Médicos</b></a></li>
			<li class=""><a href="${createLink(uri: '/tdocument/list1')}"><b>Documentos Médicos</b></a></li>
			<li class="dropdown-submenu">
				<a href="#" class="dropdown-toggle">
					<b>Exámenes Médicos</b>
				</a>
				<ul class="dropdown-menu dropdown-menu-dark">
					<li><a class="" title="Vertical" href="${createLink(uri: '/consultation/list1')}"><b>
						<g:message code="default.layout.fluid.label" default="Exámenes de Laboratorio"/></b>
					</a></li>
					<li><a class="" title="Horizontal" href="${createLink(uri: '/consultation/list2')}"><b>
						<g:message code="default.layout.fluid.label" default="Exámenes de Rayos X "/></b>
					</a></li>
				</ul>
			</li>
			<li class=""><a href="${createLink(uri: '/consultation/list3')}"><b>Récipes</b></a></li>
						
<%--			<li class="divider"></li>--%>
<%--			<li class=""><a href="${createLink(uri: '/imprint')}">Imprint</a></li>--%>
<%--			<li class=""><a href="${createLink(uri: '/terms')}"><i>Terms of Use</i></a></li>--%>
		</ul>
	</li>
</ul>
