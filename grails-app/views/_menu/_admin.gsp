<ul class="nav pull-right">
	<li class="dropdown">
		<a class="dropdown-toggle" data-toggle="dropdown" href="#"><g:message code="default.admin.label"/><b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li class="">
				<a tabindex="-1" href="${createLink(uri: '/patient')}"><b>Historia Médica</b></a>
			</li>
			<li class="">
				<a tabindex="-1" href="${createLink(uri: '/consultation')}"><b>Consulta Médica</b></a>
			</li>
			<li class="">
				<a tabindex="-1" href="${createLink(uri: '/tdocument')}"><b>Documentos Médicos</b></a>
			</li>
		</ul>
	</li>
</ul>
