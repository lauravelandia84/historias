<ul class="nav pull-right">
	<li class="dropdown">
	<a class="dropdown-toggle" data-toggle="dropdown" href="#"><g:message code="default.config.label"/><b class="caret"></b></a>
		<ul class="dropdown-menu">
            <li class=""><a href="${createLink(uri: '/usuario')}"><b>Usuarios</b></a></li>
			<li class=""><a href="${createLink(uri: '/company')}"><b>Compañía</b></a></li>
			<li class=""><a href="${createLink(uri: '/insurance')}"><b>Seguros</b></a></li>
			<li class=""><a href="${createLink(uri: '/medico')}"><b>Médicos</b></a></li>
			<li class=""><a href="${createLink(uri: '/specialty')}"><b>Especialidad</b></a></li>
			<li class=""><a href="${createLink(uri: '/consultationcause')}"><b>Motivo de Consulta</b></a></li>
			<li class=""><a href="${createLink(uri: '/typeconsultation')}"><b>Tipo de Consulta</b></a></li>
			<li class=""><a href="${createLink(uri: '/typedocument')}"><b>Tipo de Documentos</b></a></li>
			<li class=""><a href="${createLink(uri: '/tn1icd10')}"><b>Diagnósticos</b></a></li>
			<li class="dropdown-submenu">
				<a href="#" class="dropdown-toggle">
					<b>Vademecum</b>
				</a>
				<ul class="dropdown-menu dropdown-menu-dark">
					<li><a class="" title="Vertical" href="${createLink(uri: '/laboratorios')}"><b>
						<g:message code="default.layout.fluid.label" default="Laboratorios"/></b>
					</a></li>
					<li><a class="" title="Horizontal" href="${createLink(uri: '/medicamento')}"><b>
						<g:message code="default.layout.fluid.label" default="Medicamentos"/></b>
					</a></li>
					<li><a class="" title="Horizontal" href="${createLink(uri: '/principio')}"><b>
						<g:message code="default.layout.fluid.label" default="Principio"/></b>
					</a></li>
					<li><a class="" title="Horizontal" href="${createLink(uri: '/farmacologico')}"><b>
						<g:message code="default.layout.fluid.label" default="Farmacológico"/></b>
					</a></li>
				</ul>
			</li>
			<li class="dropdown-submenu">
				<a href="#" class="dropdown-toggle">
					<b>Exámenes</b>
				</a>
				<ul class="dropdown-menu dropdown-menu-dark">
					<li><a class="" title="Vertical" href="${createLink(uri: '/examlab')}"><b>
						<g:message code="default.layout.fluid.label" default="Exámen de Laboratorio"/></b>
					</a></li>
					<li><a class="" title="Horizontal" href="${createLink(uri: '/examrayx')}"><b>
						<g:message code="default.layout.fluid.label" default="Exámen de Rayos X"/></b>
					</a></li>
				</ul>
			</li>
		
		</ul>
	</li>
</ul>

<noscript>
<ul class="nav pull-right">
	<li class="">
		<g:link controller="user" action="config"><g:message code="user.config.label" default="Config"/></g:link>
	</li>
</ul>
</noscript>

