<label for="subexamlab" class="control-label">Examen de Laboratorio</label><br><br>
<div class="controls"  style="margin-left: 15px;">
    <g:select id="subexamlab" name="subexamlab.id" multiple="multiple" from="${subexamlabList}" optionKey="id" size="10"
              onchange="agregarExamen(this.value, this.options[this.selectedIndex].text)" />
</div>