<%@ page import="historiamedica.Subexamlab" %>



			<div class="control-group fieldcontain ${hasErrors(bean: subexamlabInstance, field: 'code', 'error')} ">
				<label for="code" class="control-label"><g:message code="subexamlab.code.label" default="Code" /></label>
				<div class="controls">
					<g:textField name="code" value="${subexamlabInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: subexamlabInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: subexamlabInstance, field: 'description', 'error')} ">
				<label for="description" class="control-label"><g:message code="subexamlab.description.label" default="Description" /></label>
				<div class="controls">
					<g:textField name="description" value="${subexamlabInstance?.description}"/>
					<span class="help-inline">${hasErrors(bean: subexamlabInstance, field: 'description', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: subexamlabInstance, field: 'idline', 'error')} ">
				<label for="idline" class="control-label"><g:message code="subexamlab.idline.label" default="Idline" /></label>
				<div class="controls">
					<g:textField name="idline" value="${subexamlabInstance?.idline}"/>
					<span class="help-inline">${hasErrors(bean: subexamlabInstance, field: 'idline', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: subexamlabInstance, field: 'examlab', 'error')} required">
				<label for="examlab" class="control-label"><g:message code="subexamlab.examlab.label" default="Examlab" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="examlab" name="examlab.id" from="${historiamedica.Examlab.list()}" optionKey="id" required="" value="${subexamlabInstance?.examlab?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: subexamlabInstance, field: 'examlab', 'error')}</span>
				</div>
			</div>