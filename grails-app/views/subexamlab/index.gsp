
<%@ page import="historiamedica.Subexamlab" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'subexamlab.label', default: 'Subexamlab')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-subexamlab" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'subexamlab.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="description" title="${message(code: 'subexamlab.description.label', default: 'Description')}" />
			
				<th><g:message code="subexamlab.examlab.label" default="Examlab" /></th>
			
				<g:sortableColumn property="idline" title="${message(code: 'subexamlab.idline.label', default: 'Idline')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${subexamlabInstanceList}" status="i" var="subexamlabInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${subexamlabInstance.id}">${fieldValue(bean: subexamlabInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: subexamlabInstance, field: "description")}</td>
			
				<td>${fieldValue(bean: subexamlabInstance, field: "examlab")}</td>
			
				<td>${fieldValue(bean: subexamlabInstance, field: "idline")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${subexamlabInstanceCount}" />
	</div>
</section>

</body>

</html>
