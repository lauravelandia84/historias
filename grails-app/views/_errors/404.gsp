<html>
	<head>
		<title>Error 404 - Página No Encontrada!</title>
		<meta name="layout" content="kickstart" />
	</head>

<body>
	<content tag="header">
		<!-- Empty Header -->
	</content>
	
  	<section id="Error" class="">
		<div class="big-message">
			<div class="container">
				<h1>Página no Encontrada</h1>
				<h2>Error: 404 Not Found</h2>
				<p>
					Lo sentimos, ocurrio un error, la página solicitada no existe!
				</p>
				
				<div class="actions">
					<a href="${createLink(uri: '/')}" class="btn btn-large btn-primary">
						<i class="icon-chevron-left icon-white"></i>
						Inicio
					</a>
					<%--<a href="/contact" class="btn btn-large btn-success">
						<i class="icon-envelope"></i>
						Contact Support
					</a>--%>
				</div>
			</div>
		</div>
	</section>
  
  
  </body>
</html>