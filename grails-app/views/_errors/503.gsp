<html>
	<head>
		<title>Error 503 - Servicio No Disponible!</title>
		<meta name="layout" content="kickstart" />
	</head>

<body>
	<content tag="header">
		<!-- Empty Header -->
	</content>
	
  	<section id="Error" class="">
		<div class="big-message">
			<div class="container">
				<h1>Servicio no Disponible!</h1>
				<h2>Error: 503 Service Unavailable</h2>
				<p>
					Lo sientimos, temporalmente el servidor no puede atender solicitudes debido a sobrecarga o a mantenimiento!
                    Intente nuevamente más tarde.
				</p>
				
				<div class="actions">
					<a href="${createLink(uri: '/')}" class="btn btn-large btn-primary">
						<i class="icon-chevron-left icon-white"></i>
						Inicio
					</a>
					<%--<a href="/contact" class="btn btn-large btn-success">
						<i class="icon-envelope"></i>
						Contact Support
					</a>--%>
				</div>
			</div>
		</div>
	</section>
  
  
  </body>
</html>