<footer class="footer">
	<div class="container">
		<div class="row">
			<div class="span2">
				<h4>Producto</h4>
				<ul class="unstyled">
					<li>
						<a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a>
					</li>
					<li>
						<a href="${createLink(uri: '/')}"><g:message code="default.tour.label"/></a>
					</li>
					<li>
						<a href="${createLink(uri: '/')}"><g:message code="default.pricing.label"/></a>
					</li>
					<li>
						<a href="${createLink(uri: '/')}"><g:message code="default.faq.label"/></a>
					</li>
				</ul>
			</div>
			<div class="span2">
				<h4>Compañia</h4>
				<ul class="unstyled">
					<li><a href="${createLink(uri: '/about')}"><g:message code="default.about.label"/></a></li>
					<li><a href="${createLink(uri: '/contact')}"><g:message code="default.contact.label"/></a></li>
				</ul>
			</div>
			<div class="span8">
				<h4> Información </h4>
				<p>Diseñado para centros médicos, consultorios y clínicas que requieren de un sistema de gestión de historias médicas electrónica, a fin de manejar de forma rápida y eficiente los datos correspondientes a los pacientes</a>
			</div>
		</div>
		<h4>Aviso de Confidencialidad</h4>
		<p>Este sitio Web puede contener avisos sobre productos propios e información de copyright, cuyos términos deben observarse y seguirse. La información sobre este sitio Web puede contener imprecisiones técnicas o errores tipográficos. La información puede modificarse o actualizarse sin previo aviso. 
		</p>
		<p>El operador y el autor también puede realizar mejoras y / o cambios en los productos y / o programas descritos en esta información en cualquier momento sin previo aviso. Para los documentos y software disponibles desde este servidor, el operador y el autor no garantiza ni asume ninguna responsabilidad legal o responsabilidad por la exactitud, integridad o utilidad de cualquier información, aparato, producto o proceso divulgado.
		</p>
	
		<p class="pull-right"><a href="#">Volver arriba</a></p>
	</div>
</footer>
