
<%@ page import="historiamedica.Company" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'company.label', default: 'Company')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>
<section id="list-company" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'company.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'company.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="email" title="${message(code: 'company.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="nif" title="${message(code: 'company.nif.label', default: 'Nif')}" />
			
				<g:sortableColumn property="phone" title="${message(code: 'company.phone.label', default: 'Phone')}" />
			
				<g:sortableColumn property="rif" title="${message(code: 'company.rif.label', default: 'Rif')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${companyInstanceList}" status="i" var="companyInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${companyInstance.id}">${fieldValue(bean: companyInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: companyInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "email")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "nif")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "phone")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "rif")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${companyInstanceTotal}" />
	</div>
</section>

</body>

</html>
