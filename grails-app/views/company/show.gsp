
<%@ page import="historiamedica.Company" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'company.label', default: 'Company')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>	
<section id="show-company" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="company.name.label" default="Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: companyInstance, field: "name")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="company.address.label" default="Address" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: companyInstance, field: "address")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="company.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: companyInstance, field: "email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="company.medico.label" default="Medico" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${companyInstance.medico}" var="m">
						<li><g:link controller="medico" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="company.nif.label" default="Nif" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: companyInstance, field: "nif")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="company.phone.label" default="Phone" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: companyInstance, field: "phone")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="company.rif.label" default="Rif" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: companyInstance, field: "rif")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
