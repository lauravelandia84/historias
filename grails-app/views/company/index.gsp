
<%@ page import="historiamedica.Company" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'company.label', default: 'Company')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-company" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'company.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'company.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="email" title="${message(code: 'company.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="nif" title="${message(code: 'company.nif.label', default: 'Nif')}" />
			
				<g:sortableColumn property="phone" title="${message(code: 'company.phone.label', default: 'Phone')}" />
			
				<g:sortableColumn property="rif" title="${message(code: 'company.rif.label', default: 'Rif')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${companyInstanceList}" status="i" var="companyInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${companyInstance.id}">${fieldValue(bean: companyInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: companyInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "email")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "nif")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "phone")}</td>
			
				<td>${fieldValue(bean: companyInstance, field: "rif")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${companyInstanceCount}" />
	</div>
</section>

</body>

</html>
