<%@ page import="historiamedica.Company" %>



			<div class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="company.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${companyInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: companyInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'address', 'error')} ">
				<label for="address" class="control-label"><g:message code="company.address.label" default="Address" /></label>
				<div class="controls">
					<g:textField name="address" value="${companyInstance?.address}"/>
					<span class="help-inline">${hasErrors(bean: companyInstance, field: 'address', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'email', 'error')} ">
				<label for="email" class="control-label"><g:message code="company.email.label" default="Email" /></label>
				<div class="controls">
					<g:textField name="email" value="${companyInstance?.email}"/>
					<span class="help-inline">${hasErrors(bean: companyInstance, field: 'email', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'medico', 'error')} ">
				<label for="medico" class="control-label"><g:message code="company.medico.label" default="Medico" /></label>
				<div class="controls">
					<g:select name="medico" from="${historiamedica.Medico.list()}" multiple="multiple" optionKey="id" size="5" value="${companyInstance?.medico*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: companyInstance, field: 'medico', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'nif', 'error')} ">
				<label for="nif" class="control-label"><g:message code="company.nif.label" default="Nif" /></label>
				<div class="controls">
					<g:textField name="nif" value="${companyInstance?.nif}"/>
					<span class="help-inline">${hasErrors(bean: companyInstance, field: 'nif', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'phone', 'error')} ">
				<label for="phone" class="control-label"><g:message code="company.phone.label" default="Phone" /></label>
				<div class="controls">
					<g:textField name="phone" value="${companyInstance?.phone}"/>
					<span class="help-inline">${hasErrors(bean: companyInstance, field: 'phone', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: companyInstance, field: 'rif', 'error')} ">
				<label for="rif" class="control-label"><g:message code="company.rif.label" default="Rif" /></label>
				<div class="controls">
					<g:textField name="rif" value="${companyInstance?.rif}"/>
					<span class="help-inline">${hasErrors(bean: companyInstance, field: 'rif', 'error')}</span>
				</div>
			</div>

