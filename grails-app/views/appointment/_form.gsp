<%@ page import="historiamedica.Appointment" %>



			<div class="control-group fieldcontain ${hasErrors(bean: appointmentInstance, field: 'consulta', 'error')} ">
				<label for="consulta" class="control-label"><g:message code="appointment.consulta.label" default="Consulta" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${appointmentInstance?.consulta?}" var="c">
    <li><g:link controller="consultation" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="consultation" action="create" params="['appointment.id': appointmentInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'consultation.label', default: 'Consultation')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: appointmentInstance, field: 'consulta', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: appointmentInstance, field: 'medico', 'error')} required">
				<label for="medico" class="control-label"><g:message code="appointment.medico.label" default="Medico" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="medico" name="medico.id" from="${historiamedica.Medico.list()}" optionKey="id" required="" value="${appointmentInstance?.medico?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: appointmentInstance, field: 'medico', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: appointmentInstance, field: 'patient', 'error')} required">
				<label for="patient" class="control-label"><g:message code="appointment.patient.label" default="Patient" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="patient" name="patient.id" from="${historiamedica.Patient.list()}" optionKey="id" required="" value="${appointmentInstance?.patient?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: appointmentInstance, field: 'patient', 'error')}</span>
				</div>
			</div>

