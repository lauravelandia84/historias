
<%@ page import="historiamedica.Appointment" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'appointment.label', default: 'Appointment')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-appointment" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<th><g:message code="appointment.medico.label" default="Medico" /></th>
			
				<th><g:message code="appointment.patient.label" default="Patient" /></th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${appointmentInstanceList}" status="i" var="appointmentInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${appointmentInstance.id}">${fieldValue(bean: appointmentInstance, field: "medico")}</g:link></td>
			
				<td>${fieldValue(bean: appointmentInstance, field: "patient")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${appointmentInstanceTotal}" />
	</div>
</section>

</body>

</html>
