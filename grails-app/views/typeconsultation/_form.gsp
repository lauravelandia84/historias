<%@ page import="historiamedica.Typeconsultation" %>



			<div class="control-group fieldcontain ${hasErrors(bean: typeconsultationInstance, field: 'code', 'error')} required">
				<label for="code" class="control-label"><g:message code="typeconsultation.code.label" default="Code" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="code" required="" value="${typeconsultationInstance?.code}"/>
					<span class="help-inline">${hasErrors(bean: typeconsultationInstance, field: 'code', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: typeconsultationInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="typeconsultation.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${typeconsultationInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: typeconsultationInstance, field: 'name', 'error')}</span>
				</div>
			</div>

