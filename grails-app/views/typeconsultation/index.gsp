
<%@ page import="historiamedica.Typeconsultation" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'typeconsultation.label', default: 'Typeconsultation')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-typeconsultation" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="code" title="${message(code: 'typeconsultation.code.label', default: 'Code')}" />
			
				<g:sortableColumn property="name" title="${message(code: 'typeconsultation.name.label', default: 'Name')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${typeconsultationInstanceList}" status="i" var="typeconsultationInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${typeconsultationInstance.id}">${fieldValue(bean: typeconsultationInstance, field: "code")}</g:link></td>
			
				<td>${fieldValue(bean: typeconsultationInstance, field: "name")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${typeconsultationInstanceCount}" />
	</div>
</section>

</body>

</html>
