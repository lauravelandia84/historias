
<%@ page import="historiamedica.Tn2icd10" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tn2icd10.label', default: 'Tn2icd10')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-tn2icd10" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="descripcion" title="${message(code: 'tn2icd10.descripcion.label', default: 'Descripcion')}" />
			
				<g:sortableColumn property="categoria" title="${message(code: 'tn2icd10.categoria.label', default: 'Categoria')}" />
			
				<g:sortableColumn property="subcategoria" title="${message(code: 'tn2icd10.subcategoria.label', default: 'Subcategoria')}" />
			
				<th><g:message code="tn2icd10.tn1icd10.label" default="Tn1icd10" /></th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${tn2icd10InstanceList}" status="i" var="tn2icd10Instance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${tn2icd10Instance.id}">${fieldValue(bean: tn2icd10Instance, field: "descripcion")}</g:link></td>
			
				<td>${fieldValue(bean: tn2icd10Instance, field: "categoria")}</td>
			
				<td>${fieldValue(bean: tn2icd10Instance, field: "subcategoria")}</td>
			
				<td>${fieldValue(bean: tn2icd10Instance, field: "tn1icd10")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${tn2icd10InstanceCount}" />
	</div>
</section>

</body>

</html>
