<label for="tn2icd10" class="control-label"><g:message code="consultation.tn3icd10.label" default="Tn2icd10" /></label><br><br>
<div class="controls"  style="margin-left: 15px;">
    <g:select id="tn2icd10" name="tn2icd10"  from="${tn2icd10List}" optionKey="id" size="10"
              onchange="${remoteFunction(controller: 'Tn3icd10', action: 'getTn3icd10', params: '\'id=\' + this.value', update:'tn3icd10Div')}"/>
</div>y