<%@ page import="historiamedica.Tn2icd10" %>



			<div class="control-group fieldcontain ${hasErrors(bean: tn2icd10Instance, field: 'descripcion', 'error')} required">
				<label for="descripcion" class="control-label"><g:message code="tn2icd10.descripcion.label" default="Descripcion" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="descripcion" required="" value="${tn2icd10Instance?.descripcion}"/>
					<span class="help-inline">${hasErrors(bean: tn2icd10Instance, field: 'descripcion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn2icd10Instance, field: 'categoria', 'error')} required">
				<label for="categoria" class="control-label"><g:message code="tn2icd10.categoria.label" default="Categoria" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="categoria" required="" value="${tn2icd10Instance?.categoria}"/>
					<span class="help-inline">${hasErrors(bean: tn2icd10Instance, field: 'categoria', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn2icd10Instance, field: 'subcategoria', 'error')} required">
				<label for="subcategoria" class="control-label"><g:message code="tn2icd10.subcategoria.label" default="Subcategoria" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="subcategoria" required="" value="${tn2icd10Instance?.subcategoria}"/>
					<span class="help-inline">${hasErrors(bean: tn2icd10Instance, field: 'subcategoria', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn2icd10Instance, field: 'tn1icd10', 'error')} required">
				<label for="tn1icd10" class="control-label"><g:message code="tn2icd10.tn1icd10.label" default="Tn1icd10" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="tn1icd10" name="tn1icd10.id" from="${historiamedica.Tn1icd10.list()}" optionKey="id" required="" value="${tn2icd10Instance?.tn1icd10?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: tn2icd10Instance, field: 'tn1icd10', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn2icd10Instance, field: 'tn3icd10', 'error')} ">
				<label for="tn3icd10" class="control-label"><g:message code="tn2icd10.tn3icd10.label" default="Tn3icd10" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${tn2icd10Instance?.tn3icd10?}" var="t">
    <li><g:link controller="tn3icd10" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="tn3icd10" action="create" params="['tn2icd10.id': tn2icd10Instance?.id]">${message(code: 'default.add.label', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: tn2icd10Instance, field: 'tn3icd10', 'error')}</span>
				</div>
			</div>

