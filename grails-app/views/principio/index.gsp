
<%@ page import="historiamedica.Principio" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'principio.label', default: 'Principio')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-principio" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="principio" title="${message(code: 'principio.principio.label', default: 'Principio')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${principioInstanceList}" status="i" var="principioInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${principioInstance.id}">${fieldValue(bean: principioInstance, field: "principio")}</g:link></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${principioInstanceCount}" />
	</div>
</section>

</body>

</html>
