<%@ page import="historiamedica.Principio" %>



			<div class="control-group fieldcontain ${hasErrors(bean: principioInstance, field: 'principio', 'error')} required">
				<label for="principio" class="control-label"><g:message code="principio.principio.label" default="Principio" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="principio" required="" value="${principioInstance?.principio}"/>
					<span class="help-inline">${hasErrors(bean: principioInstance, field: 'principio', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: principioInstance, field: 'medicamento', 'error')} ">
				<label for="medicamento" class="control-label"><g:message code="principio.medicamento.label" default="Medicamento" /></label>
				<div class="controls">
					
					<span class="help-inline">${hasErrors(bean: principioInstance, field: 'medicamento', 'error')}</span>
				</div>
			</div>

