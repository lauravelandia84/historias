<%@ page import="historiamedica.Tn1icd10" %>



			<div class="control-group fieldcontain ${hasErrors(bean: tn1icd10Instance, field: 'descripcion', 'error')} required">
				<label for="descripcion" class="control-label"><g:message code="tn1icd10.descripcion.label" default="Descripcion" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="descripcion" required="" value="${tn1icd10Instance?.descripcion}"/>
					<span class="help-inline">${hasErrors(bean: tn1icd10Instance, field: 'descripcion', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn1icd10Instance, field: 'categoria', 'error')} required">
				<label for="categoria" class="control-label"><g:message code="tn1icd10.categoria.label" default="Categoria" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="categoria" required="" value="${tn1icd10Instance?.categoria}"/>
					<span class="help-inline">${hasErrors(bean: tn1icd10Instance, field: 'categoria', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: tn1icd10Instance, field: 'tn2icd10', 'error')} ">
				<label for="tn2icd10" class="control-label"><g:message code="tn1icd10.tn2icd10.label" default="Tn2icd10" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${tn1icd10Instance?.tn2icd10?}" var="t">
    <li><g:link controller="tn2icd10" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="tn2icd10" action="create" params="['tn1icd10.id': tn1icd10Instance?.id]">${message(code: 'default.add.label', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: tn1icd10Instance, field: 'tn2icd10', 'error')}</span>
				</div>
			</div>

