
<%@ page import="historiamedica.Tn1icd10" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tn1icd10.label', default: 'Tn1icd10')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-tn1icd10" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="descripcion" title="${message(code: 'tn1icd10.descripcion.label', default: 'Descripcion')}" />
			
				<g:sortableColumn property="categoria" title="${message(code: 'tn1icd10.categoria.label', default: 'Categoria')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${tn1icd10InstanceList}" status="i" var="tn1icd10Instance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${tn1icd10Instance.id}">${fieldValue(bean: tn1icd10Instance, field: "descripcion")}</g:link></td>
			
				<td>${fieldValue(bean: tn1icd10Instance, field: "categoria")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${tn1icd10InstanceCount}" />
	</div>
</section>

</body>

</html>
