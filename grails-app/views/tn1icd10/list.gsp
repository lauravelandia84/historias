
<%@ page import="historiamedica.Tn1icd10" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tn1icd10.label', default: 'Tn1icd10')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>
<section id="list-tn1icd10" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="descripcion" title="${message(code: 'tn1icd10.descripcion.label', default: 'Descripcion')}" />
			
				<g:sortableColumn property="categoria" title="${message(code: 'tn1icd10.categoria.label', default: 'Categoria')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${tn1icd10InstanceList}" status="i" var="tn1icd10Instance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${tn1icd10Instance.id}">${fieldValue(bean: tn1icd10Instance, field: "descripcion")}</g:link></td>
			
				<td>${fieldValue(bean: tn1icd10Instance, field: "categoria")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${tn1icd10InstanceTotal}" />
	</div>
</section>

</body>

</html>
