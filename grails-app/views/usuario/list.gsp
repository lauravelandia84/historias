
<%@ page import="historiamedica.Usuario" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

    <g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

    <li class="${ params.action == "list" ? 'active' : '' }">
        <g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
    </li>
    <li class="${ params.action == "create" ? 'active' : '' }">
        <g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
    </li>

    <g:if test="${ params.action == 'show' || params.action == 'edit' }">
        <!-- the item is an object (not a list) -->
        <li class="${ params.action == "edit" ? 'active' : '' }">
            <g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
        </li>
        <li class="">
            <g:render template="/_common/modals/deleteTextLink"/>
        </li>
    </g:if>

</ul>
<section id="list-usuario" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="username" title="${message(code: 'usuario.username.label', default: 'Username')}" />
			
				<%--<g:sortableColumn property="password" title="${message(code: 'usuario.password.label', default: 'Password')}" />--%>
			
				<g:sortableColumn property="email" title="${message(code: 'usuario.email.label', default: 'Email')}" />
			
				<g:sortableColumn property="accountExpired" title="${message(code: 'usuario.accountExpired.label', default: 'Account Expired')}" />
			
				<g:sortableColumn property="accountLocked" title="${message(code: 'usuario.accountLocked.label', default: 'Account Locked')}" />
			
				<g:sortableColumn property="enabled" title="${message(code: 'usuario.enabled.label', default: 'Enabled')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${usuarioInstanceList}" status="i" var="usuarioInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${usuarioInstance.username.encodeAsBase64()}">${fieldValue(bean: usuarioInstance, field: "username")}</g:link></td>
			
				<%--<td>${fieldValue(bean: usuarioInstance, field: "password")}</td>--%>
				<%--<td>${fieldValue(bean: usuarioInstance, field: "password")}</td>--%>

				<td>${fieldValue(bean: usuarioInstance, field: "email")}</td>
			
				<td><g:formatBoolean boolean="${usuarioInstance.accountExpired}" /></td>
			
				<td><g:formatBoolean boolean="${usuarioInstance.accountLocked}" /></td>
			
				<td><g:formatBoolean boolean="${usuarioInstance.enabled}" /></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${usuarioInstanceTotal}" />
	</div>
</section>

</body>

</html>
