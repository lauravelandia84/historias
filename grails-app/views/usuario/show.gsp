
<%@ page import="historiamedica.Usuario" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'usuario.label', default: 'Usuario')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

    <g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

    <li class="${ params.action == "list" ? 'active' : '' }">
        <g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
    </li>
    <li class="${ params.action == "create" ? 'active' : '' }">
        <g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
    </li>

    <g:if test="${ params.action == 'show' || params.action == 'edit' }">
        <!-- the item is an object (not a list) -->
        <li class="${ params.action == "edit" ? 'active' : '' }">
            <g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
        </li>
        <li class="">
            <g:render template="/_common/modals/deleteTextLink"/>
        </li>
    </g:if>

</ul>
<section id="show-usuario" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.username.label" default="Username" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: usuarioInstance, field: "username")}</td>
				
			</tr>
		
			<%--<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.password.label" default="Password" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: usuarioInstance, field: "password")}</td>
				
			</tr>--%>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.email.label" default="Email" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: usuarioInstance, field: "email")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.accountExpired.label" default="Account Expired" /></td>
				
				<td valign="top" class="value"><g:formatBoolean boolean="${usuarioInstance?.accountExpired}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.accountLocked.label" default="Account Locked" /></td>
				
				<td valign="top" class="value"><g:formatBoolean boolean="${usuarioInstance?.accountLocked}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.enabled.label" default="Enabled" /></td>
				
				<td valign="top" class="value"><g:formatBoolean boolean="${usuarioInstance?.enabled}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.passwordExpired.label" default="Password Expired" /></td>
				
				<td valign="top" class="value"><g:formatBoolean boolean="${usuarioInstance?.passwordExpired}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="usuario.rol.label" default="Rol" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${usuarioInstance.rol}" var="r">
						<li>${r?.encodeAsHTML()}</li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
