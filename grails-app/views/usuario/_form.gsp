<%@ page import="historiamedica.Usuario" %>



			<div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'username', 'error')} required">
				<label for="username" class="control-label"><g:message code="usuario.username.label" default="Username" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<sec:ifAnyGranted roles="ROLE_ADMIN">
					    <g:textField name="username" required="" value="${usuarioInstance?.username}"/>
					    <span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'username', 'error')}</span>
                    </sec:ifAnyGranted>
                    <sec:ifNotGranted roles="ROLE_ADMIN">
                        ${usuarioInstance?.username}
                    </sec:ifNotGranted>
				</div>
			</div>

            <div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'password', 'error')} required">
				<label for="password" class="control-label"><g:message code="usuario.password.label" default="Password" /><span class="required-indicator">*</span></label>
				<div class="controls">
                        <g:textField name="password" required="" value="" />
					    <span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'password', 'error')}</span>
				</div>
			</div>


			<div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'email', 'error')} required">
				<label for="email" class="control-label"><g:message code="usuario.email.label" default="Email" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="email" name="email" required="" value="${usuarioInstance?.email}"/>
					<span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'email', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'accountExpired', 'error')} ">
				<label for="accountExpired" class="control-label"><g:message code="usuario.accountExpired.label" default="Account Expired" /></label>
				<div class="controls">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <bs:checkBox name="accountExpired" value="${usuarioInstance?.accountExpired}" />
					    <span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'accountExpired', 'error')}</span>
                    </sec:ifAnyGranted>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'accountLocked', 'error')} ">
				<label for="accountLocked" class="control-label"><g:message code="usuario.accountLocked.label" default="Account Locked" /></label>
				<div class="controls">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <bs:checkBox name="accountLocked" value="${usuarioInstance?.accountLocked}" />
					    <span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'accountLocked', 'error')}</span>
                    </sec:ifAnyGranted>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'enabled', 'error')} ">
				<label for="enabled" class="control-label"><g:message code="usuario.enabled.label" default="Enabled" /></label>
				<div class="controls">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <bs:checkBox name="enabled" value="${usuarioInstance?.enabled}" />
                        <span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'enabled', 'error')}</span>
                    </sec:ifAnyGranted>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'passwordExpired', 'error')} ">
				<label for="passwordExpired" class="control-label"><g:message code="usuario.passwordExpired.label" default="Password Expired" /></label>
				<div class="controls">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <bs:checkBox name="passwordExpired" value="${usuarioInstance?.passwordExpired}" />
                        <span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'passwordExpired', 'error')}</span>
                    </sec:ifAnyGranted>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: usuarioInstance, field: 'rol', 'error')} ">
				<label for="rol" class="control-label"><g:message code="usuario.rol.label" default="Rol" /></label>
				<div class="controls">
                    <sec:ifAnyGranted roles="ROLE_ADMIN">
					    <g:select name="rol" from="${historiamedica.Rol.list()}" multiple="multiple" optionKey="id" size="5" value="${usuarioInstance?.rol*.id}" class="many-to-many"/>
                    </sec:ifAnyGranted>
                    <sec:ifNotGranted roles="ROLE_ADMIN">
                        <ul>
                            <g:each in="${usuarioInstance.rol}" var="r">
                                <li>${r?.encodeAsHTML()}</li>
                            </g:each>
                        </ul>
                        <%--<g:select name="rol" from="${historiamedica.Rol.list()}" multiple="multiple" optionKey="id" size="5" value="${usuarioInstance?.rol*.id}" class="many-to-many" readonly="true"/>--%>
                    </sec:ifNotGranted>
					<span class="help-inline">${hasErrors(bean: usuarioInstance, field: 'rol', 'error')}</span>
				</div>
			</div>

