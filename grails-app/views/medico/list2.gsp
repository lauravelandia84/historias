<%@ page import="historiamedica.Medico" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medico.label', default: 'Medico')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">
		<li>
							<g:link class="list1" action="list1" args="[entityName]">
								<i class="icon-list"></i>
								<g:message code="Buscar Médico por fecha"/>
							</g:link>
						</li>
		</li>
		<li>
							<g:link class="list2" action="list2">
								<i class="icon-list"></i>
								<g:message code="Buscar Médico por especialidad" args="[entityName]" />
							</g:link>
						</li>
		</li>
	</ul>

<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list2" method="GET">
        <div class="fieldcontain">
            <label for="query"><b>Buscar por especialidad:</label> 
            <g:textField  name="description" value="${params.query}" />
            <div class="form-actions">
			<g:submitButton name="search" class="btn btn-primary" value="Buscar" />

			 <g:link action="downList2" params="[export: 'pdf', description: params.description, search: params.search]">Descargar</g:link>
		</div>
        </div>
    </g:form>
</fieldset>

            
<table class="table table-bordered">
		<thead>
			<tr>	
				<g:sortableColumn property="id" title="${message(code: 'medico.id.label', default: 'Id')}" />
			
				<g:sortableColumn property="name" title="${message(code: 'medico.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="firtsName" title="${message(code: 'medico.firtsName.label', default: 'Firts Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'medico.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="dateEntry" title="${message(code: 'medico.dateEntry.label', default: 'Date Entry')}" />

				<g:sortableColumn property="dateEntry" title="Especialidad" />

			    
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${medicoInstanceList}" status="i" var="medicoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

				<td>${medicoInstance[0]}</td>

				<td>${medicoInstance[1]}</td>
			
				<td>${medicoInstance[2]}</td>
			
				<td>${medicoInstance[3]}</td>

				<td>${medicoInstance[4]}</td>
			
				<td>${medicoInstance[5]}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${medicoInstanceTotal}" />
	</div>
</section>

</body>

</html>
