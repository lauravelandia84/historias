<%@ page import="historiamedica.Medico" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medico.label', default: 'Medico')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<ul id="Menu" class="nav nav-pills">
		<li>
							<g:link class="list1" action="list1" args="[entityName]">
								<i class="icon-list"></i>
								<g:message code="Buscar Médico por fecha"/>
							</g:link>
						</li>
		</li>
		<li>
							<g:link class="list2" action="list2">
								<i class="icon-list"></i>
								<g:message code="Buscar Médico por especialidad" args="[entityName]" />
							</g:link>
						</li>
		</li>
	</ul>

<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list1" method="GET">
        <div class="fieldcontain">
            <label for="query"><b>Buscar por rango de fechas:</label> <b>Fecha de Inicio
            <bs:datePicker name="dateInit" precision="day"  value="${params.query}"  /> <b>Fecha Final
            <bs:datePicker name="dateFinish" precision="day"  value="${params.query}"  />
            <div class="form-actions">
			<g:submitButton name="search" class="btn btn-primary" value="Buscar" />

            <g:link action="downList1" params="[export: 'pdf', fechaIni: params.dateInit, fechaFin: params.dateFinish, search: params.search]">Descargar</g:link>
		</div>
        </div>
    </g:form>
</fieldset>
<table>	
<section id="list-medico" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'medico.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="firtsName" title="${message(code: 'medico.firtsName.label', default: 'Firts Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'medico.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="cedula" title="${message(code: 'medico.cedula.label', default: 'Cedula')}" />
			
				<g:sortableColumn property="dateEntry" title="${message(code: 'medico.dateEntry.label', default: 'Date Entry')}" />
			
				<g:sortableColumn property="phone" title="${message(code: 'medico.phone.label', default: 'Teléfono')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${medicoInstanceList}" status="i" var="medicoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${medicoInstance.id}">${fieldValue(bean: medicoInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: medicoInstance, field: "firtsName")}</td>
			
				<td>${fieldValue(bean: medicoInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: medicoInstance, field: "cedula")}</td>
			
				<td><g:formatDate date="${medicoInstance.dateEntry}" /></td>
			
				<td>${fieldValue(bean: medicoInstance, field: "phone")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${medicoInstanceTotal}" />
	</div>
</section>

</body>

</html>
