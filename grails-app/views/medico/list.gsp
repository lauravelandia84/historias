
<%@ page import="historiamedica.Medico" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medico.label', default: 'Medico')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>
<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list" method="GET">
        <div class="fieldcontain">
            <label for="query">Buscar por Nombre del Médico:</label>
            <g:textField name="query" value="${params.query}"/>
        </div>
    </g:form>
</fieldset>
<table>	
<section id="list-medico" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'medico.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="firtsName" title="${message(code: 'medico.firtsName.label', default: 'Firts Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'medico.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="cedula" title="${message(code: 'medico.cedula.label', default: 'Cedula')}" />
			
				<g:sortableColumn property="dateEntry" title="${message(code: 'medico.dateEntry.label', default: 'Date Entry')}" />
			
				<g:sortableColumn property="phone" title="${message(code: 'medico.phone.label', default: 'Teléfono')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${medicoInstanceList}" status="i" var="medicoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${medicoInstance.id}">${fieldValue(bean: medicoInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: medicoInstance, field: "firtsName")}</td>
			
				<td>${fieldValue(bean: medicoInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: medicoInstance, field: "cedula")}</td>
			
				<td><g:formatDate date="${medicoInstance.dateEntry}" /></td>
			
				<td>${fieldValue(bean: medicoInstance, field: "phone")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${medicoInstanceTotal}" />
	</div>
</section>

</body>

</html>
