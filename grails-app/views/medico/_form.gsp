<%@ page import="historiamedica.Medico" %>



			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="medico.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="name" required="" value="${medicoInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'firtsName', 'error')} required">
				<label for="firtsName" class="control-label"><g:message code="medico.firtsName.label" default="Firts Name" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:textField name="firtsName" required="" value="${medicoInstance?.firtsName}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'firtsName', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'address', 'error')} ">
				<label for="address" class="control-label"><g:message code="medico.address.label" default="Address" /></label>
				<div class="controls">
					<g:textField name="address" value="${medicoInstance?.address}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'address', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'cedula', 'error')} ">
				<label for="cedula" class="control-label"><g:message code="medico.cedula.label" default="Cedula" /></label>
				<div class="controls">
					<g:textField name="cedula" value="${medicoInstance?.cedula}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'cedula', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'dateEntry', 'error')} required">
				<label for="dateEntry" class="control-label"><g:message code="medico.dateEntry.label" default="Date Entry" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="dateEntry" precision="day"  value="${medicoInstance?.dateEntry}"  />
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'dateEntry', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'especialidad', 'error')} ">
				<label for="especialidad" class="control-label"><g:message code="medico.especialidad.label" default="Especialidad" /></label>
				<div class="controls">
					<g:select name="especialidad" from="${historiamedica.Specialty.list()}" multiple="multiple" optionKey="id" size="5" value="${medicoInstance?.especialidad*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'especialidad', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'fiscalAddress', 'error')} ">
				<label for="fiscalAddress" class="control-label"><g:message code="medico.fiscalAddress.label" default="Fiscal Address" /></label>
				<div class="controls">
					<g:textField name="fiscalAddress" value="${medicoInstance?.fiscalAddress}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'fiscalAddress', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'fiscalPhone', 'error')} ">
				<label for="fiscalPhone" class="control-label"><g:message code="medico.fiscalPhone.label" default="Fiscal Phone" /></label>
				<div class="controls">
					<g:textField name="fiscalPhone" value="${medicoInstance?.fiscalPhone}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'fiscalPhone', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'numberCerfificate', 'error')} ">
				<label for="numberCerfificate" class="control-label"><g:message code="medico.numberCerfificate.label" default="Number Cerfificate" /></label>
				<div class="controls">
					<g:textField name="numberCerfificate" value="${medicoInstance?.numberCerfificate}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'numberCerfificate', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'numberCollege', 'error')} ">
				<label for="numberCollege" class="control-label"><g:message code="medico.numberCollege.label" default="Number College" /></label>
				<div class="controls">
					<g:textField name="numberCollege" value="${medicoInstance?.numberCollege}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'numberCollege', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'phone', 'error')} ">
				<label for="phone" class="control-label"><g:message code="medico.phone.label" default="Phone" /></label>
				<div class="controls">
					<g:textField name="phone" value="${medicoInstance?.phone}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'phone', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: medicoInstance, field: 'rif', 'error')} ">
				<label for="rif" class="control-label"><g:message code="medico.rif.label" default="Rif" /></label>
				<div class="controls">
					<g:textField name="rif" value="${medicoInstance?.rif}"/>
					<span class="help-inline">${hasErrors(bean: medicoInstance, field: 'rif', 'error')}</span>
				</div>
			</div>

