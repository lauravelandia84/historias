
<%@ page import="historiamedica.Medico" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medico.label', default: 'Medico')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-medico" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'medico.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="firtsName" title="${message(code: 'medico.firtsName.label', default: 'Firts Name')}" />
			
				<g:sortableColumn property="address" title="${message(code: 'medico.address.label', default: 'Address')}" />
			
				<g:sortableColumn property="cedula" title="${message(code: 'medico.cedula.label', default: 'Cedula')}" />
			
				<g:sortableColumn property="dateEntry" title="${message(code: 'medico.dateEntry.label', default: 'Date Entry')}" />
			
				<g:sortableColumn property="fiscalAddress" title="${message(code: 'medico.fiscalAddress.label', default: 'Fiscal Address')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${medicoInstanceList}" status="i" var="medicoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${medicoInstance.id}">${fieldValue(bean: medicoInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: medicoInstance, field: "firtsName")}</td>
			
				<td>${fieldValue(bean: medicoInstance, field: "address")}</td>
			
				<td>${fieldValue(bean: medicoInstance, field: "cedula")}</td>
			
				<td><g:formatDate date="${medicoInstance.dateEntry}" /></td>
			
				<td>${fieldValue(bean: medicoInstance, field: "fiscalAddress")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${medicoInstanceCount}" />
	</div>
</section>

</body>

</html>
