
<%@ page import="historiamedica.Medico" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'medico.label', default: 'Medico')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>	
<section id="show-medico" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.name.label" default="Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "name")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.firtsName.label" default="Firts Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "firtsName")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.address.label" default="Address" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "address")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.cedula.label" default="Cedula" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "cedula")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.dateEntry.label" default="Date Entry" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${medicoInstance?.dateEntry}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.especialidad.label" default="Especialidad" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${medicoInstance.especialidad}" var="e">
						<li><g:link controller="specialty" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.fiscalAddress.label" default="Fiscal Address" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "fiscalAddress")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.fiscalPhone.label" default="Fiscal Phone" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "fiscalPhone")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.numberCerfificate.label" default="Number Cerfificate" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "numberCerfificate")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.numberCollege.label" default="Number College" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "numberCollege")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.phone.label" default="Phone" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "phone")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="medico.rif.label" default="Rif" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: medicoInstance, field: "rif")}</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
