<%@ page import="historiamedica.Consultation" %>

<script type="text/javascript" src="http://loudev.com/js/jquery.js"></script>
<script type="text/javascript" src="http://loudev.com/js/jquery.quicksearch.js"></script>
<script type="text/javascript" src="http://loudev.com/js/jquery.multi-select.js"></script>
<link   href="${resource(dir:'css', file:'multi-select.css')}" media="screen" rel="stylesheet" type="text/css">

<script src="${resource(dir:'js', file:'jstree.min.js')}"></script>
<script src="${resource(dir:'js', file:'jstree.js')}"></script>


<script type="text/javascript">
    $(function() {
   $('#custom-headers').multiSelect({
  selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Agregar Medicamento\'>",
  selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='Descartar Medicamento\'>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        $selectionSearch = that.$selectionUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
    });

    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
    .on('keydown', function(e){
      if (e.which == 40){
        that.$selectionUl.focus();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
    this.qs2.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
    this.qs2.cache();
  }
});

   $("#tn1icd10").change(function(){
       $("#tn3icd10 option").remove();
   });
        //console.log( "ready!" );
        //$('#medicamento').multiSelect();






    });
</script>
<g:javascript>

    function quitar_fila_examen(id){
        //alert ("Quitar");
        var miTabla = document.getElementById('tbody_subexamlab');
        // var rowIndex = document.getElementById('tr_subexamlab_'+id).rowIndex;
        //miTabla.deleteRow(rowIndex);

        var row = document.getElementById('tr_subexamlab_'+id);
        row.parentNode.removeChild(row);
    }
    function agregarExamen(id, descripcion){
        //alert("Examen => "+id+" Valor => "+valor);

        if (! document.getElementById('tr_subexamlab_'+id)) {
            var miTabla = document.getElementById('tbody_subexamlab');
            var lasCeldas = miTabla.getElementsByTagName('tr');
            var fila = document.createElement('tr');
            fila.setAttribute('id', 'tr_subexamlab_'+id);

            var celda1 = document.createElement('td'); // descripcion del examen
            var celda2 = document.createElement('td'); // Quitar fila
            celda2.setAttribute('width', '10%');

            celda1.innerHTML = descripcion + '<input type="hidden" name="subexam_'+id+'" id="subexam_'+id+'" value="'+id+'" >';
            celda2.innerHTML = "<a style=\"cursor:pointer\"><img src=\"https://pdvsa-mantenimiento.rhcloud.com/imagenes/delete.png\" alt=\"Quitar Examen\" title=\"Quitar Examen\" onclick=\"quitar_fila_examen("+id+");\" /></a>";
            fila.appendChild(celda1);
            fila.appendChild(celda2);
            miTabla.appendChild(fila);
        }
    }


        function quitar_fila_examen1(id){
        //alert ("Quitar");
        var miTabla = document.getElementById('tbody_subexamrayx');
        // var rowIndex = document.getElementById('tr_subexamlab_'+id).rowIndex;
        //miTabla.deleteRow(rowIndex);

        var row = document.getElementById('tr_subexamrayx_'+id);
        row.parentNode.removeChild(row);
    }

    function agregarExamen1(id, descripcion){
        //alert("Examen => "+id+" Valor => "+valor);

        if (! document.getElementById('tr_subexamrayx_'+id)) {
            var miTabla = document.getElementById('tbody_subexamrayx');
            var lasCeldas = miTabla.getElementsByTagName('tr');
            var fila = document.createElement('tr');
            fila.setAttribute('id', 'tr_subexamrayx_'+id);

            var celda1 = document.createElement('td'); // descripcion del examen
            var celda2 = document.createElement('td'); // Quitar fila
            celda2.setAttribute('width', '10%');

            celda1.innerHTML = descripcion + '<input type="hidden" name="subexamrayx_'+id+'" id="subexamrayx_'+id+'" value="'+id+'" >';
            celda2.innerHTML = "<a style=\"cursor:pointer\"><img src=\"https://pdvsa-mantenimiento.rhcloud.com/imagenes/delete.png\" alt=\"Quitar Examen\" title=\"Quitar Examen\" onclick=\"quitar_fila_examen1("+id+");\" /></a>";
            fila.appendChild(celda1);
            fila.appendChild(celda2);
            miTabla.appendChild(fila);
        }
    }

    function agregarDiagnostico(id, descripcion){
        if (! document.getElementById('tr_diagnostico_'+id)) {
            var miTabla = document.getElementById('tbody_diagnosticos');
            var lasCeldas = miTabla.getElementsByTagName('tr');
            var fila = document.createElement('tr');
            fila.setAttribute('id', 'tr_diagnostico_'+id);

            var celda1 = document.createElement('td'); // descripcion del examen
            var celda2 = document.createElement('td'); // Quitar fila
            celda2.setAttribute('width', '10%');

            celda1.innerHTML = descripcion + '<input type="hidden" name="tn3icd10_'+id+'" id="tn3icd10_'+id+'" value="'+id+'" >';
            celda2.innerHTML = "<a style=\"cursor:pointer\" title=\"Quitar Diagnostico\" onclick=\"quitar_fila_diagnostico("+id+");\" >x</a>";
            fila.appendChild(celda1);
            fila.appendChild(celda2);
            miTabla.appendChild(fila);
        }
    }

    function quitar_fila_diagnostico(id){
        var miTabla = document.getElementById('tbody_diagnosticos');
        var row = document.getElementById('tr_diagnostico_'+id);
        row.parentNode.removeChild(row);      //
    }

</g:javascript>

			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'date', 'error')} required">
				<label for="date" class="control-label"><g:message code="consultation.date.label" default="Date" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<bs:datePicker name="date" precision="day"  value="${consultationInstance?.date}"  />
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'date', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'medico', 'error')} required">
				<label for="medico" class="control-label"><g:message code="consultation.medico.label" default="Medico" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="medico" name="medico.id" from="${historiamedica.Medico.list()}" optionKey="id" required="" value="${consultationInstance?.medico?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'medico', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'patient', 'error')} required">
				<label for="patient" class="control-label"><g:message code="consultation.patient.label" default="Patient" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="patient" name="patient.id" from="${historiamedica.Patient.list()}" optionKey="id" required="" value="${consultationInstance?.patient?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'patient', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'typeconsultation', 'error')} required">
				<label for="typeconsultation" class="control-label"><g:message code="consultation.typeconsultation.label" default="Typeconsultation" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="typeconsultation" name="typeconsultation.id" from="${historiamedica.Typeconsultation.list()}" optionKey="id" required="" value="${consultationInstance?.typeconsultation?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'typeconsultation', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'consultationcause', 'error')} required">
				<label for="consultationcause" class="control-label"><g:message code="consultation.consultationcause.label" default="Consultationcause" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="consultationcause" name="consultationcause.id" from="${historiamedica.Consultationcause.list()}" optionKey="id" required="" value="${consultationInstance?.consultationcause?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'consultationcause', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'images', 'error')} required">
				<label for="images" class="control-label"><g:message code="consultation.images.label" default="Images" /><span class="required-indicator">*</span></label>
				<div class="controls">
                        <g:field type="file" name="img" multiple=""/>
                        <span class="help-inline">${hasErrors(bean: consultationInstance, field: 'images', 'error')}</span>

				</div>
			</div>

	<div class="panel-group" id="accordion">
   		<div class="panel panel-primary">	
			<div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
			          Ficha de Consulta
			        </a>
			      </h4>
			    </div>
			    <div id="collapseOne" class="panel-collapse collapse">
			      <div class="panel-body">


			<ul id="myTab" class="nav nav-tabs">
			    <li class="active"><a href="#cause" data-toggle="tab">Motivo</a></li>
			    <li><a href="#antecedent" data-toggle="tab">Antecedentes</a></li>
			    <li><a href="#diseasedPresent" data-toggle="tab">Enfermedad Actual</a></li>
			    <li><a href="#evolution" data-toggle="tab">Evolución</a></li>
			    <li><a href="#examFunctional" data-toggle="tab">Exámen Funcional</a></li>
			    <li><a href="#examPhysical" data-toggle="tab">Exámen Físico</a></li>
			    <li><a href="#medicalPlan" data-toggle="tab">Plan Médico</a></li>
			    <li><a href="#studyClinical" data-toggle="tab">Estudio Clínico</a></li>
			</ul>
			<div id="myTabContent" class="tab-content">
			<div class="tab-pane fade in active" id="cause">
				<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'cause', 'error')} ">
				<label for="cause" class="control-label"><g:message code="consultation.cause.label" default="Cause" /></label>
				<div class="controls">
					<g:textArea name="cause" style="width: 60%;" value="${consultationInstance?.cause}" rows="10" cols="500"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'cause', 'error')}</span>
				</div>
			</div>
			</div>
			<div class="tab-pane fade" id="antecedent">
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'antecedent', 'error')} ">
				<label for="antecedent" class="control-label"><g:message code="consultation.antecedent.label" default="Antecedent" /></label>
				<div class="controls">
					<g:textArea name="antecedent" style="width: 60%;" value="${consultationInstance?.antecedent}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'antecedent', 'error')}</span>
				</div>
			</div>
			</div>
			<div class="tab-pane fade" id="diseasedPresent">
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'diseasedPresent', 'error')} ">
				<label for="diseasedPresent" class="control-label"><g:message code="consultation.diseasedPresent.label" default="Diseased Present" /></label>
				<div class="controls">
					<g:textArea name="diseasedPresent" style="width: 60%;" value="${consultationInstance?.diseasedPresent}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'diseasedPresent', 'error')}</span>
				</div>
			</div>
			</div>
			<div class="tab-pane fade" id="evolution">
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'evolution', 'error')} ">
				<label for="evolution" class="control-label"><g:message code="consultation.evolution.label" default="Evolution" /></label>
				<div class="controls">
					<g:textArea name="evolution" style="width: 60%;" value="${consultationInstance?.evolution}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'evolution', 'error')}</span>
				</div>
			</div>
			</div>
			<div class="tab-pane fade" id="examFunctional">
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'examFunctional', 'error')} ">
				<label for="examFunctional" class="control-label"><g:message code="consultation.examFunctional.label" default="Exam Functional" /></label>
				<div class="controls">
					<g:textArea name="examFunctional" style="width: 60%;" value="${consultationInstance?.examFunctional}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'examFunctional', 'error')}</span>
				</div>
			</div>
			</div>
			<div class="tab-pane fade" id="examPhysical">
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'examPhysical', 'error')} ">
				<label for="examPhysical" class="control-label"><g:message code="consultation.examPhysical.label" default="Exam Physical" /></label>
				<div class="controls">
					<g:textArea name="examPhysical" style="width: 60%;" value="${consultationInstance?.examPhysical}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'examPhysical', 'error')}</span>
				</div>
			</div>
			</div>
			<div class="tab-pane fade" id="medicalPlan">
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'medicalPlan', 'error')} ">
				<label for="medicalPlan" class="control-label"><g:message code="consultation.medicalPlan.label" default="Medical Plan" /></label>
				<div class="controls">
					<g:textArea name="medicalPlan" style="width: 60%;" value="${consultationInstance?.medicalPlan}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'medicalPlan', 'error')}</span>
				</div>
			</div>
			</div>
			<div class="tab-pane fade" id="studyClinical">
			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'studyClinical', 'error')} ">
				<label for="studyClinical" class="control-label"><g:message code="consultation.studyClinical.label" default="Study Clinical" /></label>
				<div class="controls">
					<g:textArea name="studyClinical" style="width: 60%;" value="${consultationInstance?.studyClinical}" rows="10" cols="100"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'studyClinical', 'error')}</span>
				</div>
			</div>
			</div>

	     		</div>	
			</div>
	     </div>
	 </div>

	 <div class="panel-group" id="accordion">
   		<div class="panel panel-primary">	
			<div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
			          Exámenes Médicos
			        </a>
			      </h4>
			    </div>
			    <div id="collapseTwo" class="panel-collapse collapse">
			      <div class="panel-body">


					<div class="panel-group" id="accordion">
			   		<div class="panel panel-primary">	
						<div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
						         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Exámenes de Laboratorio
						        </a>
						      </h4>
						    </div>
						    <div id="collapseSix" class="panel-collapse collapse">
						      <div class="panel-body">

						 <div class="row">
                          <div class="span3">
                              <label for="examlab" class="control-label"><g:message code="consultation.examlab.label" default="Categoría del Exámen" /></label><br><br>
                              <div class="controls" style="margin-left: 15px;">
                                  <g:select id="examlab" name="examlab" from="${historiamedica.Examlab.list()}" optionKey="id" size="10"

                                            onchange="${remoteFunction(controller: 'Subexamlab', action: 'getSubexamlabs', params: '\'id=\' + this.value', update:'subexamenDiv')}" />
                                  <span class="help-inline">${hasErrors(bean: consultationInstance, field: 'examlab', 'error')}</span>
                              </div>
                          </div>
                          <div id="subexamenDiv" class="span4" style="margin-left: 20px">
                              <label for="Subexamlab" class="control-label">Exámen de Laboratorio</label><br><br>
                              <div class="controls"  style="margin-left: 15px;">
                                  <g:select id="subexamlab" name="subexamlab.id" multiple="multiple" from="" optionKey="id" size="10" />

                              </div>
                          </div>
                          <div class="span4" style="margin-left: 20px">
                              <table border="1" width="100%" cellpadding="5" style="border: 1px solid #ddd;">
                                  <thead>
                                  <tr>
                                      <th colspan="2">Exámenes Seleccionados:</th>
                                  </tr>
                                  </thead>
                                  <tbody id="tbody_subexamlab">
                                  </tbody>
                              </table>
                          </div>
                      </div>

							</div>	
						</div>
				     </div>
				 </div>

				 <div class="panel-group" id="accordion">
			   		<div class="panel panel-primary">	
						<div class="panel-heading">
						      <h4 class="panel-title">
						        <a data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
						          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Exámenes de Rayos X
						        </a>
						      </h4>
						    </div>
						    <div id="collapseSeven" class="panel-collapse collapse">
						      <div class="panel-body">

						      
						      <div class="row">
                          <div class="span3">
                              <label for="examrayx" class="control-label"><g:message code="consultation.examrayx.label" default="Categoría del Exámen" /></label><br><br>
                              <div class="controls" style="margin-left: 15px;">
                                  <g:select id="examrayx" name="examrayx" from="${historiamedica.Examrayx.list()}" optionKey="id" size="10"

                                            onchange="${remoteFunction(controller: 'Subexamrayx', action: 'getSubexamrayx', params: '\'id=\' + this.value', update:'subexamenrayxDiv')}" />
                                  <span class="help-inline">${hasErrors(bean: consultationInstance, field: 'examrayx', 'error')}</span>
                              </div>
                          </div>
                          <div id="subexamenrayxDiv" class="span4" style="margin-left: 20px">
                              <label for="Subexamrayx" class="control-label">Exámen de Laboratorio</label><br><br>
                              <div class="controls"  style="margin-left: 15px;">
                                  <g:select id="subexamrayx" name="subexamrayx.id" multiple="multiple" from="" optionKey="id" size="10" />

                              </div>
                          </div>
                          <div class="span4" style="margin-left: 20px">
                              <table border="1" width="100%" cellpadding="5" style="border: 1px solid #ddd;">
                                  <thead>
                                  <tr>
                                      <th colspan="2">Exámenes Seleccionados:</th>
                                  </tr>
                                  </thead>
                                  <tbody id="tbody_subexamrayx">
                                  </tbody>
                              </table>
                          </div>
                      </div>



                 			</div>	
						</div>
				     </div>
				 </div>     

			

		     		</div>	
				</div>
		     </div>
		 </div>

		 


		<div class="panel-group" id="accordion">
   		<div class="panel panel-primary">	
			<div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
			          Medicamentos
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFour" class="panel-collapse collapse">
			      <div class="panel-body">

			<div class="control-group fieldcontain ${hasErrors(bean: consultationInstance, field: 'medicamento', 'error')} ">
				<label for="medicamento" class="control-label"><g:message code="consultation.medicamento.label" default="Medicamento" /></label>
				<div class="controls">
					<g:select id='custom-headers' name="medicamento" from="${historiamedica.Medicamento.list()}" multiple="multiple" optionKey="id" size="10" value="${consultationInstance?.medicamento*.id}" class="many-to-many"/>
					<span class="help-inline">${hasErrors(bean: consultationInstance, field: 'medicamento', 'error')}</span>
				</div>
			</div>

		     		</div>	
				</div>
		     </div>
		 </div>
					
		 		<div class="panel-group" id="accordion">
   		<div class="panel panel-primary">	
			<div class="panel-heading">
			      <h4 class="panel-title">
			        <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
			          Diagnósticos
			        </a>
			      </h4>
			    </div>
			    <div id="collapseFive" class="panel-collapse collapse">
			      <div class="panel-body">

          <div class="row">
              <div class="span3">
                  <label for="tn1icd10" class="control-label"><g:message code="consultation.tn1icd10.label" default="Categoría 1" /></label><br><br>
                  <div class="controls" style="margin-left: 15px;">
                      <g:select id="tn1icd10" name="tn1icd10" from="${historiamedica.Tn1icd10.list()}" optionKey="id" size="10"

                                onchange="${remoteFunction(controller: 'tn2icd10', action: 'getTn2icd10', params: '\'id=\' + this.value', update:'tn2icd10Div')}" />
                  </div>
              </div>
              <div id="tn2icd10Div" class="span3" style="margin-left: 20px">
                  <label for="tn2icd10" class="control-label"><g:message code="consultation.tn3icd10.label" default="Categoría 2" /></label><br><br>
                  <div class="controls"  style="margin-left: 15px;">
                      <g:select id="tn2icd10" name="tn2icd10" from="" optionKey="id" size="10" />
                  </div>
              </div>

              <div id="tn3icd10Div" class="span4" style="margin-left: 20px">
                  <label for="tn3icd10" class="control-label"><g:message code="consultation.tn3icd10.label" default="Categoría 3" /></label><br><br>
                  <div class="controls"  style="margin-left: 15px;">
                      <g:select id="tn3icd10" name="tn3icd10" from="" multiple="multiple" optionKey="id" size="10" />
                  </div>x
                  <% /*
                  <table border="1" width="100%" cellpadding="5" style="border: 1px solid #ddd;">
                      <thead>
                      <tr>
                          <th colspan="2">Examenes seleccionados:</th>
                      </tr>
                      </thead>
                      <tbody id="tbody_subexamlab">
                      </tbody>
                  </table> */ %>
              </div>

          </div>
            <br>
          <div class="row">
              <div class="span8">
                  <table class="table table-bordered" style="margin-left: 15px;">
                      <thead>
                      <tr>
                          <th colspan="2">Diagnósticos seleccionados</th>
                      </tr>
                      </thead>
                      <tbody id="tbody_diagnosticos">
                      </tbody>
                  </table>
              </div>
          </div>


		



		     		</div>	
				</div>
		     </div>
		 </div>
