
<%@ page import="historiamedica.Consultation" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'consultation.label', default: 'Consultation')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-consultation" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.antecedent.label" default="Antecedent" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "antecedent")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.cause.label" default="Cause" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "cause")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.consultationcause.label" default="Consultationcause" /></td>
				
				<td valign="top" class="value"><g:link controller="consultationcause" action="show" id="${consultationInstance?.consultationcause?.id}">${consultationInstance?.consultationcause?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.date.label" default="Date" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${consultationInstance?.date}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.diseasedPresent.label" default="Diseased Present" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "diseasedPresent")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.evolution.label" default="Evolution" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "evolution")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.examFunctional.label" default="Exam Functional" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "examFunctional")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.examPhysical.label" default="Exam Physical" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "examPhysical")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.images.label" default="Images" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${consultationInstance.images}" var="i">
						<li><g:link controller="images" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.medicalPlan.label" default="Medical Plan" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "medicalPlan")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.medicamento.label" default="Medicamento" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${consultationInstance.medicamento}" var="m">
						<li><g:link controller="medicamento" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.medico.label" default="Medico" /></td>
				
				<td valign="top" class="value"><g:link controller="medico" action="show" id="${consultationInstance?.medico?.id}">${consultationInstance?.medico?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.patient.label" default="Patient" /></td>
				
				<td valign="top" class="value"><g:link controller="patient" action="show" id="${consultationInstance?.patient?.id}">${consultationInstance?.patient?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.studyClinical.label" default="Study Clinical" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: consultationInstance, field: "studyClinical")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.subexamlab.label" default="Subexamlab" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${consultationInstance.subexamlab}" var="s">
						<li><g:link controller="subexamlab" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.subexamrayx.label" default="Subexamrayx" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${consultationInstance.subexamrayx}" var="s">
						<li><g:link controller="subexamrayx" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.tn3icd10.label" default="Tn3icd10" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${consultationInstance.tn3icd10}" var="t">
						<li><g:link controller="tn3icd10" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="consultation.typeconsultation.label" default="Typeconsultation" /></td>
				
				<td valign="top" class="value"><g:link controller="typeconsultation" action="show" id="${consultationInstance?.typeconsultation?.id}">${consultationInstance?.typeconsultation?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
