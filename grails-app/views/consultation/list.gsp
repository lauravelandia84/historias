
<%@ page import="historiamedica.Consultation" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'consultation.label', default: 'Consultation')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />
		
		<li class="${ params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>
		<li class="${ params.action == "create" ? 'active' : '' }">
			<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		</li>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>	
<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list" method="GET">
        <div class="fieldcontain">
            <label for="query"><b>Buscar por Nombre del Paciente:</label>
            <g:textField name="query" value="${params.query}"/>
            
        </div>
    </g:form>
</fieldset>	
<section id="list-consultation" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
				<g:sortableColumn property="date" title="${message(code: 'consultation.date.label', default: 'Fecha')}" />

				<g:sortableColumn property="Paciente" title="Nombre del Paciente" />
				<g:sortableColumn property="Medico" title="Nombre del Médico" />

				<g:sortableColumn property="Especialidad" title="Especialidad" />

			</tr>
		</thead>
		<tbody>
		<g:each in="${consultationInstanceList}" status="i" var="consultationInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			<td>${consultationInstance[0]}</td>
				<td>${consultationInstance[1]}</td>
			
				<td>${consultationInstance[2]}</td>
			
				<td>${consultationInstance[3]}</td>
				
				<td><g:link action="show" id="${consultationInstance[4]}">Ver</g:link>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		
	</div>
</section>

</body>

</html>
