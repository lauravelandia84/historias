<%@ page import="historiamedica.Patient" %>
<%@ page import="historiamedica.Consultation" %>

<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'patient.label', default: 'Pacientes')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>

<ul id="Menu" class="nav nav-pills">
		<li>
							<g:link class="list1" action="list1" args="[entityName]">
								<i class="icon-list"></i>
								<g:message code="Buscar Exámenes de Laboratorio por fecha de Consulta"/>
							</g:link>
						</li>
		</li>
		
	</ul>

<g:if test="${flash.message}">
<div class="message" role="status">${flash.message}</div>
</g:if>
<fieldset class="form">
    <g:form action="list2" method="GET">
        <div class="fieldcontain">
            <label for="query"><b>Buscar fecha:</label> <b>Fecha del Documento
            <bs:datePicker name="dateInit" precision="day"  value="${params.query}"  />
          
            <div class="form-actions">
			<g:submitButton name="search" class="btn btn-primary" value="Buscar" />
            
		
		 
        </div>
        </div>
    </g:form>
</fieldset>
<table>	
<section id="list-consultation" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
				<g:sortableColumn property="date" title="${message(code: 'consultation.date.label', default: 'Date')}" />
			
				<th><g:message code="consultation.patient.label" default="Patient" /></th>
				<th><g:message code="consultation.medico.label" default="Medico" /></th>
			
				
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${consultationInstanceList}" status="i" var="consultationInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td>${consultationInstance[0]}</td>
			
				<td>${consultationInstance[1]}</td>
				<td>${consultationInstance[2]}</td>

				<td><g:link action="downList2" id="${consultationInstance[3]}" params="[export: 'pdf']">Descargar</g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		
	</div>
</section>

</body>

</html>
