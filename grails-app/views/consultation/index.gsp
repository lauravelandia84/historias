
<%@ page import="historiamedica.Consultation" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'consultation.label', default: 'Consultation')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-consultation" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
			<tr>
			
				<g:sortableColumn property="antecedent" title="${message(code: 'consultation.antecedent.label', default: 'Antecedent')}" />
			
				<g:sortableColumn property="cause" title="${message(code: 'consultation.cause.label', default: 'Cause')}" />
			
				<th><g:message code="consultation.consultationcause.label" default="Consultationcause" /></th>
			
				<g:sortableColumn property="date" title="${message(code: 'consultation.date.label', default: 'Date')}" />
			
				<g:sortableColumn property="diseasedPresent" title="${message(code: 'consultation.diseasedPresent.label', default: 'Diseased Present')}" />
			
				<g:sortableColumn property="evolution" title="${message(code: 'consultation.evolution.label', default: 'Evolution')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${consultationInstanceList}" status="i" var="consultationInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${consultationInstance.id}">${fieldValue(bean: consultationInstance, field: "antecedent")}</g:link></td>
			
				<td>${fieldValue(bean: consultationInstance, field: "cause")}</td>
			
				<td>${fieldValue(bean: consultationInstance, field: "consultationcause")}</td>
			
				<td><g:formatDate date="${consultationInstance.date}" /></td>
			
				<td>${fieldValue(bean: consultationInstance, field: "diseasedPresent")}</td>
			
				<td>${fieldValue(bean: consultationInstance, field: "evolution")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${consultationInstanceCount}" />
	</div>
</section>

</body>

</html>
