package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * PrincipioController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class PrincipioController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [principioInstanceList: Principio.list(params), principioInstanceTotal: Principio.count()]
    }

    def create() {
        [principioInstance: new Principio(params)]
    }

    def save() {
        def principioInstance = new Principio(params)
        if (!principioInstance.save(flush: true)) {
            render(view: "create", model: [principioInstance: principioInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'principio.label', default: 'Principio'), principioInstance.id])
        redirect(action: "show", id: principioInstance.id)
    }

    def show() {
        def principioInstance = Principio.get(params.id)
        if (!principioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'principio.label', default: 'Principio'), params.id])
            redirect(action: "list")
            return
        }

        [principioInstance: principioInstance]
    }

    def edit() {
        def principioInstance = Principio.get(params.id)
        if (!principioInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'principio.label', default: 'Principio'), params.id])
            redirect(action: "list")
            return
        }

        [principioInstance: principioInstance]
    }

    def update() {
        def principioInstance = Principio.get(params.id)
        if (!principioInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'principio.label', default: 'Principio'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (principioInstance.version > version) {
                principioInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'principio.label', default: 'Principio')] as Object[],
                          "Another user has updated this Principio while you were editing")
                render(view: "edit", model: [principioInstance: principioInstance])
                return
            }
        }

        principioInstance.properties = params

        if (!principioInstance.save(flush: true)) {
            render(view: "edit", model: [principioInstance: principioInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'principio.label', default: 'Principio'), principioInstance.id])
        redirect(action: "show", id: principioInstance.id)
    }

    def delete() {
        def principioInstance = Principio.get(params.id)
        if (!principioInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'principio.label', default: 'Principio'), params.id])
            redirect(action: "list")
            return
        }

        try {
            principioInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'principio.label', default: 'Principio'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'principio.label', default: 'Principio'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def initialLoad() {
        def dir = new File('web-app/f_resources/Prin_List.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==1){
                def prinInstance = new Principio()
                prinInstance.principio=line[0]
                // println labInstance
                if(!prinInstance.save(flush: true)){
                    println "fallo en ${prinInstance.principio}"
                }
            }
        }
        redirect(action: "list")
    }
}
