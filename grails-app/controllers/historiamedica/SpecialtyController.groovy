package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * SpecialtyController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class SpecialtyController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        [specialtyInstanceList: Specialty.list(params), specialtyInstanceTotal: Specialty.count()]
    }
   
    def create() {
        [specialtyInstance: new Specialty(params)]
    }

    def save() {
        def specialtyInstance = new Specialty(params)
        if (!specialtyInstance.save(flush: true)) {
            render(view: "create", model: [specialtyInstance: specialtyInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'specialty.label', default: 'Specialty'), specialtyInstance.id])
        redirect(action: "show", id: specialtyInstance.id)
    }

    def show() {
        def specialtyInstance = Specialty.get(params.id)
        if (!specialtyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'specialty.label', default: 'Specialty'), params.id])
            redirect(action: "list")
            return
        }

        [specialtyInstance: specialtyInstance]
    }

    def edit() {
        def specialtyInstance = Specialty.get(params.id)
        if (!specialtyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'specialty.label', default: 'Specialty'), params.id])
            redirect(action: "list")
            return
        }

        [specialtyInstance: specialtyInstance]
    }

    def update() {
        def specialtyInstance = Specialty.get(params.id)
        if (!specialtyInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'specialty.label', default: 'Specialty'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (specialtyInstance.version > version) {
                specialtyInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'specialty.label', default: 'Specialty')] as Object[],
                          "Another user has updated this Specialty while you were editing")
                render(view: "edit", model: [specialtyInstance: specialtyInstance])
                return
            }
        }

        specialtyInstance.properties = params

        if (!specialtyInstance.save(flush: true)) {
            render(view: "edit", model: [specialtyInstance: specialtyInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'specialty.label', default: 'Specialty'), specialtyInstance.id])
        redirect(action: "show", id: specialtyInstance.id)
    }

    def delete() {
        def specialtyInstance = Specialty.get(params.id)
        if (!specialtyInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'specialty.label', default: 'Specialty'), params.id])
            redirect(action: "list")
            return
        }

        try {
            specialtyInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'specialty.label', default: 'Specialty'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'specialty.label', default: 'Specialty'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
