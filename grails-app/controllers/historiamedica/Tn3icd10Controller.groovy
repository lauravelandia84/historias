package historiamedica

import org.springframework.dao.DataIntegrityViolationException



/**
 * Tn3icd10Controller
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class Tn3icd10Controller {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def getTn3icd10 = {

        def tn2icd10 = Tn2icd10.get(params.id)

        def tn3icd10List = tn2icd10?.tn3icd10

        render(template: "selectTn3icd10", model: [tn3icd10List: tn3icd10List])
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tn3icd10InstanceList: Tn3icd10.list(params), tn3icd10InstanceTotal: Tn3icd10.count()]
    }

    def create() {
        [tn3icd10Instance: new Tn3icd10(params)]
    }

    def save() {
        def tn3icd10Instance = new Tn3icd10(params)
        if (!tn3icd10Instance.save(flush: true)) {
            render(view: "create", model: [tn3icd10Instance: tn3icd10Instance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), tn3icd10Instance.id])
        redirect(action: "show", id: tn3icd10Instance.id)
    }

    def show() {
        def tn3icd10Instance = Tn3icd10.get(params.id)
        if (!tn3icd10Instance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), params.id])
            redirect(action: "list")
            return
        }

        [tn3icd10Instance: tn3icd10Instance]
    }

    def edit() {
        def tn3icd10Instance = Tn3icd10.get(params.id)
        if (!tn3icd10Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), params.id])
            redirect(action: "list")
            return
        }

        [tn3icd10Instance: tn3icd10Instance]
    }

    def update() {
        def tn3icd10Instance = Tn3icd10.get(params.id)
        if (!tn3icd10Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (tn3icd10Instance.version > version) {
                tn3icd10Instance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'tn3icd10.label', default: 'Tn3icd10')] as Object[],
                          "Another user has updated this Tn3icd10 while you were editing")
                render(view: "edit", model: [tn3icd10Instance: tn3icd10Instance])
                return
            }
        }

        tn3icd10Instance.properties = params

        if (!tn3icd10Instance.save(flush: true)) {
            render(view: "edit", model: [tn3icd10Instance: tn3icd10Instance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), tn3icd10Instance.id])
        redirect(action: "show", id: tn3icd10Instance.id)
    }

    def delete() {
        def tn3icd10Instance = Tn3icd10.get(params.id)
        if (!tn3icd10Instance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), params.id])
            redirect(action: "list")
            return
        }

        try {
            tn3icd10Instance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tn3icd10.label', default: 'Tn3icd10'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
