package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * ExamrayxController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ExamrayxController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [examrayxInstanceList: Examrayx.list(params), examrayxInstanceTotal: Examrayx.count()]
    }

    def create() {
        [examrayxInstance: new Examrayx(params)]
    }

    def save() {
        def examrayxInstance = new Examrayx(params)
        if (!examrayxInstance.save(flush: true)) {
            render(view: "create", model: [examrayxInstance: examrayxInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), examrayxInstance.id])
        redirect(action: "show", id: examrayxInstance.id)
    }

    def show() {
        def examrayxInstance = Examrayx.get(params.id)
        if (!examrayxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), params.id])
            redirect(action: "list")
            return
        }

        [examrayxInstance: examrayxInstance]
    }

    def edit() {
        def examrayxInstance = Examrayx.get(params.id)
        if (!examrayxInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), params.id])
            redirect(action: "list")
            return
        }

        [examrayxInstance: examrayxInstance]
    }

    def update() {
        def examrayxInstance = Examrayx.get(params.id)
        if (!examrayxInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (examrayxInstance.version > version) {
                examrayxInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'examrayx.label', default: 'Examrayx')] as Object[],
                          "Another user has updated this Examrayx while you were editing")
                render(view: "edit", model: [examrayxInstance: examrayxInstance])
                return
            }
        }

        examrayxInstance.properties = params

        if (!examrayxInstance.save(flush: true)) {
            render(view: "edit", model: [examrayxInstance: examrayxInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), examrayxInstance.id])
        redirect(action: "show", id: examrayxInstance.id)
    }
    
    def initialLoad() {
        def dir = new File('web-app/f_resources/examrayx.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==2){
                def examrayxInstance = new Examrayx()
                examrayxInstance.code=line[0]
                examrayxInstance.name=line[1]
                if(!examrayxInstance.save(flush: true)){
                    println "fallo en ${examrayxInstance.code}"
                }
            }
        }
        redirect(action: "list")
    }

    def delete() {
        def examrayxInstance = Examrayx.get(params.id)
        if (!examrayxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), params.id])
            redirect(action: "list")
            return
        }

        try {
            examrayxInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'examrayx.label', default: 'Examrayx'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
