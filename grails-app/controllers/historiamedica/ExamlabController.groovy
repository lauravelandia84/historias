package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * ExamlabController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ExamlabController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [examlabInstanceList: Examlab.list(params), examlabInstanceTotal: Examlab.count()]
    }

    def create() {
        [examlabInstance: new Examlab(params)]
    }

    def save() {
        def examlabInstance = new Examlab(params)
        if (!examlabInstance.save(flush: true)) {
            render(view: "create", model: [examlabInstance: examlabInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'examlab.label', default: 'Examlab'), examlabInstance.id])
        redirect(action: "show", id: examlabInstance.id)
    }

    def show() {
        def examlabInstance = Examlab.get(params.id)
        if (!examlabInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'examlab.label', default: 'Examlab'), params.id])
            redirect(action: "list")
            return
        }

        [examlabInstance: examlabInstance]
    }

    def edit() {
        def examlabInstance = Examlab.get(params.id)
        if (!examlabInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'examlab.label', default: 'Examlab'), params.id])
            redirect(action: "list")
            return
        }

        [examlabInstance: examlabInstance]
    }

    def update() {
        def examlabInstance = Examlab.get(params.id)
        if (!examlabInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'examlab.label', default: 'Examlab'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (examlabInstance.version > version) {
                examlabInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'examlab.label', default: 'Examlab')] as Object[],
                          "Another user has updated this Examlab while you were editing")
                render(view: "edit", model: [examlabInstance: examlabInstance])
                return
            }
        }

        examlabInstance.properties = params

        if (!examlabInstance.save(flush: true)) {
            render(view: "edit", model: [examlabInstance: examlabInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'examlab.label', default: 'Examlab'), examlabInstance.id])
        redirect(action: "show", id: examlabInstance.id)
    }

    def delete() {
        def examlabInstance = Examlab.get(params.id)
        if (!examlabInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'examlab.label', default: 'Examlab'), params.id])
            redirect(action: "list")
            return
        }

        try {
            examlabInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'examlab.label', default: 'Examlab'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'examlab.label', default: 'Examlab'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
    def initialLoad() {
        def dir = new File('web-app/f_resources/examlab.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==2){
                def examlabInstance = new Examlab()
                examlabInstance.code=line[0]
                examlabInstance.name=line[1]
                if(!examlabInstance.save(flush: true)){
                    println "fallo en ${examlabInstance.code}"
                }
            }
        }
        redirect(action: "list")
    }
}
