package historiamedica
import org.springframework.dao.DataIntegrityViolationException
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JRExporterParameter
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.export.JRPdfExporter

/**
 * MedicamentoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class MedicamentoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def sessionFactory

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [medicamentoInstanceList: Medicamento.list(params), medicamentoInstanceTotal: Medicamento.count()]
    }

    def create() {
        [medicamentoInstance: new Medicamento(params)]
    }

    def save() {
        def medicamentoInstance = new Medicamento(params)
        if (!medicamentoInstance.save(flush: true)) {
            render(view: "create", model: [medicamentoInstance: medicamentoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), medicamentoInstance.id])
        redirect(action: "show", id: medicamentoInstance.id)
    }

    def show() {
        def medicamentoInstance = Medicamento.get(params.id)
        if (!medicamentoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), params.id])
            redirect(action: "list")
            return
        }

        [medicamentoInstance: medicamentoInstance]
    }

    def edit() {
        def medicamentoInstance = Medicamento.get(params.id)
        if (!medicamentoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), params.id])
            redirect(action: "list")
            return
        }

        [medicamentoInstance: medicamentoInstance]
    }

    def update() {
        def medicamentoInstance = Medicamento.get(params.id)
        if (!medicamentoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (medicamentoInstance.version > version) {
                medicamentoInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'medicamento.label', default: 'Medicamento')] as Object[],
                          "Another user has updated this Medicamento while you were editing")
                render(view: "edit", model: [medicamentoInstance: medicamentoInstance])
                return
            }
        }

        medicamentoInstance.properties = params

        if (!medicamentoInstance.save(flush: true)) {
            render(view: "edit", model: [medicamentoInstance: medicamentoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), medicamentoInstance.id])
        redirect(action: "show", id: medicamentoInstance.id)
    }

    def delete() {
        def medicamentoInstance = Medicamento.get(params.id)
        if (!medicamentoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), params.id])
            redirect(action: "list")
            return
        }

        try {
            medicamentoInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'medicamento.label', default: 'Medicamento'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
    def downList1(){

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                println "id  ${params.id}"

                    parameters.put("id", params.id as long);
                

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ListadoRecipe.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                   
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ListadoRecipe'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    } 
    def initialLoad() {
        def dir = new File('web-app/f_resources/Med_List.csv')
        def lab=''
        def labInstance
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==19){
                def medInstance = new Medicamento()
                println "aqui"
                println line[0]
                println "aqui"
                if(lab=='' || lab!= line[0]) {
                    labInstance=Laboratorios.findByLaboratorio(line[0])

                    lab=labInstance.laboratorio
                }
                medInstance.laboratorios=labInstance
                println medInstance.laboratorios
                medInstance.medicamento=line[1]
                println medInstance.medicamento
                medInstance.nombre=line[2]
                println medInstance.nombre
                medInstance.descripcion=line[3]
                println medInstance.descripcion
                medInstance.composicion=line[4]
                println medInstance.composicion
                medInstance.propiedades=line[5]
                println medInstance.propiedades
                medInstance.farmacología=line[6]
                println medInstance.farmacología
                medInstance.indicaciones=line[7]
                println medInstance.indicaciones
                medInstance.dosificacion=line[8]
                println medInstance.dosificacion
                medInstance.contraindicaciones=line[9]
                println medInstance.contraindicaciones
                medInstance.advertencias=line[10]
                println medInstance.advertencias
                medInstance.reaccionesAdversas=line[11]
                println medInstance.reaccionesAdversas
                medInstance.precauciones=line[12]
                println medInstance.presentaciones
                medInstance.interacciones=line[13]
                println medInstance.interacciones
                medInstance.conservacion=line[14]
                println medInstance.conservacion
                medInstance.sobredosificacion=line[15]
                println medInstance.sobredosificacion
                medInstance.presentaciones=line[16]
                println medInstance.presentaciones
                medInstance.nota=line[17]
                println medInstance.nota
                medInstance.total=line[18]
                println medInstance.total
                if(!medInstance.save(flush: true)){
                    println "fallo en ${medInstance.medicamento}"
                    println medInstance
                }
            }
        }
        initialfarmmedLoad()
        initialprinmedLoad()
    }

    def initialfarmmedLoad() {
        def dir = new File('web-app/f_resources/Farm_Med_List.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==2){
                def medInstance = Medicamento.findByMedicamento(line[0])
                def farmInstance = Farmacologico.findByFarmacologico(line[1])
                MedicamentoFarmacologico.create(medInstance,farmInstance,true)
            }
        }
    }

    def initialprinmedLoad() {
        def dir = new File('web-app/f_resources/Prin_Med_List.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==2){
                def medInstance = Medicamento.findByMedicamento(line[0])
                def prinInstance = Principio.findByPrincipio(line[1])
                MedicamentoPrincipio.create(medInstance,prinInstance,true)
            }
        }
        redirect(action: "list")
    }
}
