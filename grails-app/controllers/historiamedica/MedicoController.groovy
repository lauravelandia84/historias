package historiamedica
import org.hibernate.criterion.CriteriaSpecification

import org.springframework.dao.DataIntegrityViolationException

import org.hibernate.SessionFactory
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperExportManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.export.JRXlsExporter
import net.sf.jasperreports.engine.JRExporterParameter
import net.sf.jasperreports.engine.export.JRXlsExporterParameter
import net.sf.jasperreports.engine.export.JRPdfExporter
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JREmptyDataSource

/**
 * MedicoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class MedicoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def sessionFactory
    
    def index() {
        redirect(action: "list", params: params)
    }
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        def medicoList = Medico.createCriteria().list(params) {
            if ( params.query ) {
                ilike("name", "%${params.query}%")
            }
        }
        [medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
    }

    def downList1(){
        println "Parametros: ${params}"

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                if(params.search){
                    println "Enviar parametros"
                    def fechaIni = new Date().parse("yyyy-MM-dd", params.fechaIni)
                    def fechaFin = new Date().parse("yyyy-MM-dd", params.fechaFin)

                    println "FechaIni: ${fechaIni}"

                    parameters.put("desde", fechaIni);
                    parameters.put("hasta", fechaFin);
                }else{
                    parameters.put("desde", Date.parse( 'dd-MM-yyyy', '01-01-1900' ) );
                    parameters.put("hasta", Date.parse( 'dd-MM-yyyy', '01-01-2100') );
                }

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ListadoMedicos.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                    /*
                    println "Creando Excel Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reports/transaccionesCentro_3.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRXlsExporter exporter = new JRXlsExporter();
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();*/
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ListadoMedicos'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    }
    def downList2(){
        println "Parametros: ${params}"

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                if(params.search){
                    println "Enviar parametros"
                    def description = params.description
                    parameters.put("description", description);
                    
                }else{
                    parameters.put("description", description);
                }

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ListadoMedicos1.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                    /*
                    println "Creando Excel Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reports/transaccionesCentro_3.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRXlsExporter exporter = new JRXlsExporter();
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();*/
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ListadoMedicos'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    }
    def list1() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        def medicoList = Medico.createCriteria().list() {
            if(params.search){
                
                params.dateInit="${params.dateInit}" 
                params.dateFinish="${params.dateFinish}"
                def fechaini=new Date().parse("yyyy-MM-dd",params.dateInit)
                def fechafin=new Date().parse("yyyy-MM-dd",params.dateFinish)
                between('dateEntry', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
            }

        //params.dateInit="${params.dateInit} 23:59:59"
        //params.dateFinish="${params.dateFinish} 00:00:00"
        //def fechaini=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateInit)
        //def fechafin=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateFinish)
        
        
        }  
        medicoList.each { println "${it}" }
        def medicoInstanceTotal = medicoList.size
        [medicoInstanceList: medicoList, medicoInstanceTotal: medicoInstanceTotal]
    }

    def list2(){
        //params.max = Math.min(params.max ? params.int('max') : 5, 100)
       def medicoList = Medico.createCriteria().list() {
            if(params.description){
            }
        }
        if(params.description){
                def listMedico = Medico.executeQuery("SELECT m.id,m.name,m.firtsName,m.address,m.dateEntry,e.description FROM Medico m JOIN m.especialidad AS e WHERE e.description like '%${params.description}%'")
            listMedico.each { println "${it}" }
        def medicoInstanceTotal = listMedico.size()
        [medicoInstanceList: listMedico, medicoInstanceTotal: medicoInstanceTotal]
            }
        else{
             def listMedico = Medico.executeQuery("SELECT m.id,m.name,m.firtsName,m.address,m.cedula,m.dateEntry FROM Medico m JOIN m.especialidad AS e")
        listMedico.each { println "${it}" }
        def medicoInstanceTotal = listMedico.size()
        [medicoInstanceList: listMedico, medicoInstanceTotal: medicoInstanceTotal]
        }
        //println "especialidad: ${params.description}"
        //def listMedico = Medico.executeQuery("SELECT m.id,m.name,m.firtsName,m.address,m.cedula,m.dateEntry FROM Medico m JOIN m.especialidad AS e WHERE e.description like '%${params.description}%'")
        //println "longitud: ${listMedico.size()}"
        
        //listMedico.each { println "${it}" }
        //def medicoInstanceTotal = listMedico.size()
        //[medicoInstanceList: listMedico, medicoInstanceTotal: medicoInstanceTotal]
    }

    def listanterior() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [medicoInstanceList: Medico.list(params), medicoInstanceTotal: Medico.count()]
    }

    def create() {
        [medicoInstance: new Medico(params)]
    }

    def save() {
        def medicoInstance = new Medico(params)
        if (!medicoInstance.save(flush: true)) {
            render(view: "create", model: [medicoInstance: medicoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'medico.label', default: 'Medico'), medicoInstance.id])
        redirect(action: "show", id: medicoInstance.id)
    }

    def show() {
        def medicoInstance = Medico.get(params.id)
        if (!medicoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'medico.label', default: 'Medico'), params.id])
            redirect(action: "list")
            return
        }

        [medicoInstance: medicoInstance]
    }

    def edit() {
        def medicoInstance = Medico.get(params.id)
        if (!medicoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'medico.label', default: 'Medico'), params.id])
            redirect(action: "list")
            return
        }

        [medicoInstance: medicoInstance]
    }

    def update() {
        def medicoInstance = Medico.get(params.id)
        if (!medicoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'medico.label', default: 'Medico'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (medicoInstance.version > version) {
                medicoInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'medico.label', default: 'Medico')] as Object[],
                          "Another user has updated this Medico while you were editing")
                render(view: "edit", model: [medicoInstance: medicoInstance])
                return
            }
        }

        medicoInstance.properties = params

        if (!medicoInstance.save(flush: true)) {
            render(view: "edit", model: [medicoInstance: medicoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'medico.label', default: 'Medico'), medicoInstance.id])
        redirect(action: "show", id: medicoInstance.id)
    }

    def delete() {
        def medicoInstance = Medico.get(params.id)
        if (!medicoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'medico.label', default: 'Medico'), params.id])
            redirect(action: "list")
            return
        }

        try {
            medicoInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'medico.label', default: 'Medico'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'medico.label', default: 'Medico'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
