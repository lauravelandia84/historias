package historiamedica

import org.springframework.dao.DataIntegrityViolationException
import grails.converters.*

import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JRExporterParameter
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.export.JRPdfExporter
/**
 * PatientController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class PatientController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def sessionFactory

    def index() {
        redirect(action: "list", params: params)
    }
    def search() {
    }
  
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        def patientList = Patient.createCriteria().list(params) {
            if ( params.query ) {
                ilike("name", "%${params.query}%")
            }
        }
 
        [patientInstanceList: patientList, patientInstanceTotal: patientList.totalCount]
    }
   def list2(){
        //params.max = Math.min(params.max ? params.int('max') : 5, 100)
       
        //def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p WHERE e.name like '%${params.name}%'")
        if(params.name){
        def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p WHERE e.name like '%${params.name}%'")
        listConsultation.each { println "${it}" }
        def consultationInstanceTotal = listConsultation.size()
        [consultationInstanceList: listConsultation, consultationInstanceTotal: consultationInstanceTotal]
        }
        else{
            //def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p WHERE e.name like '%${params.name}%'")
        }

    }
    def list3(){
        if(params.description){
        def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p JOIN e.especialidad AS s WHERE s.description like '%${params.description}%'")
        println "${params.description}"
        listConsultation.each { println "${it}" }
        def consultationInstanceTotal = listConsultation.size()
        [consultationInstanceList: listConsultation, consultationInstanceTotal: consultationInstanceTotal]
        }
        else{
            //def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p WHERE e.name like '%${params.name}%'")
        }
    }
   def downList1(){

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                if(params.search){
                    println "Enviar parametros"
                    def fechaIni = new Date().parse("yyyy-MM-dd", params.fechaIni)
                    def fechaFin = new Date().parse("yyyy-MM-dd", params.fechaFin)

                    println "FechaIni: ${fechaIni}"

                    parameters.put("desde", fechaIni);
                    parameters.put("hasta", fechaFin);
                }else{
                    println "Parametros vacios"
                    parameters.put("desde", new Date() );
                    parameters.put("hasta", new Date() );
                }

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ListadoPacientes.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                    /*
                    println "Creando Excel Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reports/transaccionesCentro_3.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRXlsExporter exporter = new JRXlsExporter();
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();*/
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ListadoPacientes'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    } 

   def downList2(){
        println "Parametros: ${params}"

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                if(params.search){
                    println "Enviar parametros"
                    def name = params.name
                    parameters.put("name", name);
                    
                }else{
                    parameters.put("name", name);
                }

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ListadoPacientes1.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                    /*
                    println "Creando Excel Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reports/transaccionesCentro_3.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRXlsExporter exporter = new JRXlsExporter();
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();*/
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ListadoPacientes'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    }

   def downList3(){
        println "Parametros: ${params}"

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                if(params.search){
                    println "Enviar parametros"
                    def description = params.description
                    parameters.put("description", description);
                    
                }else{
                    parameters.put("description", description);
                }

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ListadoPacientes2.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                    /*
                    println "Creando Excel Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reports/transaccionesCentro_3.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRXlsExporter exporter = new JRXlsExporter();
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_DETECT_CELL_TYPE, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);
                    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.FALSE);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();*/
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ListadoPacientes'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    }
       
   def list1() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        //def patientList = Patient.createCriteria().list() {
            if(params.search){
                params.dateInit="${params.dateInit}" 
                params.dateFinish="${params.dateFinish}"
                def fechaini=new Date().parse("yyyy-MM-dd",params.dateInit)
                def fechafin=new Date().parse("yyyy-MM-dd",params.dateFinish)
                def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.patient AS p WHERE m.date between '${params.dateInit}' AND '${params.dateFinish}'")
                listConsultation.each { println "${it}" }
                println "${params.dateInit}"
                println "${params.dateFinish}"
                def consultationInstanceTotal = listConsultation.size()
                [consultationInstanceList: listConsultation, consultationInstanceTotal: consultationInstanceTotal]
                //between('date', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
            }
            
        //params.dateInit="${params.dateInit} 23:59:59"
        //params.dateFinish="${params.dateFinish} 00:00:00"
        //def fechaini=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateInit)
        //def fechafin=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateFinish)
    
        
        //patientList.each { println "${it}" }
        //def patientInstanceTotal = patientList.size
        //[patientInstanceList: patientList, patientInstanceTotal: patientInstanceTotal]
    }
    def create() {
        [patientInstance: new Patient(params)]
    }

    def save() {
        def patientInstance = new Patient(params)
        if (!patientInstance.save(flush: true)) {
            render(view: "create", model: [patientInstance: patientInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'patient.label', default: 'Patient'), patientInstance.id])
        redirect(action: "show", id: patientInstance.id)
    }
   
    def show() {
        def patientInstance = Patient.get(params.id)
        if (!patientInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), params.id])
            redirect(action: "list")
            return
        }

        [patientInstance: patientInstance]
    }

    def edit() {
        def patientInstance = Patient.get(params.id)
        if (!patientInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), params.id])
            redirect(action: "list")
            return
        }

        [patientInstance: patientInstance]
    }

    def update() {
        def patientInstance = Patient.get(params.id)
        if (!patientInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (patientInstance.version > version) {
                patientInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'patient.label', default: 'Patient')] as Object[],
                          "Another user has updated this Patient while you were editing")
                render(view: "edit", model: [patientInstance: patientInstance])
                return
            }
        }

        patientInstance.properties = params

        if (!patientInstance.save(flush: true)) {
            render(view: "edit", model: [patientInstance: patientInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'patient.label', default: 'Patient'), patientInstance.id])
        redirect(action: "show", id: patientInstance.id)
    }
    def showImage() {
        def patientInstance = Patient.get(params.id)
        response.outputStream << patientInstance.image 
        response.outputStream.flush()
    }
    def delete() {
        def patientInstance = Patient.get(params.id)
        if (!patientInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'patient.label', default: 'Patient'), params.id])
            redirect(action: "list")
            return
        }

        try {
            patientInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'patient.label', default: 'Patient'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'patient.label', default: 'Patient'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
    
}
