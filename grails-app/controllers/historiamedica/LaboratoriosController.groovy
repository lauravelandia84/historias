package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * LaboratoriosController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class LaboratoriosController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [laboratoriosInstanceList: Laboratorios.list(params), laboratoriosInstanceTotal: Laboratorios.count()]
    }

    def create() {
        [laboratoriosInstance: new Laboratorios(params)]
    }

    def save() {
        def laboratoriosInstance = new Laboratorios(params)
        if (!laboratoriosInstance.save(flush: true)) {
            render(view: "create", model: [laboratoriosInstance: laboratoriosInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), laboratoriosInstance.id])
        redirect(action: "show", id: laboratoriosInstance.id)
    }

    def show() {
        def laboratoriosInstance = Laboratorios.get(params.id)
        if (!laboratoriosInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), params.id])
            redirect(action: "list")
            return
        }

        [laboratoriosInstance: laboratoriosInstance]
    }

    def edit() {
        def laboratoriosInstance = Laboratorios.get(params.id)
        if (!laboratoriosInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), params.id])
            redirect(action: "list")
            return
        }

        [laboratoriosInstance: laboratoriosInstance]
    }

    def update() {
        def laboratoriosInstance = Laboratorios.get(params.id)
        if (!laboratoriosInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (laboratoriosInstance.version > version) {
                laboratoriosInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'laboratorios.label', default: 'Laboratorios')] as Object[],
                          "Another user has updated this Laboratorios while you were editing")
                render(view: "edit", model: [laboratoriosInstance: laboratoriosInstance])
                return
            }
        }

        laboratoriosInstance.properties = params

        if (!laboratoriosInstance.save(flush: true)) {
            render(view: "edit", model: [laboratoriosInstance: laboratoriosInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), laboratoriosInstance.id])
        redirect(action: "show", id: laboratoriosInstance.id)
    }

    def delete() {
        def laboratoriosInstance = Laboratorios.get(params.id)
        if (!laboratoriosInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), params.id])
            redirect(action: "list")
            return
        }

        try {
            laboratoriosInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'laboratorios.label', default: 'Laboratorios'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def initialLoad() {
        def dir = new File('web-app/f_resources/Lab_List.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==4){
                def labInstance = new Laboratorios()
                labInstance.laboratorio=line[0]
                labInstance.ciudad=line[1]
                labInstance.telefono=line[2]
                labInstance.dirección=line[3]
                println labInstance
                if(!labInstance.save(flush: true)){
                    println "fallo en ${labInstance.laboratorio}"
                }
            }
        }
        redirect(action: "list")
    }
}
