package historiamedica

import org.springframework.dao.DataIntegrityViolationException
import historiamedica.Subexamlab
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JRExporterParameter
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.export.JRPdfExporter

/**
 * ConsultationController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ConsultationController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def sessionFactory

    def index() {
        redirect(action: "list", params: params)
    }

    def list_anterior() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [consultationInstanceList: Consultation.list(params), consultationInstanceTotal: Consultation.count()]
    }
    def list() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        //def patientList = Patient.createCriteria().list() {
          
                //def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p JOIN e.especialidad AS s WHERE s.description like '%${params.description}%'")
             def listConsultation = Consultation.executeQuery("SELECT m.date, CONCAT(p.name, p.firtsName), CONCAT(mm.name, mm.firtsName), e.description, m.id FROM Consultation m JOIN m.patient p JOIN m.medico AS mm JOIN mm.especialidad AS e")
                listConsultation.each { println "${it}" }
              
                def consultationInstanceTotal = listConsultation.size()
                [consultationInstanceList: listConsultation, consultationInstanceTotal: consultationInstanceTotal]
                //between('date', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
           
    }
    def list1() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        //def patientList = Patient.createCriteria().list() {
            if(params.search){
                params.dateInit="${params.dateInit}" 
                def fechaini=new Date().parse("yyyy-MM-dd",params.dateInit)
                //def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p JOIN e.especialidad AS s WHERE s.description like '%${params.description}%'")
             def listConsultation = Consultation.executeQuery("SELECT distinct m.date, p.name, mm.name, m.id FROM Consultation m JOIN m.patient p JOIN m.medico AS mm JOIN m.subexamlab AS r WHERE m.date = '${params.dateInit}'")
                listConsultation.each { println "${it}" }
                println "${params.dateInit}"
              
                def consultationInstanceTotal = listConsultation.size()
                [consultationInstanceList: listConsultation, consultationInstanceTotal: consultationInstanceTotal]
                //between('date', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
            }
            
        //params.dateInit="${params.dateInit} 23:59:59"
        //params.dateFinish="${params.dateFinish} 00:00:00"
        //def fechaini=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateInit)
        //def fechafin=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateFinish)
    
        
        //patientList.each { println "${it}" }
        //def patientInstanceTotal = patientList.size
        //[patientInstanceList: patientList, patientInstanceTotal: patientInstanceTotal]
    }
    def list2() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        //def patientList = Patient.createCriteria().list() {
            if(params.search){
                params.dateInit="${params.dateInit}" 
                def fechaini=new Date().parse("yyyy-MM-dd",params.dateInit)
                //def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p JOIN e.especialidad AS s WHERE s.description like '%${params.description}%'")
             def listConsultation = Consultation.executeQuery("SELECT distinct m.date, p.name, mm.name, m.id FROM Consultation m JOIN m.patient p JOIN m.medico AS mm JOIN m.subexamrayx AS r WHERE m.date = '${params.dateInit}' ")
                listConsultation.each { println "${it}" }
                println "${params.dateInit}"
              
                def consultationInstanceTotal = listConsultation.size()
                [consultationInstanceList: listConsultation, consultationInstanceTotal: consultationInstanceTotal]
                //between('date', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
            }
            
        //params.dateInit="${params.dateInit} 23:59:59"
        //params.dateFinish="${params.dateFinish} 00:00:00"
        //def fechaini=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateInit)
        //def fechafin=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateFinish)
    
        
        //patientList.each { println "${it}" }
        //def patientInstanceTotal = patientList.size
        //[patientInstanceList: patientList, patientInstanceTotal: patientInstanceTotal]
    }
        def list3() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        //def patientList = Patient.createCriteria().list() {
            if(params.search){
                params.dateInit="${params.dateInit}" 
                def fechaini=new Date().parse("yyyy-MM-dd",params.dateInit)
                //def listConsultation = Consultation.executeQuery("SELECT p.name,p.firtsName,p.gender,p.statusMarital,p.groupSanguineo,p.allergy FROM Consultation m JOIN m.medico AS e JOIN m.patient AS p JOIN e.especialidad AS s WHERE s.description like '%${params.description}%'")
             def listConsultation = Consultation.executeQuery("SELECT distinct m.date, p.name, mm.name, m.id FROM Consultation m JOIN m.patient p JOIN m.medico AS mm JOIN m.medicamento AS r WHERE m.date = '${params.dateInit}' ")
                listConsultation.each { println "${it}" }
                println "${params.dateInit}"
              
                def consultationInstanceTotal = listConsultation.size()
                [consultationInstanceList: listConsultation, consultationInstanceTotal: consultationInstanceTotal]
                //between('date', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
            }
            
        //params.dateInit="${params.dateInit} 23:59:59"
        //params.dateFinish="${params.dateFinish} 00:00:00"
        //def fechaini=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateInit)
        //def fechafin=new Date().parse("dd/MM/yyyy HH:mm:ss",params.dateFinish)
    
        
        //patientList.each { println "${it}" }
        //def patientInstanceTotal = patientList.size
        //[patientInstanceList: patientList, patientInstanceTotal: patientInstanceTotal]
    }
    def downList1(){

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                println "id  ${params.id}"

                    parameters.put("id", params.id as long);
                

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ExamenesLaboratorio.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                   
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ExamenLaboratorio'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    } 

    def downList2(){

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                println "id  ${params.id}"

                    parameters.put("id", params.id as long);
                

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ExamenesRayosx.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                   
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ExamenRayosx'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    }
    def downList3(){

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                println "id  ${params.id}"

                    parameters.put("id", params.id as long);
                

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/Recipe.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                   
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'Recipe'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    }
    def create() {
        [consultationInstance: new Consultation(params)]
    }

    def save() {
        def consultationInstance = new Consultation(params)
        println "llego a guardar la consulta"
        if (!consultationInstance.save(flush: true)) {
            render(view: "create", model: [consultationInstance: consultationInstance])
            return
        }
        println "paso el guardado de la consulta"
        def f
        request.getMultiFileMap().img.each {
            println it
            println it.originalFilename
            //f = it.getFile()
            def i = new Images()
            i.filename=it.originalFilename
            i.image=it.getBytes()
            consultationInstance.addToImages(i)
            consultationInstance.save(flush: true)
            println "guardo imagen"
        }
        println "paso el guardado de la instancia nuevamente"

        params.each {name, value ->
            if(name.contains('subexam_')){
                    println "Name: ${name}, Value: ${value}"
                    def subexamlab = Subexamlab.get(value as long)
                    consultationInstance.addToSubexamlab(subexamlab)
                    consultationInstance.save(flush: true)
            }

            if(name.contains('tn3icd10_')){
                println "Name: ${name}, Value: ${value}"
                def tn3icd10 = Tn3icd10.get(value as long)
                consultationInstance.addToTn3icd10(tn3icd10)
                consultationInstance.save(flush: true)
            }
            if(name.contains('subexamrayx_')){
                    println "Name: ${name}, Value: ${value}"
                    def subexamrayx = Subexamrayx.get(value as long)
                    consultationInstance.addToSubexamrayx(subexamrayx)
                    consultationInstance.save(flush: true)
            }
        }


        flash.message = message(code: 'default.created.message', args: [message(code: 'consultation.label', default: 'Consultation'), consultationInstance.id])
        redirect(action: "show", id: consultationInstance.id)
    }

    def show() {
        def consultationInstance = Consultation.get(params.id)
        if (!consultationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultation.label', default: 'Consultation'), params.id])
            redirect(action: "list")
            return
        }

        [consultationInstance: consultationInstance]
    }

    def edit() {
        def consultationInstance = Consultation.get(params.id)
        if (!consultationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultation.label', default: 'Consultation'), params.id])
            redirect(action: "list")
            return
        }

        [consultationInstance: consultationInstance]
    }

    def update() {
        def consultationInstance = Consultation.get(params.id)
        if (!consultationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultation.label', default: 'Consultation'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (consultationInstance.version > version) {
                consultationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'consultation.label', default: 'Consultation')] as Object[],
                          "Another user has updated this Consultation while you were editing")
                render(view: "edit", model: [consultationInstance: consultationInstance])
                return
            }
        }

        consultationInstance.properties = params

        if (!consultationInstance.save(flush: true)) {
            render(view: "edit", model: [consultationInstance: consultationInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'consultation.label', default: 'Consultation'), consultationInstance.id])
        redirect(action: "show", id: consultationInstance.id)
    }

    def delete() {
        def consultationInstance = Consultation.get(params.id)
        if (!consultationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultation.label', default: 'Consultation'), params.id])
            redirect(action: "list")
            return
        }

        try {
            consultationInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'consultation.label', default: 'Consultation'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'consultation.label', default: 'Consultation'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
