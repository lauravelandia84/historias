package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * ImagesController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ImagesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [imagesInstanceList: Images.list(params), imagesInstanceTotal: Images.count()]
    }

    def create() {
        [imagesInstance: new Images(params)]
    }

    def save() {
        def imagesInstance = new Images(params)
        if (!imagesInstance.save(flush: true)) {
            render(view: "create", model: [imagesInstance: imagesInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'images.label', default: 'Images'), imagesInstance.id])
        redirect(action: "show", id: imagesInstance.id)
    }

    def show() {
        def imagesInstance = Images.get(params.id)
        if (!imagesInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'images.label', default: 'Images'), params.id])
            redirect(action: "list")
            return
        }

        [imagesInstance: imagesInstance]
    }

    def edit() {
        def imagesInstance = Images.get(params.id)
        if (!imagesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'images.label', default: 'Images'), params.id])
            redirect(action: "list")
            return
        }

        [imagesInstance: imagesInstance]
    }

    def update() {
        def imagesInstance = Images.get(params.id)
        if (!imagesInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'images.label', default: 'Images'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (imagesInstance.version > version) {
                imagesInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'images.label', default: 'Images')] as Object[],
                          "Another user has updated this Images while you were editing")
                render(view: "edit", model: [imagesInstance: imagesInstance])
                return
            }
        }

        imagesInstance.properties = params

        if (!imagesInstance.save(flush: true)) {
            render(view: "edit", model: [imagesInstance: imagesInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'images.label', default: 'Images'), imagesInstance.id])
        redirect(action: "show", id: imagesInstance.id)
    }

    def delete() {
        def imagesInstance = Images.get(params.id)
        if (!imagesInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'images.label', default: 'Images'), params.id])
            redirect(action: "list")
            return
        }

        try {
            imagesInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'images.label', default: 'Images'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'images.label', default: 'Images'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
    def showImage() {
        def imagesInstance = Images.get(params.id)
        response.outputStream << imagesInstance.image
        response.outputStream.flush()
    }
}
