package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * TypedocumentController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class TypedocumentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [typedocumentInstanceList: Typedocument.list(params), typedocumentInstanceTotal: Typedocument.count()]
    }

    def create() {
        [typedocumentInstance: new Typedocument(params)]
    }

    def save() {
        def typedocumentInstance = new Typedocument(params)
        if (!typedocumentInstance.save(flush: true)) {
            render(view: "create", model: [typedocumentInstance: typedocumentInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), typedocumentInstance.id])
        redirect(action: "show", id: typedocumentInstance.id)
    }

    def show() {
        def typedocumentInstance = Typedocument.get(params.id)
        if (!typedocumentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), params.id])
            redirect(action: "list")
            return
        }

        [typedocumentInstance: typedocumentInstance]
    }

    def edit() {
        def typedocumentInstance = Typedocument.get(params.id)
        if (!typedocumentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), params.id])
            redirect(action: "list")
            return
        }

        [typedocumentInstance: typedocumentInstance]
    }

    def update() {
        def typedocumentInstance = Typedocument.get(params.id)
        if (!typedocumentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (typedocumentInstance.version > version) {
                typedocumentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'typedocument.label', default: 'Typedocument')] as Object[],
                          "Another user has updated this Typedocument while you were editing")
                render(view: "edit", model: [typedocumentInstance: typedocumentInstance])
                return
            }
        }

        typedocumentInstance.properties = params

        if (!typedocumentInstance.save(flush: true)) {
            render(view: "edit", model: [typedocumentInstance: typedocumentInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), typedocumentInstance.id])
        redirect(action: "show", id: typedocumentInstance.id)
    }

    def delete() {
        def typedocumentInstance = Typedocument.get(params.id)
        if (!typedocumentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), params.id])
            redirect(action: "list")
            return
        }

        try {
            typedocumentInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'typedocument.label', default: 'Typedocument'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
