package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * Tn2icd10Controller
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class Tn2icd10Controller {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def getTn2icd10 = {
        def tn1icd10 = Tn1icd10.get(params.id)

        def tn2icd10List = tn1icd10?.tn2icd10

        render(template: "selectTn2icd10", model: [tn2icd10List: tn2icd10List])
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tn2icd10InstanceList: Tn2icd10.list(params), tn2icd10InstanceTotal: Tn2icd10.count()]
    }

    def create() {
        [tn2icd10Instance: new Tn2icd10(params)]
    }

    def save() {
        def tn2icd10Instance = new Tn2icd10(params)
        if (!tn2icd10Instance.save(flush: true)) {
            render(view: "create", model: [tn2icd10Instance: tn2icd10Instance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), tn2icd10Instance.id])
        redirect(action: "show", id: tn2icd10Instance.id)
    }

    def show() {
        def tn2icd10Instance = Tn2icd10.get(params.id)
        if (!tn2icd10Instance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), params.id])
            redirect(action: "list")
            return
        }

        [tn2icd10Instance: tn2icd10Instance]
    }

    def edit() {
        def tn2icd10Instance = Tn2icd10.get(params.id)
        if (!tn2icd10Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), params.id])
            redirect(action: "list")
            return
        }

        [tn2icd10Instance: tn2icd10Instance]
    }

    def update() {
        def tn2icd10Instance = Tn2icd10.get(params.id)
        if (!tn2icd10Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (tn2icd10Instance.version > version) {
                tn2icd10Instance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'tn2icd10.label', default: 'Tn2icd10')] as Object[],
                          "Another user has updated this Tn2icd10 while you were editing")
                render(view: "edit", model: [tn2icd10Instance: tn2icd10Instance])
                return
            }
        }

        tn2icd10Instance.properties = params

        if (!tn2icd10Instance.save(flush: true)) {
            render(view: "edit", model: [tn2icd10Instance: tn2icd10Instance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), tn2icd10Instance.id])
        redirect(action: "show", id: tn2icd10Instance.id)
    }

    def delete() {
        def tn2icd10Instance = Tn2icd10.get(params.id)
        if (!tn2icd10Instance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), params.id])
            redirect(action: "list")
            return
        }

        try {
            tn2icd10Instance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tn2icd10.label', default: 'Tn2icd10'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
