package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * TypeconsultationController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class TypeconsultationController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [typeconsultationInstanceList: Typeconsultation.list(params), typeconsultationInstanceTotal: Typeconsultation.count()]
    }

    def create() {
        [typeconsultationInstance: new Typeconsultation(params)]
    }

    def save() {
        def typeconsultationInstance = new Typeconsultation(params)
        if (!typeconsultationInstance.save(flush: true)) {
            render(view: "create", model: [typeconsultationInstance: typeconsultationInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), typeconsultationInstance.id])
        redirect(action: "show", id: typeconsultationInstance.id)
    }

    def show() {
        def typeconsultationInstance = Typeconsultation.get(params.id)
        if (!typeconsultationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), params.id])
            redirect(action: "list")
            return
        }

        [typeconsultationInstance: typeconsultationInstance]
    }

    def edit() {
        def typeconsultationInstance = Typeconsultation.get(params.id)
        if (!typeconsultationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), params.id])
            redirect(action: "list")
            return
        }

        [typeconsultationInstance: typeconsultationInstance]
    }

    def update() {
        def typeconsultationInstance = Typeconsultation.get(params.id)
        if (!typeconsultationInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (typeconsultationInstance.version > version) {
                typeconsultationInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'typeconsultation.label', default: 'Typeconsultation')] as Object[],
                          "Another user has updated this Typeconsultation while you were editing")
                render(view: "edit", model: [typeconsultationInstance: typeconsultationInstance])
                return
            }
        }

        typeconsultationInstance.properties = params

        if (!typeconsultationInstance.save(flush: true)) {
            render(view: "edit", model: [typeconsultationInstance: typeconsultationInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), typeconsultationInstance.id])
        redirect(action: "show", id: typeconsultationInstance.id)
    }

    def delete() {
        def typeconsultationInstance = Typeconsultation.get(params.id)
        if (!typeconsultationInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), params.id])
            redirect(action: "list")
            return
        }

        try {
            typeconsultationInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'typeconsultation.label', default: 'Typeconsultation'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
