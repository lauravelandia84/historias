package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * SubexamrayxController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class SubexamrayxController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def getSubexamrayx = {
        // Se obtiene el id del Examlab
             def examrayxInstance = Examrayx.get(params.id)
        // Se listan los Subexamlab
             def subexamrayxList = examrayxInstance?.subexamrayx
        // println "subexamlabList => ${subexamlabList}"

        render(template: "selectSubexamrayx", model: [subexamrayxList: subexamrayxList])
    }    

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [subexamrayxInstanceList: Subexamrayx.list(params), subexamrayxInstanceTotal: Subexamrayx.count()]
    }

    def create() {
        [subexamrayxInstance: new Subexamrayx(params)]
    }

    def save() {
        def subexamrayxInstance = new Subexamrayx(params)
        if (!subexamrayxInstance.save(flush: true)) {
            render(view: "create", model: [subexamrayxInstance: subexamrayxInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), subexamrayxInstance.id])
        redirect(action: "show", id: subexamrayxInstance.id)
    }

    def show() {
        def subexamrayxInstance = Subexamrayx.get(params.id)
        if (!subexamrayxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), params.id])
            redirect(action: "list")
            return
        }

        [subexamrayxInstance: subexamrayxInstance]
    }

    def edit() {
        def subexamrayxInstance = Subexamrayx.get(params.id)
        if (!subexamrayxInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), params.id])
            redirect(action: "list")
            return
        }

        [subexamrayxInstance: subexamrayxInstance]
    }

    def update() {
        def subexamrayxInstance = Subexamrayx.get(params.id)
        if (!subexamrayxInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (subexamrayxInstance.version > version) {
                subexamrayxInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'subexamrayx.label', default: 'Subexamrayx')] as Object[],
                          "Another user has updated this Subexamrayx while you were editing")
                render(view: "edit", model: [subexamrayxInstance: subexamrayxInstance])
                return
            }
        }

        subexamrayxInstance.properties = params

        if (!subexamrayxInstance.save(flush: true)) {
            render(view: "edit", model: [subexamrayxInstance: subexamrayxInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), subexamrayxInstance.id])
        redirect(action: "show", id: subexamrayxInstance.id)
    }
    def initialLoad() {
        def dir = new File('web-app/f_resources/subexam_rayx.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==3){
                def subexamrayxInstance = new Subexamrayx()
                def examrayxInstance = Examrayx.findByCode(line[0])
                subexamrayxInstance.examrayx=examrayxInstance
                
                subexamrayxInstance.code=line[0]
                subexamrayxInstance.idline=line[1]
                subexamrayxInstance.description=line[2]
              
                if(!subexamrayxInstance.save(flush: true)){
                    println "fallo en ${subexamrayxInstance.code}"
                }
            }
        }
        redirect(action: "list")
    }
    def delete() {
        def subexamrayxInstance = Subexamrayx.get(params.id)
        if (!subexamrayxInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), params.id])
            redirect(action: "list")
            return
        }

        try {
            subexamrayxInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subexamrayx.label', default: 'Subexamrayx'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
