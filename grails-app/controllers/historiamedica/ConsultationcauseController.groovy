package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * ConsultationcauseController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class ConsultationcauseController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [consultationcauseInstanceList: Consultationcause.list(params), consultationcauseInstanceTotal: Consultationcause.count()]
    }

    def create() {
        [consultationcauseInstance: new Consultationcause(params)]
    }

    def save() {
        def consultationcauseInstance = new Consultationcause(params)
        if (!consultationcauseInstance.save(flush: true)) {
            render(view: "create", model: [consultationcauseInstance: consultationcauseInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), consultationcauseInstance.id])
        redirect(action: "show", id: consultationcauseInstance.id)
    }

    def show() {
        def consultationcauseInstance = Consultationcause.get(params.id)
        if (!consultationcauseInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), params.id])
            redirect(action: "list")
            return
        }

        [consultationcauseInstance: consultationcauseInstance]
    }

    def edit() {
        def consultationcauseInstance = Consultationcause.get(params.id)
        if (!consultationcauseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), params.id])
            redirect(action: "list")
            return
        }

        [consultationcauseInstance: consultationcauseInstance]
    }

    def update() {
        def consultationcauseInstance = Consultationcause.get(params.id)
        if (!consultationcauseInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (consultationcauseInstance.version > version) {
                consultationcauseInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'consultationcause.label', default: 'Consultationcause')] as Object[],
                          "Another user has updated this Consultationcause while you were editing")
                render(view: "edit", model: [consultationcauseInstance: consultationcauseInstance])
                return
            }
        }

        consultationcauseInstance.properties = params

        if (!consultationcauseInstance.save(flush: true)) {
            render(view: "edit", model: [consultationcauseInstance: consultationcauseInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), consultationcauseInstance.id])
        redirect(action: "show", id: consultationcauseInstance.id)
    }

    def initialLoad() {
        def dir = new File('web-app/f_resources/motivo.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
         
            if(line.length==2){
                def consultationcauseInstance = new Consultationcause()
                consultationcauseInstance.code=line[0]
                consultationcauseInstance.name=line[1]
                if(!consultationcauseInstance.save(flush: true)){
                    println "fallo en ${consultationcauseInstance.code}"
                }
            }
        }
        redirect(action: "list")
    }
    def delete() {
        def consultationcauseInstance = Consultationcause.get(params.id)
        if (!consultationcauseInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), params.id])
            redirect(action: "list")
            return
        }

        try {
            consultationcauseInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'consultationcause.label', default: 'Consultationcause'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
