package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * FarmacologicoController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class FarmacologicoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [farmacologicoInstanceList: Farmacologico.list(params), farmacologicoInstanceTotal: Farmacologico.count()]
    }

    def create() {
        [farmacologicoInstance: new Farmacologico(params)]
    }

    def save() {
        def farmacologicoInstance = new Farmacologico(params)
        if (!farmacologicoInstance.save(flush: true)) {
            render(view: "create", model: [farmacologicoInstance: farmacologicoInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), farmacologicoInstance.id])
        redirect(action: "show", id: farmacologicoInstance.id)
    }

    def show() {
        def farmacologicoInstance = Farmacologico.get(params.id)
        if (!farmacologicoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), params.id])
            redirect(action: "list")
            return
        }

        [farmacologicoInstance: farmacologicoInstance]
    }

    def edit() {
        def farmacologicoInstance = Farmacologico.get(params.id)
        if (!farmacologicoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), params.id])
            redirect(action: "list")
            return
        }

        [farmacologicoInstance: farmacologicoInstance]
    }

    def update() {
        def farmacologicoInstance = Farmacologico.get(params.id)
        if (!farmacologicoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (farmacologicoInstance.version > version) {
                farmacologicoInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'farmacologico.label', default: 'Farmacologico')] as Object[],
                          "Another user has updated this Farmacologico while you were editing")
                render(view: "edit", model: [farmacologicoInstance: farmacologicoInstance])
                return
            }
        }

        farmacologicoInstance.properties = params

        if (!farmacologicoInstance.save(flush: true)) {
            render(view: "edit", model: [farmacologicoInstance: farmacologicoInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), farmacologicoInstance.id])
        redirect(action: "show", id: farmacologicoInstance.id)
    }

    def delete() {
        def farmacologicoInstance = Farmacologico.get(params.id)
        if (!farmacologicoInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), params.id])
            redirect(action: "list")
            return
        }

        try {
            farmacologicoInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'farmacologico.label', default: 'Farmacologico'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def initialLoad() {
        def dir = new File('web-app/f_resources/Farm_List.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==1){
                def farmInstance = new Farmacologico()
                farmInstance.farmacologico=line[0]
                // println labInstance
                if(!farmInstance.save(flush: true)){
                    println "fallo en ${farmInstance.farmacologico}"
                }
            }
        }
        redirect(action: "list")
    }
}
