package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * Tn1icd10Controller
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class Tn1icd10Controller {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tn1icd10InstanceList: Tn1icd10.list(params), tn1icd10InstanceTotal: Tn1icd10.count()]
    }

    def create() {
        [tn1icd10Instance: new Tn1icd10(params)]
    }

    def save() {
        def tn1icd10Instance = new Tn1icd10(params)
        if (!tn1icd10Instance.save(flush: true)) {
            render(view: "create", model: [tn1icd10Instance: tn1icd10Instance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), tn1icd10Instance.id])
        redirect(action: "show", id: tn1icd10Instance.id)
    }

    def show() {
        def tn1icd10Instance = Tn1icd10.get(params.id)
        if (!tn1icd10Instance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), params.id])
            redirect(action: "list")
            return
        }

        [tn1icd10Instance: tn1icd10Instance]
    }

    def edit() {
        def tn1icd10Instance = Tn1icd10.get(params.id)
        if (!tn1icd10Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), params.id])
            redirect(action: "list")
            return
        }

        [tn1icd10Instance: tn1icd10Instance]
    }

    def update() {
        def tn1icd10Instance = Tn1icd10.get(params.id)
        if (!tn1icd10Instance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (tn1icd10Instance.version > version) {
                tn1icd10Instance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'tn1icd10.label', default: 'Tn1icd10')] as Object[],
                          "Another user has updated this Tn1icd10 while you were editing")
                render(view: "edit", model: [tn1icd10Instance: tn1icd10Instance])
                return
            }
        }

        tn1icd10Instance.properties = params

        if (!tn1icd10Instance.save(flush: true)) {
            render(view: "edit", model: [tn1icd10Instance: tn1icd10Instance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), tn1icd10Instance.id])
        redirect(action: "show", id: tn1icd10Instance.id)
    }

    def delete() {
        def tn1icd10Instance = Tn1icd10.get(params.id)
        if (!tn1icd10Instance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), params.id])
            redirect(action: "list")
            return
        }

        try {
            tn1icd10Instance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tn1icd10.label', default: 'Tn1icd10'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def initialLoad() {
        def dir = new File('web-app/f_resources/Ticd10_List.csv')
        def n1icd10Instance = new Tn1icd10()
        def n2icd10Instance = new Tn2icd10()
        def tn1=''
        def tn2=''
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==7){
                if(tn1=='' || tn1!=line[0]) {
                    tn1=line[0]
                    n1icd10Instance = new Tn1icd10()
                    n1icd10Instance.descripcion=line[0]
                    n1icd10Instance.categoria=line[1]
                    n1icd10Instance.save(flush: true)
                }
                if(tn2=='' || tn2!=line[2]) {
                    tn2=line[2]
                    n2icd10Instance = new Tn2icd10()
                    n2icd10Instance.descripcion=line[2]
                    n2icd10Instance.categoria=line[3]
                    n2icd10Instance.subcategoria=line[4]
                    n1icd10Instance.addToTn2icd10(n2icd10Instance)
                    n1icd10Instance.save(flush: true)
                }
                def n3icd10Instance = new Tn3icd10()
                n3icd10Instance.descripcion=line[5]
                n3icd10Instance.categoria=line[6]
                n2icd10Instance.addToTn3icd10(n3icd10Instance)
                n1icd10Instance.save(flush: true)
            }
        }
        redirect(action: "list")
    }
}
