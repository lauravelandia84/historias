package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * SubexamlabController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class SubexamlabController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def getSubexamlabs = {
        // Se obtiene el id del Examlab
             def examlabInstance = Examlab.get(params.id)
        // Se listan los Subexamlab
             def subexamlabList = examlabInstance?.subexamlab
        // println "subexamlabList => ${subexamlabList}"

        render(template: "selectSubexamlab", model: [subexamlabList: subexamlabList])
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [subexamlabInstanceList: Subexamlab.list(params), subexamlabInstanceTotal: Subexamlab.count()]
    }

    def create() {
        [subexamlabInstance: new Subexamlab(params)]
    }

    def save() {
        def subexamlabInstance = new Subexamlab(params)
        if (!subexamlabInstance.save(flush: true)) {
            render(view: "create", model: [subexamlabInstance: subexamlabInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), subexamlabInstance.id])
        redirect(action: "show", id: subexamlabInstance.id)
    }

    def show() {
        def subexamlabInstance = Subexamlab.get(params.id)
        if (!subexamlabInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), params.id])
            redirect(action: "list")
            return
        }

        [subexamlabInstance: subexamlabInstance]
    }

    def edit() {
        def subexamlabInstance = Subexamlab.get(params.id)
        if (!subexamlabInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), params.id])
            redirect(action: "list")
            return
        }

        [subexamlabInstance: subexamlabInstance]
    }

    def update() {
        def subexamlabInstance = Subexamlab.get(params.id)
        if (!subexamlabInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (subexamlabInstance.version > version) {
                subexamlabInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'subexamlab.label', default: 'Subexamlab')] as Object[],
                          "Another user has updated this Subexamlab while you were editing")
                render(view: "edit", model: [subexamlabInstance: subexamlabInstance])
                return
            }
        }

        subexamlabInstance.properties = params

        if (!subexamlabInstance.save(flush: true)) {
            render(view: "edit", model: [subexamlabInstance: subexamlabInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), subexamlabInstance.id])
        redirect(action: "show", id: subexamlabInstance.id)
    }
    def initialLoad() {
        def dir = new File('web-app/f_resources/subexamlab.csv')
        dir.toCsvReader(['skipLines': '1']).eachLine { line ->
            if(line.length==3){
                def subexamlabInstance = new Subexamlab()
                def examlabInstance = Examlab.findByCode(line[0])
                subexamlabInstance.examlab=examlabInstance
                
                subexamlabInstance.code=line[0]
                subexamlabInstance.idline=line[1]
                subexamlabInstance.description=line[2]
                
                if(!subexamlabInstance.save(flush: true)){
                    println "fallo en ${subexamlabInstance.code}"
                }
            }
        }
        redirect(action: "list")
    }
    def delete() {
        def subexamlabInstance = Subexamlab.get(params.id)
        if (!subexamlabInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), params.id])
            redirect(action: "list")
            return
        }

        try {
            subexamlabInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'subexamlab.label', default: 'Subexamlab'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
