package historiamedica

import org.springframework.dao.DataIntegrityViolationException
import net.sf.jasperreports.engine.JRExporter
import net.sf.jasperreports.engine.JRExporterParameter
import net.sf.jasperreports.engine.JasperCompileManager
import net.sf.jasperreports.engine.JasperFillManager
import net.sf.jasperreports.engine.JasperPrint
import net.sf.jasperreports.engine.JasperReport
import net.sf.jasperreports.engine.export.JRPdfExporter
/**
 * TdocumentController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class TdocumentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
    def sessionFactory
    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [tdocumentInstanceList: Tdocument.list(params), tdocumentInstanceTotal: Tdocument.count()]
    }
    def downList1(){

        if(params.export=='xls' || params.export=='pdf'){ // Parámetro enviado

            //==========================================================================================================
            println "Creando Reporte"
            byte[] bytes= null; // // byte[] bytes = getJasperReport(xml, 'thePath/andNameOfYourReport.jasper')
            def connection = sessionFactory.currentSession.connection()
            try{
                Map parameters = new HashMap();

                println "id  ${params.id}"

                    parameters.put("id", params.id as long);
                

                if(params.export=='pdf'){
                    println "Creando PDF Jasper"
                    JasperReport report = JasperCompileManager.compileReport("./web-app/reportes/ListadoRecipe.jrxml");
                    JasperPrint print = JasperFillManager.fillReport(report, parameters, connection);
                    JRExporter exporter = null;
                    ByteArrayOutputStream  bytesReport = new ByteArrayOutputStream();
                    exporter = new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
                    exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, bytesReport);
                    exporter.exportReport();
                    bytes = bytesReport.toByteArray();
                }
                else if(params.export == 'xls'){
                   
                }
            }catch(Exception e){
                println "ERROR Descargando el Reporte: "+e.getMessage()
            }
            //==========================================================================================================
            def nombre = 'ListadoRecipe'

            if(bytes) {
                if (params.export == 'pdf') {
                    // Descargar Archivo
                    response.setContentType("application/pdf")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.pdf")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo Descargado"
                } else {
                    response.setContentType("application/vnd.ms-excel")
                    response.setHeader("Content-disposition", "attachment; filename=${nombre}.xls")
                    response.setContentLength(bytes.length)
                    response.getOutputStream().write(bytes)
                    println "Archivo xls Descargado"
                }
            }
        }
        //redirect(action: "list1")
        //return
    } 

    def list1() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        //def patientList = Patient.createCriteria().list() {
            if(params.search){
                params.dateInit="${params.dateInit}" 
                def fechaini=new Date().parse("yyyy-MM-dd",params.dateInit)
                 
                println "${params.name}"
                def listTdocument = Tdocument.executeQuery("SELECT p.datedocument,p.description,p.medico,m.name,p.typedocument, p.id FROM Tdocument p JOIN p.patient AS m WHERE p.datedocument  = '${params.dateInit}'")
                listTdocument.each { println "${it}" }
                println "${params.dateInit}"
                def tdocumentInstanceTotal = listTdocument.size()
                [tdocumentInstanceList: listTdocument, tdocumentInstanceTotal: tdocumentInstanceTotal]
                //between('date', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
            }
    }
    def list2() {
        params.max = Math.min(params.max ? params.int('max') : 5, 100)
        //def patientList = Patient.createCriteria().list() {
            if(params.search){
                params.dateInit="${params.dateInit}" 
                def fechaini=new Date().parse("yyyy-MM-dd",params.dateInit)
                 
                println "${params.name}"
                def listTdocument = Tdocument.executeQuery("SELECT p.datedocument,p.description,p.medico,m.name,p.typedocument, p.id FROM Tdocument p JOIN p.patient AS m WHERE m.name like '%${params.name}%' AND p.datedocument  = '${params.dateInit}'")
                listTdocument.each { println "${it}" }
                println "${params.dateInit}"
                def tdocumentInstanceTotal = listTdocument.size()
                [tdocumentInstanceList: listTdocument, tdocumentInstanceTotal: tdocumentInstanceTotal]
                //between('date', fechaini, fechafin)
                //GreaterThanEquals('dateEntry',fechaini)
                //LessThanEquals(dateEntry,fechafin)
                //medicoInstanceList: medicoList, medicoInstanceTotal: medicoList.totalCount]
            }
    }
    def create() {
        [tdocumentInstance: new Tdocument(params)]
    }

    def save() {
        def tdocumentInstance = new Tdocument(params)
        if (!tdocumentInstance.save(flush: true)) {
            render(view: "create", model: [tdocumentInstance: tdocumentInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), tdocumentInstance.id])
        redirect(action: "show", id: tdocumentInstance.id)
    }

    def show() {
        def tdocumentInstance = Tdocument.get(params.id)
        if (!tdocumentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), params.id])
            redirect(action: "list")
            return
        }

        [tdocumentInstance: tdocumentInstance]
    }

    def show1() {
        def tdocumentInstance = Tdocument.get(params.id)
        if (!tdocumentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), params.id])
            redirect(action: "list")
            return
        }

        [tdocumentInstance: tdocumentInstance]
    }
    def edit() {
        def tdocumentInstance = Tdocument.get(params.id)
        if (!tdocumentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), params.id])
            redirect(action: "list")
            return
        }

        [tdocumentInstance: tdocumentInstance]
    }

    def update() {
        def tdocumentInstance = Tdocument.get(params.id)
        if (!tdocumentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (tdocumentInstance.version > version) {
                tdocumentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'tdocument.label', default: 'Tdocument')] as Object[],
                          "Another user has updated this Tdocument while you were editing")
                render(view: "edit", model: [tdocumentInstance: tdocumentInstance])
                return
            }
        }

        tdocumentInstance.properties = params

        if (!tdocumentInstance.save(flush: true)) {
            render(view: "edit", model: [tdocumentInstance: tdocumentInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), tdocumentInstance.id])
        redirect(action: "show", id: tdocumentInstance.id)
    }

    def delete() {
        def tdocumentInstance = Tdocument.get(params.id)
        if (!tdocumentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), params.id])
            redirect(action: "list")
            return
        }

        try {
            tdocumentInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'tdocument.label', default: 'Tdocument'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
