package historiamedica

import org.springframework.dao.DataIntegrityViolationException

/**
 * AppointmentController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class AppointmentController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [appointmentInstanceList: Appointment.list(params), appointmentInstanceTotal: Appointment.count()]
    }

    def create() {
        [appointmentInstance: new Appointment(params)]
    }

    def save() {
        def appointmentInstance = new Appointment(params)
        if (!appointmentInstance.save(flush: true)) {
            render(view: "create", model: [appointmentInstance: appointmentInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'appointment.label', default: 'Appointment'), appointmentInstance.id])
        redirect(action: "show", id: appointmentInstance.id)
    }

    def show() {
        def appointmentInstance = Appointment.get(params.id)
        if (!appointmentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'appointment.label', default: 'Appointment'), params.id])
            redirect(action: "list")
            return
        }

        [appointmentInstance: appointmentInstance]
    }

    def edit() {
        def appointmentInstance = Appointment.get(params.id)
        if (!appointmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appointment.label', default: 'Appointment'), params.id])
            redirect(action: "list")
            return
        }

        [appointmentInstance: appointmentInstance]
    }

    def update() {
        def appointmentInstance = Appointment.get(params.id)
        if (!appointmentInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'appointment.label', default: 'Appointment'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (appointmentInstance.version > version) {
                appointmentInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'appointment.label', default: 'Appointment')] as Object[],
                          "Another user has updated this Appointment while you were editing")
                render(view: "edit", model: [appointmentInstance: appointmentInstance])
                return
            }
        }

        appointmentInstance.properties = params

        if (!appointmentInstance.save(flush: true)) {
            render(view: "edit", model: [appointmentInstance: appointmentInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'appointment.label', default: 'Appointment'), appointmentInstance.id])
        redirect(action: "show", id: appointmentInstance.id)
    }

    def delete() {
        def appointmentInstance = Appointment.get(params.id)
        if (!appointmentInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'appointment.label', default: 'Appointment'), params.id])
            redirect(action: "list")
            return
        }

        try {
            appointmentInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'appointment.label', default: 'Appointment'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'appointment.label', default: 'Appointment'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
