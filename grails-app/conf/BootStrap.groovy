import historiamedica.Usuario
import historiamedica.Rol
import historiamedica.UsuarioRol

class BootStrap {

    def init = { servletContext ->
    	def adminUser = new Usuario(username: "admin", password: "admin", email: "admin@medicalhistory.com",enabled: true)
    	adminUser.save(flush: true)
    	def medicUser = new Usuario(username: "medic", password: "medic", email: "medic@medicalhistory.com",enabled: true)
    	medicUser.save(flush: true)
    	def assistantUser = new Usuario(username: "assist", password: "assist", email: "assist@medicalhistory.com",enabled: true)
    	assistantUser.save(flush: true)

    	def adminRol = new Rol(authority: "ROLE_ADMIN").save(flush: true)
    	def medicRol = new Rol(authority: "ROLE_MEDIC").save(flush: true)
    	def assistantRol = new Rol(authority: "ROLE_ASSISTANT").save(flush: true)

    	UsuarioRol.create adminUser, adminRol, true
    	UsuarioRol.create medicUser, medicRol, true
    	UsuarioRol.create assistantUser, assistantRol, true

    	/*assert Usuario.count() == 3
      	assert Rol.count() == 3
      	assert UsuarioRol.count() == 3*/


    } 
    def destroy = {
    }
}
