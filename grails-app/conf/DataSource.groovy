dataSource {
    pooled = true
    driverClassName = "org.postgresql.Driver"
    dialect = org.hibernate.dialect.PostgreSQLDialect 
}

hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:postgresql://localhost/hm?useUnicode=yes&characterEncoding=UTF-8"
            //driverClassName = "com.mysql.jdbc.Driver"
            username = "postgres"
            password = "123456789"
            //password = "postgres"
            format_sql = true
            logSql = true

            /*
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:postgresql://localhost/hm?useUnicode=yes&characterEncoding=UTF-8"
            username = "postgres"
            password = "root"

            grails.dbconsole.enabled = true 
            */
        }
    }
   production {
        dataSource {
            dbCreate = "update"
    
            uri = new URI(System.env.DATABASE_URL?:"postgres://user:pass@localhost:5432/database")
            
            url = "jdbc:postgresql://"+uri.host+uri.path
            username = uri.userInfo.split(":")[0]
            password = uri.userInfo.split(":")[1]
            
            grails.dbconsole.enabled = true
        }
    }
}