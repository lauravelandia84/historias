package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * TypedocumentControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(TypedocumentController)
@Mock(Typedocument)
class TypedocumentControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/typedocument/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.typedocumentInstanceList.size() == 0
        assert model.typedocumentInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.typedocumentInstance != null
    }

    void testSave() {
        controller.save()

        assert model.typedocumentInstance != null
        assert view == '/typedocument/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/typedocument/show/1'
        assert controller.flash.message != null
        assert Typedocument.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/typedocument/list'


        populateValidParams(params)
        def typedocument = new Typedocument(params)

        assert typedocument.save() != null

        params.id = typedocument.id

        def model = controller.show()

        assert model.typedocumentInstance == typedocument
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/typedocument/list'


        populateValidParams(params)
        def typedocument = new Typedocument(params)

        assert typedocument.save() != null

        params.id = typedocument.id

        def model = controller.edit()

        assert model.typedocumentInstance == typedocument
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/typedocument/list'

        response.reset()


        populateValidParams(params)
        def typedocument = new Typedocument(params)

        assert typedocument.save() != null

        // test invalid parameters in update
        params.id = typedocument.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/typedocument/edit"
        assert model.typedocumentInstance != null

        typedocument.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/typedocument/show/$typedocument.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        typedocument.clearErrors()

        populateValidParams(params)
        params.id = typedocument.id
        params.version = -1
        controller.update()

        assert view == "/typedocument/edit"
        assert model.typedocumentInstance != null
        assert model.typedocumentInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/typedocument/list'

        response.reset()

        populateValidParams(params)
        def typedocument = new Typedocument(params)

        assert typedocument.save() != null
        assert Typedocument.count() == 1

        params.id = typedocument.id

        controller.delete()

        assert Typedocument.count() == 0
        assert Typedocument.get(typedocument.id) == null
        assert response.redirectedUrl == '/typedocument/list'
    }
}
