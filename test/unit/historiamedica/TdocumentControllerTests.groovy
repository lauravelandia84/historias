package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * TdocumentControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(TdocumentController)
@Mock(Tdocument)
class TdocumentControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/tdocument/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.tdocumentInstanceList.size() == 0
        assert model.tdocumentInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.tdocumentInstance != null
    }

    void testSave() {
        controller.save()

        assert model.tdocumentInstance != null
        assert view == '/tdocument/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/tdocument/show/1'
        assert controller.flash.message != null
        assert Tdocument.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/tdocument/list'


        populateValidParams(params)
        def tdocument = new Tdocument(params)

        assert tdocument.save() != null

        params.id = tdocument.id

        def model = controller.show()

        assert model.tdocumentInstance == tdocument
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/tdocument/list'


        populateValidParams(params)
        def tdocument = new Tdocument(params)

        assert tdocument.save() != null

        params.id = tdocument.id

        def model = controller.edit()

        assert model.tdocumentInstance == tdocument
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/tdocument/list'

        response.reset()


        populateValidParams(params)
        def tdocument = new Tdocument(params)

        assert tdocument.save() != null

        // test invalid parameters in update
        params.id = tdocument.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/tdocument/edit"
        assert model.tdocumentInstance != null

        tdocument.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/tdocument/show/$tdocument.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        tdocument.clearErrors()

        populateValidParams(params)
        params.id = tdocument.id
        params.version = -1
        controller.update()

        assert view == "/tdocument/edit"
        assert model.tdocumentInstance != null
        assert model.tdocumentInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/tdocument/list'

        response.reset()

        populateValidParams(params)
        def tdocument = new Tdocument(params)

        assert tdocument.save() != null
        assert Tdocument.count() == 1

        params.id = tdocument.id

        controller.delete()

        assert Tdocument.count() == 0
        assert Tdocument.get(tdocument.id) == null
        assert response.redirectedUrl == '/tdocument/list'
    }
}
