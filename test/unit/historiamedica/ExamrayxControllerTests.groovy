package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * ExamrayxControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(ExamrayxController)
@Mock(Examrayx)
class ExamrayxControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/examrayx/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.examrayxInstanceList.size() == 0
        assert model.examrayxInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.examrayxInstance != null
    }

    void testSave() {
        controller.save()

        assert model.examrayxInstance != null
        assert view == '/examrayx/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/examrayx/show/1'
        assert controller.flash.message != null
        assert Examrayx.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/examrayx/list'


        populateValidParams(params)
        def examrayx = new Examrayx(params)

        assert examrayx.save() != null

        params.id = examrayx.id

        def model = controller.show()

        assert model.examrayxInstance == examrayx
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/examrayx/list'


        populateValidParams(params)
        def examrayx = new Examrayx(params)

        assert examrayx.save() != null

        params.id = examrayx.id

        def model = controller.edit()

        assert model.examrayxInstance == examrayx
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/examrayx/list'

        response.reset()


        populateValidParams(params)
        def examrayx = new Examrayx(params)

        assert examrayx.save() != null

        // test invalid parameters in update
        params.id = examrayx.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/examrayx/edit"
        assert model.examrayxInstance != null

        examrayx.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/examrayx/show/$examrayx.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        examrayx.clearErrors()

        populateValidParams(params)
        params.id = examrayx.id
        params.version = -1
        controller.update()

        assert view == "/examrayx/edit"
        assert model.examrayxInstance != null
        assert model.examrayxInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/examrayx/list'

        response.reset()

        populateValidParams(params)
        def examrayx = new Examrayx(params)

        assert examrayx.save() != null
        assert Examrayx.count() == 1

        params.id = examrayx.id

        controller.delete()

        assert Examrayx.count() == 0
        assert Examrayx.get(examrayx.id) == null
        assert response.redirectedUrl == '/examrayx/list'
    }
}
