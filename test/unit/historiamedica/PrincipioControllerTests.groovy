package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * PrincipioControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(PrincipioController)
@Mock(Principio)
class PrincipioControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/principio/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.principioInstanceList.size() == 0
        assert model.principioInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.principioInstance != null
    }

    void testSave() {
        controller.save()

        assert model.principioInstance != null
        assert view == '/principio/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/principio/show/1'
        assert controller.flash.message != null
        assert Principio.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/principio/list'


        populateValidParams(params)
        def principio = new Principio(params)

        assert principio.save() != null

        params.id = principio.id

        def model = controller.show()

        assert model.principioInstance == principio
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/principio/list'


        populateValidParams(params)
        def principio = new Principio(params)

        assert principio.save() != null

        params.id = principio.id

        def model = controller.edit()

        assert model.principioInstance == principio
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/principio/list'

        response.reset()


        populateValidParams(params)
        def principio = new Principio(params)

        assert principio.save() != null

        // test invalid parameters in update
        params.id = principio.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/principio/edit"
        assert model.principioInstance != null

        principio.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/principio/show/$principio.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        principio.clearErrors()

        populateValidParams(params)
        params.id = principio.id
        params.version = -1
        controller.update()

        assert view == "/principio/edit"
        assert model.principioInstance != null
        assert model.principioInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/principio/list'

        response.reset()

        populateValidParams(params)
        def principio = new Principio(params)

        assert principio.save() != null
        assert Principio.count() == 1

        params.id = principio.id

        controller.delete()

        assert Principio.count() == 0
        assert Principio.get(principio.id) == null
        assert response.redirectedUrl == '/principio/list'
    }
}
