package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * ExamlabControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(ExamlabController)
@Mock(Examlab)
class ExamlabControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/examlab/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.examlabInstanceList.size() == 0
        assert model.examlabInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.examlabInstance != null
    }

    void testSave() {
        controller.save()

        assert model.examlabInstance != null
        assert view == '/examlab/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/examlab/show/1'
        assert controller.flash.message != null
        assert Examlab.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/examlab/list'


        populateValidParams(params)
        def examlab = new Examlab(params)

        assert examlab.save() != null

        params.id = examlab.id

        def model = controller.show()

        assert model.examlabInstance == examlab
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/examlab/list'


        populateValidParams(params)
        def examlab = new Examlab(params)

        assert examlab.save() != null

        params.id = examlab.id

        def model = controller.edit()

        assert model.examlabInstance == examlab
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/examlab/list'

        response.reset()


        populateValidParams(params)
        def examlab = new Examlab(params)

        assert examlab.save() != null

        // test invalid parameters in update
        params.id = examlab.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/examlab/edit"
        assert model.examlabInstance != null

        examlab.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/examlab/show/$examlab.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        examlab.clearErrors()

        populateValidParams(params)
        params.id = examlab.id
        params.version = -1
        controller.update()

        assert view == "/examlab/edit"
        assert model.examlabInstance != null
        assert model.examlabInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/examlab/list'

        response.reset()

        populateValidParams(params)
        def examlab = new Examlab(params)

        assert examlab.save() != null
        assert Examlab.count() == 1

        params.id = examlab.id

        controller.delete()

        assert Examlab.count() == 0
        assert Examlab.get(examlab.id) == null
        assert response.redirectedUrl == '/examlab/list'
    }
}
