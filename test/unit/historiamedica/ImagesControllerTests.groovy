package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * ImagesControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(ImagesController)
@Mock(Images)
class ImagesControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/images/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.imagesInstanceList.size() == 0
        assert model.imagesInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.imagesInstance != null
    }

    void testSave() {
        controller.save()

        assert model.imagesInstance != null
        assert view == '/images/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/images/show/1'
        assert controller.flash.message != null
        assert Images.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/images/list'


        populateValidParams(params)
        def images = new Images(params)

        assert images.save() != null

        params.id = images.id

        def model = controller.show()

        assert model.imagesInstance == images
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/images/list'


        populateValidParams(params)
        def images = new Images(params)

        assert images.save() != null

        params.id = images.id

        def model = controller.edit()

        assert model.imagesInstance == images
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/images/list'

        response.reset()


        populateValidParams(params)
        def images = new Images(params)

        assert images.save() != null

        // test invalid parameters in update
        params.id = images.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/images/edit"
        assert model.imagesInstance != null

        images.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/images/show/$images.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        images.clearErrors()

        populateValidParams(params)
        params.id = images.id
        params.version = -1
        controller.update()

        assert view == "/images/edit"
        assert model.imagesInstance != null
        assert model.imagesInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/images/list'

        response.reset()

        populateValidParams(params)
        def images = new Images(params)

        assert images.save() != null
        assert Images.count() == 1

        params.id = images.id

        controller.delete()

        assert Images.count() == 0
        assert Images.get(images.id) == null
        assert response.redirectedUrl == '/images/list'
    }
}
