package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * TypeconsultationControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(TypeconsultationController)
@Mock(Typeconsultation)
class TypeconsultationControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/typeconsultation/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.typeconsultationInstanceList.size() == 0
        assert model.typeconsultationInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.typeconsultationInstance != null
    }

    void testSave() {
        controller.save()

        assert model.typeconsultationInstance != null
        assert view == '/typeconsultation/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/typeconsultation/show/1'
        assert controller.flash.message != null
        assert Typeconsultation.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/typeconsultation/list'


        populateValidParams(params)
        def typeconsultation = new Typeconsultation(params)

        assert typeconsultation.save() != null

        params.id = typeconsultation.id

        def model = controller.show()

        assert model.typeconsultationInstance == typeconsultation
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/typeconsultation/list'


        populateValidParams(params)
        def typeconsultation = new Typeconsultation(params)

        assert typeconsultation.save() != null

        params.id = typeconsultation.id

        def model = controller.edit()

        assert model.typeconsultationInstance == typeconsultation
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/typeconsultation/list'

        response.reset()


        populateValidParams(params)
        def typeconsultation = new Typeconsultation(params)

        assert typeconsultation.save() != null

        // test invalid parameters in update
        params.id = typeconsultation.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/typeconsultation/edit"
        assert model.typeconsultationInstance != null

        typeconsultation.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/typeconsultation/show/$typeconsultation.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        typeconsultation.clearErrors()

        populateValidParams(params)
        params.id = typeconsultation.id
        params.version = -1
        controller.update()

        assert view == "/typeconsultation/edit"
        assert model.typeconsultationInstance != null
        assert model.typeconsultationInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/typeconsultation/list'

        response.reset()

        populateValidParams(params)
        def typeconsultation = new Typeconsultation(params)

        assert typeconsultation.save() != null
        assert Typeconsultation.count() == 1

        params.id = typeconsultation.id

        controller.delete()

        assert Typeconsultation.count() == 0
        assert Typeconsultation.get(typeconsultation.id) == null
        assert response.redirectedUrl == '/typeconsultation/list'
    }
}
