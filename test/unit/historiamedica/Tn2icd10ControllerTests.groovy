package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * Tn2icd10ControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(Tn2icd10Controller)
@Mock(Tn2icd10)
class Tn2icd10ControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/tn2icd10/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.tn2icd10InstanceList.size() == 0
        assert model.tn2icd10InstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.tn2icd10Instance != null
    }

    void testSave() {
        controller.save()

        assert model.tn2icd10Instance != null
        assert view == '/tn2icd10/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/tn2icd10/show/1'
        assert controller.flash.message != null
        assert Tn2icd10.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/tn2icd10/list'


        populateValidParams(params)
        def tn2icd10 = new Tn2icd10(params)

        assert tn2icd10.save() != null

        params.id = tn2icd10.id

        def model = controller.show()

        assert model.tn2icd10Instance == tn2icd10
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/tn2icd10/list'


        populateValidParams(params)
        def tn2icd10 = new Tn2icd10(params)

        assert tn2icd10.save() != null

        params.id = tn2icd10.id

        def model = controller.edit()

        assert model.tn2icd10Instance == tn2icd10
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/tn2icd10/list'

        response.reset()


        populateValidParams(params)
        def tn2icd10 = new Tn2icd10(params)

        assert tn2icd10.save() != null

        // test invalid parameters in update
        params.id = tn2icd10.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/tn2icd10/edit"
        assert model.tn2icd10Instance != null

        tn2icd10.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/tn2icd10/show/$tn2icd10.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        tn2icd10.clearErrors()

        populateValidParams(params)
        params.id = tn2icd10.id
        params.version = -1
        controller.update()

        assert view == "/tn2icd10/edit"
        assert model.tn2icd10Instance != null
        assert model.tn2icd10Instance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/tn2icd10/list'

        response.reset()

        populateValidParams(params)
        def tn2icd10 = new Tn2icd10(params)

        assert tn2icd10.save() != null
        assert Tn2icd10.count() == 1

        params.id = tn2icd10.id

        controller.delete()

        assert Tn2icd10.count() == 0
        assert Tn2icd10.get(tn2icd10.id) == null
        assert response.redirectedUrl == '/tn2icd10/list'
    }
}
