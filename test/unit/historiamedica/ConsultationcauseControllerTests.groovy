package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * ConsultationcauseControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(ConsultationcauseController)
@Mock(Consultationcause)
class ConsultationcauseControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/consultationcause/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.consultationcauseInstanceList.size() == 0
        assert model.consultationcauseInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.consultationcauseInstance != null
    }

    void testSave() {
        controller.save()

        assert model.consultationcauseInstance != null
        assert view == '/consultationcause/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/consultationcause/show/1'
        assert controller.flash.message != null
        assert Consultationcause.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/consultationcause/list'


        populateValidParams(params)
        def consultationcause = new Consultationcause(params)

        assert consultationcause.save() != null

        params.id = consultationcause.id

        def model = controller.show()

        assert model.consultationcauseInstance == consultationcause
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/consultationcause/list'


        populateValidParams(params)
        def consultationcause = new Consultationcause(params)

        assert consultationcause.save() != null

        params.id = consultationcause.id

        def model = controller.edit()

        assert model.consultationcauseInstance == consultationcause
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/consultationcause/list'

        response.reset()


        populateValidParams(params)
        def consultationcause = new Consultationcause(params)

        assert consultationcause.save() != null

        // test invalid parameters in update
        params.id = consultationcause.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/consultationcause/edit"
        assert model.consultationcauseInstance != null

        consultationcause.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/consultationcause/show/$consultationcause.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        consultationcause.clearErrors()

        populateValidParams(params)
        params.id = consultationcause.id
        params.version = -1
        controller.update()

        assert view == "/consultationcause/edit"
        assert model.consultationcauseInstance != null
        assert model.consultationcauseInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/consultationcause/list'

        response.reset()

        populateValidParams(params)
        def consultationcause = new Consultationcause(params)

        assert consultationcause.save() != null
        assert Consultationcause.count() == 1

        params.id = consultationcause.id

        controller.delete()

        assert Consultationcause.count() == 0
        assert Consultationcause.get(consultationcause.id) == null
        assert response.redirectedUrl == '/consultationcause/list'
    }
}
