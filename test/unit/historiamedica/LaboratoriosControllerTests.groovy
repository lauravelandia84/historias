package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * LaboratoriosControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(LaboratoriosController)
@Mock(Laboratorios)
class LaboratoriosControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/laboratorios/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.laboratoriosInstanceList.size() == 0
        assert model.laboratoriosInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.laboratoriosInstance != null
    }

    void testSave() {
        controller.save()

        assert model.laboratoriosInstance != null
        assert view == '/laboratorios/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/laboratorios/show/1'
        assert controller.flash.message != null
        assert Laboratorios.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/laboratorios/list'


        populateValidParams(params)
        def laboratorios = new Laboratorios(params)

        assert laboratorios.save() != null

        params.id = laboratorios.id

        def model = controller.show()

        assert model.laboratoriosInstance == laboratorios
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/laboratorios/list'


        populateValidParams(params)
        def laboratorios = new Laboratorios(params)

        assert laboratorios.save() != null

        params.id = laboratorios.id

        def model = controller.edit()

        assert model.laboratoriosInstance == laboratorios
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/laboratorios/list'

        response.reset()


        populateValidParams(params)
        def laboratorios = new Laboratorios(params)

        assert laboratorios.save() != null

        // test invalid parameters in update
        params.id = laboratorios.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/laboratorios/edit"
        assert model.laboratoriosInstance != null

        laboratorios.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/laboratorios/show/$laboratorios.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        laboratorios.clearErrors()

        populateValidParams(params)
        params.id = laboratorios.id
        params.version = -1
        controller.update()

        assert view == "/laboratorios/edit"
        assert model.laboratoriosInstance != null
        assert model.laboratoriosInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/laboratorios/list'

        response.reset()

        populateValidParams(params)
        def laboratorios = new Laboratorios(params)

        assert laboratorios.save() != null
        assert Laboratorios.count() == 1

        params.id = laboratorios.id

        controller.delete()

        assert Laboratorios.count() == 0
        assert Laboratorios.get(laboratorios.id) == null
        assert response.redirectedUrl == '/laboratorios/list'
    }
}
