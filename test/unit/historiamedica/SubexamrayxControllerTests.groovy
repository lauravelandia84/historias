package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * SubexamrayxControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(SubexamrayxController)
@Mock(Subexamrayx)
class SubexamrayxControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/subexamrayx/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.subexamrayxInstanceList.size() == 0
        assert model.subexamrayxInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.subexamrayxInstance != null
    }

    void testSave() {
        controller.save()

        assert model.subexamrayxInstance != null
        assert view == '/subexamrayx/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/subexamrayx/show/1'
        assert controller.flash.message != null
        assert Subexamrayx.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/subexamrayx/list'


        populateValidParams(params)
        def subexamrayx = new Subexamrayx(params)

        assert subexamrayx.save() != null

        params.id = subexamrayx.id

        def model = controller.show()

        assert model.subexamrayxInstance == subexamrayx
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/subexamrayx/list'


        populateValidParams(params)
        def subexamrayx = new Subexamrayx(params)

        assert subexamrayx.save() != null

        params.id = subexamrayx.id

        def model = controller.edit()

        assert model.subexamrayxInstance == subexamrayx
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/subexamrayx/list'

        response.reset()


        populateValidParams(params)
        def subexamrayx = new Subexamrayx(params)

        assert subexamrayx.save() != null

        // test invalid parameters in update
        params.id = subexamrayx.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/subexamrayx/edit"
        assert model.subexamrayxInstance != null

        subexamrayx.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/subexamrayx/show/$subexamrayx.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        subexamrayx.clearErrors()

        populateValidParams(params)
        params.id = subexamrayx.id
        params.version = -1
        controller.update()

        assert view == "/subexamrayx/edit"
        assert model.subexamrayxInstance != null
        assert model.subexamrayxInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/subexamrayx/list'

        response.reset()

        populateValidParams(params)
        def subexamrayx = new Subexamrayx(params)

        assert subexamrayx.save() != null
        assert Subexamrayx.count() == 1

        params.id = subexamrayx.id

        controller.delete()

        assert Subexamrayx.count() == 0
        assert Subexamrayx.get(subexamrayx.id) == null
        assert response.redirectedUrl == '/subexamrayx/list'
    }
}
