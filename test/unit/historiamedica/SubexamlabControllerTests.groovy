package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * SubexamlabControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(SubexamlabController)
@Mock(Subexamlab)
class SubexamlabControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/subexamlab/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.subexamlabInstanceList.size() == 0
        assert model.subexamlabInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.subexamlabInstance != null
    }

    void testSave() {
        controller.save()

        assert model.subexamlabInstance != null
        assert view == '/subexamlab/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/subexamlab/show/1'
        assert controller.flash.message != null
        assert Subexamlab.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/subexamlab/list'


        populateValidParams(params)
        def subexamlab = new Subexamlab(params)

        assert subexamlab.save() != null

        params.id = subexamlab.id

        def model = controller.show()

        assert model.subexamlabInstance == subexamlab
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/subexamlab/list'


        populateValidParams(params)
        def subexamlab = new Subexamlab(params)

        assert subexamlab.save() != null

        params.id = subexamlab.id

        def model = controller.edit()

        assert model.subexamlabInstance == subexamlab
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/subexamlab/list'

        response.reset()


        populateValidParams(params)
        def subexamlab = new Subexamlab(params)

        assert subexamlab.save() != null

        // test invalid parameters in update
        params.id = subexamlab.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/subexamlab/edit"
        assert model.subexamlabInstance != null

        subexamlab.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/subexamlab/show/$subexamlab.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        subexamlab.clearErrors()

        populateValidParams(params)
        params.id = subexamlab.id
        params.version = -1
        controller.update()

        assert view == "/subexamlab/edit"
        assert model.subexamlabInstance != null
        assert model.subexamlabInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/subexamlab/list'

        response.reset()

        populateValidParams(params)
        def subexamlab = new Subexamlab(params)

        assert subexamlab.save() != null
        assert Subexamlab.count() == 1

        params.id = subexamlab.id

        controller.delete()

        assert Subexamlab.count() == 0
        assert Subexamlab.get(subexamlab.id) == null
        assert response.redirectedUrl == '/subexamlab/list'
    }
}
