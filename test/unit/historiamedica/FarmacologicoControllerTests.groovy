package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * FarmacologicoControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(FarmacologicoController)
@Mock(Farmacologico)
class FarmacologicoControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/farmacologico/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.farmacologicoInstanceList.size() == 0
        assert model.farmacologicoInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.farmacologicoInstance != null
    }

    void testSave() {
        controller.save()

        assert model.farmacologicoInstance != null
        assert view == '/farmacologico/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/farmacologico/show/1'
        assert controller.flash.message != null
        assert Farmacologico.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/farmacologico/list'


        populateValidParams(params)
        def farmacologico = new Farmacologico(params)

        assert farmacologico.save() != null

        params.id = farmacologico.id

        def model = controller.show()

        assert model.farmacologicoInstance == farmacologico
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/farmacologico/list'


        populateValidParams(params)
        def farmacologico = new Farmacologico(params)

        assert farmacologico.save() != null

        params.id = farmacologico.id

        def model = controller.edit()

        assert model.farmacologicoInstance == farmacologico
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/farmacologico/list'

        response.reset()


        populateValidParams(params)
        def farmacologico = new Farmacologico(params)

        assert farmacologico.save() != null

        // test invalid parameters in update
        params.id = farmacologico.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/farmacologico/edit"
        assert model.farmacologicoInstance != null

        farmacologico.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/farmacologico/show/$farmacologico.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        farmacologico.clearErrors()

        populateValidParams(params)
        params.id = farmacologico.id
        params.version = -1
        controller.update()

        assert view == "/farmacologico/edit"
        assert model.farmacologicoInstance != null
        assert model.farmacologicoInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/farmacologico/list'

        response.reset()

        populateValidParams(params)
        def farmacologico = new Farmacologico(params)

        assert farmacologico.save() != null
        assert Farmacologico.count() == 1

        params.id = farmacologico.id

        controller.delete()

        assert Farmacologico.count() == 0
        assert Farmacologico.get(farmacologico.id) == null
        assert response.redirectedUrl == '/farmacologico/list'
    }
}
