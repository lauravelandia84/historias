package historiamedica



import org.junit.*
import grails.test.mixin.*

/**
 * MedicamentoControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(MedicamentoController)
@Mock(Medicamento)
class MedicamentoControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/medicamento/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.medicamentoInstanceList.size() == 0
        assert model.medicamentoInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.medicamentoInstance != null
    }

    void testSave() {
        controller.save()

        assert model.medicamentoInstance != null
        assert view == '/medicamento/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/medicamento/show/1'
        assert controller.flash.message != null
        assert Medicamento.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/medicamento/list'


        populateValidParams(params)
        def medicamento = new Medicamento(params)

        assert medicamento.save() != null

        params.id = medicamento.id

        def model = controller.show()

        assert model.medicamentoInstance == medicamento
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/medicamento/list'


        populateValidParams(params)
        def medicamento = new Medicamento(params)

        assert medicamento.save() != null

        params.id = medicamento.id

        def model = controller.edit()

        assert model.medicamentoInstance == medicamento
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/medicamento/list'

        response.reset()


        populateValidParams(params)
        def medicamento = new Medicamento(params)

        assert medicamento.save() != null

        // test invalid parameters in update
        params.id = medicamento.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/medicamento/edit"
        assert model.medicamentoInstance != null

        medicamento.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/medicamento/show/$medicamento.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        medicamento.clearErrors()

        populateValidParams(params)
        params.id = medicamento.id
        params.version = -1
        controller.update()

        assert view == "/medicamento/edit"
        assert model.medicamentoInstance != null
        assert model.medicamentoInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/medicamento/list'

        response.reset()

        populateValidParams(params)
        def medicamento = new Medicamento(params)

        assert medicamento.save() != null
        assert Medicamento.count() == 1

        params.id = medicamento.id

        controller.delete()

        assert Medicamento.count() == 0
        assert Medicamento.get(medicamento.id) == null
        assert response.redirectedUrl == '/medicamento/list'
    }
}
